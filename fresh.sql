-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 25, 2013 at 12:21 PM
-- Server version: 5.5.27-log
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cicsdrive3`
--

-- --------------------------------------------------------

--
-- Table structure for table `archives`
--

CREATE TABLE IF NOT EXISTS `archives` (
  `archiveID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) unsigned NOT NULL,
  `archiveDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` char(1) COLLATE utf8_bin NOT NULL DEFAULT 'A',
  PRIMARY KEY (`archiveID`),
  KEY `userID` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `commentID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) unsigned NOT NULL,
  `publicationID` int(11) NOT NULL,
  `comment` varchar(140) COLLATE utf8_bin NOT NULL,
  `commentDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` char(1) COLLATE utf8_bin NOT NULL DEFAULT 'A',
  PRIMARY KEY (`commentID`),
  KEY `userID` (`userID`),
  KEY `publicationID` (`publicationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fileactivitylogs`
--

CREATE TABLE IF NOT EXISTS `fileactivitylogs` (
  `activityID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` int(10) unsigned NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userID` int(10) unsigned zerofill NOT NULL,
  PRIMARY KEY (`activityID`),
  KEY `userID` (`userID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fileactivityproperties`
--

CREATE TABLE IF NOT EXISTS `fileactivityproperties` (
  `activityID` int(10) unsigned NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`activityID`,`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `filefilters`
--

CREATE TABLE IF NOT EXISTS `filefilters` (
  `fileFiltersID` int(11) NOT NULL AUTO_INCREMENT,
  `extension` varchar(10) COLLATE utf8_bin NOT NULL,
  `forFaculty` tinyint(1) NOT NULL DEFAULT '0',
  `forStudent` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fileFiltersID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE IF NOT EXISTS `logins` (
  `loginID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userID` int(11) unsigned zerofill DEFAULT NULL,
  `loginDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IpAddress` varchar(19) COLLATE utf8_bin NOT NULL,
  `sessionID` char(32) COLLATE utf8_bin NOT NULL,
  `userAgent` varchar(50) COLLATE utf8_bin NOT NULL,
  `status` char(1) COLLATE utf8_bin NOT NULL DEFAULT 'A',
  PRIMARY KEY (`loginID`),
  KEY `login_user` (`userID`),
  KEY `index3` (`userID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `publicationmutes`
--

CREATE TABLE IF NOT EXISTS `publicationmutes` (
  `muteID` int(11) NOT NULL AUTO_INCREMENT,
  `publicationID` int(11) NOT NULL,
  `userID` int(10) unsigned zerofill NOT NULL,
  `muteDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`muteID`),
  KEY `user_id` (`userID`),
  KEY `publication` (`publicationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `publications`
--

CREATE TABLE IF NOT EXISTS `publications` (
  `publicationID` int(11) NOT NULL AUTO_INCREMENT,
  `publicationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userID` int(10) unsigned zerofill NOT NULL,
  `filePath` text COLLATE utf8_bin NOT NULL,
  `origPath` text COLLATE utf8_bin NOT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` char(1) COLLATE utf8_bin NOT NULL DEFAULT 'A',
  PRIMARY KEY (`publicationID`),
  KEY `userID` (`userID`),
  KEY `publicationDate` (`publicationDate`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `publishnotifications`
--

CREATE TABLE IF NOT EXISTS `publishnotifications` (
  `notificationID` int(11) NOT NULL AUTO_INCREMENT,
  `studentUserID` int(11) unsigned NOT NULL,
  `publicationID` int(11) NOT NULL,
  `status` char(1) COLLATE utf8_bin NOT NULL DEFAULT 'A',
  PRIMARY KEY (`notificationID`),
  KEY `student_user` (`studentUserID`),
  KEY `publication` (`publicationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sharedfilefilters`
--

CREATE TABLE IF NOT EXISTS `sharedfilefilters` (
  `sharedFileFilterID` int(11) NOT NULL AUTO_INCREMENT,
  `extension` varchar(10) COLLATE utf8_bin NOT NULL,
  `sharedFolderID` int(11) NOT NULL,
  PRIMARY KEY (`sharedFileFilterID`),
  KEY `shared_folder` (`sharedFolderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sharedfolders`
--

CREATE TABLE IF NOT EXISTS `sharedfolders` (
  `sharedFolderID` int(11) NOT NULL AUTO_INCREMENT,
  `facultyUserID` int(10) unsigned zerofill NOT NULL,
  `folderPath` text COLLATE utf8_bin NOT NULL,
  `sharedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deadlineDate` timestamp NULL DEFAULT NULL,
  `accessMode` char(1) COLLATE utf8_bin NOT NULL,
  `filterType` int(11) NOT NULL,
  `submissionPrefix` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `submissionSuffix` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `publicationID` int(11) DEFAULT NULL,
  `password` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `status` char(1) COLLATE utf8_bin NOT NULL DEFAULT 'A',
  PRIMARY KEY (`sharedFolderID`),
  KEY `faculty` (`facultyUserID`),
  KEY `publication` (`publicationID`),
  KEY `facultyUserID` (`facultyUserID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `studentinfo`
--

CREATE TABLE IF NOT EXISTS `studentinfo` (
  `studentInfoID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userID` int(11) unsigned zerofill NOT NULL,
  `answer` varchar(100) COLLATE utf8_bin NOT NULL,
  `studentQuestionID` int(11) DEFAULT NULL,
  `status` char(1) COLLATE utf8_bin NOT NULL DEFAULT 'A',
  PRIMARY KEY (`studentInfoID`),
  UNIQUE KEY `studentInfoID` (`studentInfoID`),
  KEY `student_question` (`studentQuestionID`),
  KEY `userID` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `studentquestions`
--

CREATE TABLE IF NOT EXISTS `studentquestions` (
  `studentQuestionID` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(100) COLLATE utf8_bin NOT NULL,
  `status` char(1) COLLATE utf8_bin NOT NULL DEFAULT 'A',
  PRIMARY KEY (`studentQuestionID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=27 ;

--
-- Dumping data for table `studentquestions`
--

INSERT INTO `studentquestions` (`studentQuestionID`, `question`, `status`) VALUES
(1, 'What is your mother''s first name?', 'A'),
(2, 'What is your father''s first name?', 'A'),
(3, 'How many siblings do you have?', 'A'),
(4, 'How many sisters do you have?', 'A'),
(5, 'How many brothers do you have?', 'A'),
(6, 'What is your father''s occupation?', 'A'),
(7, 'What is your mother''s occupation?', 'A'),
(8, 'What month is your birthday?', 'A'),
(9, 'What month is your mother''s birthday?', 'A'),
(10, 'What month is your father''s birthday?', 'A'),
(11, 'How many are you in the family?', 'A'),
(12, 'What is your nickname at home?', 'A'),
(13, 'Where did you attend primary school?', 'A'),
(14, 'Where did you attend secondary school?', 'A'),
(15, 'What programming language do you like most?', 'A'),
(16, 'What is your favorite color?', 'A'),
(17, 'What type of pet you have at home?', 'A'),
(18, 'What is your favorite movie?', 'A'),
(19, 'What is your favorite sports team?', 'A'),
(20, 'What is your preferred musical genre?', 'A'),
(21, 'What is the title of your favorite song?', 'A'),
(22, 'What is your favorite TV program?', 'A'),
(23, 'Who is your favorite actor?', 'A'),
(24, 'Who was your childhood hero?', 'A'),
(25, 'Who is your first kiss?', 'A'),
(26, 'Who is you favorite actress?', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `submissions`
--

CREATE TABLE IF NOT EXISTS `submissions` (
  `submissionID` int(11) NOT NULL AUTO_INCREMENT,
  `sharedFolderID` int(11) NOT NULL,
  `studentUserID` int(10) unsigned zerofill NOT NULL,
  `submissionDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `originalFolderName` varchar(50) COLLATE utf8_bin NOT NULL,
  `status` char(1) COLLATE utf8_bin NOT NULL DEFAULT 'A',
  PRIMARY KEY (`submissionID`),
  KEY `submission_user_id` (`studentUserID`),
  KEY `sharedFolderID` (`sharedFolderID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE IF NOT EXISTS `uploads` (
  `uploadID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) unsigned NOT NULL,
  `path` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `uploadDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '-1' COMMENT 'UPLOAD_ERR_OK:0 UPLOAD_ERR_INI_SIZE:1 UPLOAD_ERR_FORM_SIZE:2 UPLOAD_ERR_PARTIAL:3 UPLOAD_ERR_NO_FILE:4 UPLOAD_ERR_NO_TMP_DIR:6 UPLOAD_ERR_CANT_WRITE:7 UPLOAD_ERR_EXTENSION:8 FILE_FILTER_REJECT:9 ANTIVIRUS_REJECT:10 INSUFFICIENT_STORAGE_SPACE:11 FILE_EXISTS:12 UNKNOWN_FILE:13 OVERWRITE:14 UPLOAD_CANCELLED:15',
  PRIMARY KEY (`uploadID`),
  KEY `fk_Uploads_Users1` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `useractivitylogs`
--

CREATE TABLE IF NOT EXISTS `useractivitylogs` (
  `activityID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userID` int(10) unsigned zerofill NOT NULL,
  PRIMARY KEY (`activityID`),
  KEY `category` (`category`),
  KEY `date` (`date`),
  KEY `userID` (`userID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `useractivityproperties`
--

CREATE TABLE IF NOT EXISTS `useractivityproperties` (
  `activityID` bigint(20) unsigned NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`activityID`,`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userID` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `password` char(32) COLLATE utf8_bin DEFAULT NULL,
  `idNumber` char(8) COLLATE utf8_bin DEFAULT NULL,
  `firstName` varchar(50) COLLATE utf8_bin NOT NULL,
  `middleInitial` char(1) COLLATE utf8_bin DEFAULT NULL,
  `lastName` varchar(50) COLLATE utf8_bin NOT NULL,
  `usedStorage` decimal(10,0) NOT NULL DEFAULT '0',
  `userType` int(11) NOT NULL DEFAULT '0',
  `avatarFileName` varchar(36) COLLATE utf8_bin DEFAULT NULL,
  `driveView` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `status` char(1) COLLATE utf8_bin NOT NULL DEFAULT 'A',
  PRIMARY KEY (`userID`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `idNumber_UNIQUE` (`idNumber`),
  KEY `user_type` (`userType`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1000 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userID`, `email`, `password`, `idNumber`, `firstName`, `middleInitial`, `lastName`, `usedStorage`, `userType`, `avatarFileName`, `driveView`, `status`) VALUES
(0000000001, 'administrator@uc.ph', 'cc6b45fd5f888ada103ec4afad3f1300', NULL, 'Jeff', NULL, 'Salimbangon', 0, 1, NULL, 0, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `usertypes`
--

CREATE TABLE IF NOT EXISTS `usertypes` (
  `userTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) COLLATE utf8_bin NOT NULL,
  `storageCapacity` decimal(10,0) NOT NULL,
  `status` char(1) COLLATE utf8_bin NOT NULL DEFAULT 'A',
  PRIMARY KEY (`userTypeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Dumping data for table `usertypes`
--

INSERT INTO `usertypes` (`userTypeID`, `description`, `storageCapacity`, `status`) VALUES
(1, 'Admin', -1, 'O'),
(2, 'Faculty', 2147000000, 'O'),
(3, 'Student', 104857600, 'O');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `archives`
--
ALTER TABLE `archives`
  ADD CONSTRAINT `archives_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`publicationID`) REFERENCES `publications` (`publicationID`);

--
-- Constraints for table `fileactivitylogs`
--
ALTER TABLE `fileactivitylogs`
  ADD CONSTRAINT `fileactivitylogs_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`) ON UPDATE CASCADE;

--
-- Constraints for table `fileactivityproperties`
--
ALTER TABLE `fileactivityproperties`
  ADD CONSTRAINT `fileactivityproperties_ibfk_1` FOREIGN KEY (`activityID`) REFERENCES `fileactivitylogs` (`activityID`);

--
-- Constraints for table `logins`
--
ALTER TABLE `logins`
  ADD CONSTRAINT `logins_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);

--
-- Constraints for table `publicationmutes`
--
ALTER TABLE `publicationmutes`
  ADD CONSTRAINT `publicationmutes_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`),
  ADD CONSTRAINT `publicationmutes_ibfk_2` FOREIGN KEY (`publicationID`) REFERENCES `publications` (`publicationID`);

--
-- Constraints for table `publications`
--
ALTER TABLE `publications`
  ADD CONSTRAINT `publications_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);

--
-- Constraints for table `publishnotifications`
--
ALTER TABLE `publishnotifications`
  ADD CONSTRAINT `publishnotifications_ibfk_1` FOREIGN KEY (`studentUserID`) REFERENCES `users` (`userID`),
  ADD CONSTRAINT `publishnotifications_ibfk_2` FOREIGN KEY (`publicationID`) REFERENCES `publications` (`publicationID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sharedfilefilters`
--
ALTER TABLE `sharedfilefilters`
  ADD CONSTRAINT `sharedfilefilters_ibfk_1` FOREIGN KEY (`sharedFolderID`) REFERENCES `sharedfolders` (`sharedFolderID`);

--
-- Constraints for table `sharedfolders`
--
ALTER TABLE `sharedfolders`
  ADD CONSTRAINT `sharedfolders_ibfk_1` FOREIGN KEY (`facultyUserID`) REFERENCES `users` (`userID`),
  ADD CONSTRAINT `sharedfolders_ibfk_2` FOREIGN KEY (`publicationID`) REFERENCES `publications` (`publicationID`);

--
-- Constraints for table `studentinfo`
--
ALTER TABLE `studentinfo`
  ADD CONSTRAINT `studentinfo_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);

--
-- Constraints for table `submissions`
--
ALTER TABLE `submissions`
  ADD CONSTRAINT `submissions_ibfk_1` FOREIGN KEY (`studentUserID`) REFERENCES `users` (`userID`),
  ADD CONSTRAINT `submissions_ibfk_2` FOREIGN KEY (`sharedFolderID`) REFERENCES `sharedfolders` (`sharedFolderID`) ON UPDATE CASCADE;

--
-- Constraints for table `uploads`
--
ALTER TABLE `uploads`
  ADD CONSTRAINT `uploads_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);

--
-- Constraints for table `useractivitylogs`
--
ALTER TABLE `useractivitylogs`
  ADD CONSTRAINT `useractivitylogs_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);

--
-- Constraints for table `useractivityproperties`
--
ALTER TABLE `useractivityproperties`
  ADD CONSTRAINT `useractivityproperties_ibfk_1` FOREIGN KEY (`activityID`) REFERENCES `useractivitylogs` (`activityID`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`userType`) REFERENCES `usertypes` (`userTypeID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
