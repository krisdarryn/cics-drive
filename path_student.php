<?php
/**
 * @package system
 * @subpackage paths
 */

respond('/*', function ($request, $response) { 
	$response->redirect('/student/drive');
});

respond('/item_recovery', function($request, $response){
    require_once PROCESS.'student/ItemRecoveryProcess.php';
    new ItemRecoveryProcess($request, $response);
});

with('/account', function(){

	respond('/edit', function ($request, $response) { 
		require_once PROCESS.'student/EditAccount.php';
		new EditAccount($request, $response);
	});
	
	respond('/edit/login_details', function ($request, $response) { 
		require_once PROCESS.'student/EditLoginDetails.php';
		new EditLoginDetails($request, $response);
	});
	
	respond('/edit/security_questions', function ($request, $response) { 
		require_once PROCESS.'student/EditQuestions.php';
		new EditQuestions($request, $response);
	});

});

with('/drive', function () {
	
        respond('/*', function ($request, $response) { 
	    require_once PROCESS.'student/DriveProcess.php';
	    new DriveProcess($request, $response);
		});

	respond('/[*]', function ($request, $response) { 
	    require_once PROCESS.'student/DriveProcess.php';
	    new DriveProcess($request, $response);
	});

});


with('/editor', function () {
	respond('/[*]', function ($request, $response) { 
	    require_once PROCESS.'student/StudentEditorProcess.php';
	    new StudentEditorProcess($request, $response);
	});

	respond('/*', function ($request, $response) { 
	    require_once PROCESS.'student/StudentEditorProcess.php';
	    new StudentEditorProcess($request, $response);
	});
});

with('/[shared]/[i:userID]', function () {
  respond('/*', function ($request, $response) {
      require_once PROCESS.'student/ViewSharedFolder.php';
      new ViewSharedFolder($request, $response);
  });
  respond('/[:root]', function ($request, $response) {
      require_once PROCESS.'student/ViewSharedFolder.php';
      new ViewSharedFolder($request, $response);
  });
  
  respond('/Publications/[i:pubID]', function ($request, $response) {
    $response->redirect(SITE_URL.'student/publication/'.$request->pubID);
    die();
  });
  respond('/[:root]/[**:path]', function ($request, $response) {
      require_once PROCESS.'student/ViewSharedFolder.php';
      new ViewSharedFolder($request, $response);
  });

});

respond('/publication/[i:id]', function($request, $response){  
  require_once PROCESS.'student/ViewPublication.php';
  new ViewPublication($request, $response);
});