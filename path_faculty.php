<?php

respond('/*', function ($request, $response) { 
	$response->redirect('/faculty/drive');
});

respond ('/submission/report/[i:id]', function($request, $response){
  require_once PROCESS.'faculty/SubmissionReport.php';
  new SubmissionReport($request, $response);
});

respond('/item_recovery', function($request, $response){
    require_once PROCESS.'faculty/ItemRecoveryProcess.php';
    new ItemRecoveryProcess($request, $response);
});

with('/archive', function(){
  respond('/*',function($request, $response){
      require_once PROCESS.'faculty/ArchiveProcess.php';
      new ArchiveProcess($request, $response);
  });
  respond('/[*:archiveName]',function($request, $response){
      require_once PROCESS.'faculty/ArchiveProcess.php';
      new ArchiveProcess($request, $response);
  });
});


with('/account',function(){
		respond('/edit', function ($request, $response) { 
		require_once PROCESS.'faculty/EditAccount.php';
		new EditAccount($request, $response);
	});
	
	respond('/edit/login_details', function ($request, $response) { 
		require_once PROCESS.'faculty/EditLoginDetails.php';
		new EditLoginDetails($request, $response);
	});
	

});

respond('/publications', function ($request, $response) { 
    require_once PROCESS.'faculty/ViewPublications.php';
    new ViewPublications($request, $response);
});

with('/drive', function () {

	respond('/*', function ($request, $response) { 
	    require_once PROCESS.'faculty/DriveProcess.php';
	    new DriveProcess($request, $response);
		});

	respond('/[*]', function ($request, $response) { 
	    require_once PROCESS.'faculty/DriveProcess.php';
	    new DriveProcess($request, $response);
	});
	
});

with('/editor', function () {
	respond('/[*]', function ($request, $response) { 
	    require_once PROCESS.'faculty/EditorProcess.php';
	    new EditorProcess($request, $response);
	});

	respond('/*', function ($request, $response) { 
	    require_once PROCESS.'faculty/EditorProcess.php';
	    new EditorProcess($request, $response);
	});
});

with('/compare', function () {
	respond('/*', function ($request, $response) { 
	    require_once PROCESS.'faculty/CompareProcess.php';
	    new CompareProcess($request, $response);
        });
        
	respond('/[*:path]', function ($request, $response) { 
	    require_once PROCESS.'faculty/CompareProcess.php';
	    new CompareProcess($request, $response);
        });
});

with ('/publication', function(){
  respond('/*', function($request, $response){
          require_once PROCESS.'faculty/PublishProcess.php';
          new PublishProcess($request, $response);

  });
  respond('/[i:id]', function($request, $response){
          require_once PROCESS.'faculty/PublishProcess.php';
          new PublishProcess($request, $response);

  });
});
