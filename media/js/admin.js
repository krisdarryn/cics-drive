if( window.location.pathname.indexOf('admin/logs')!= -1 ){
    $('#startdate').appendDtpicker({dateOnly:true, dateFormat: 'YYYY/MM/DD'});
    $('#enddate').appendDtpicker({dateOnly:true, dateFormat: 'YYYY/MM/DD'});
}

//$('#createStudent').click(function(){
//	$.modal.close();
//});

$(".sortable").click(function(){
    //find index
    var clickedIndex = 0;
    var clickedItem = this;
    
    $(".sortable").each(function(index, element){
        if (clickedItem === element) {
            clickedIndex = index+1;
            return;
        }
    });
    
    if ($("#sortColumn").val() == clickedIndex) {
        var sortDirection = $("#sortOrder").val();
        if (sortDirection == "ASC") {
            $("#sortOrder").val("DESC");
        } else {
            $("#sortOrder").val("ASC");
        }
    }
    
    $("#sortColumn").val((clickedIndex));
    $("#submitButton").click();
});


$(".btnStatus").click(function(){
    var firstName = $(this).parent().siblings(".firstNameColumn").html();
    var lastName = $(this).parent().siblings(".lastNameColumn").html();
    var nameString = lastName + ', ' + firstName + "'s";
    var status = $(this).attr('data-user-status');
    $("#userFullName").html(nameString);
    $("#userIdForStatus").val($(this).attr('data-user-id'));
    $("input[name=setUserStatus]:checked").prop('checked', false);
    $("input[name=setUserStatus][value="+status+"]").prop('checked', true);
});

/**
*	view user drive
*/
var customHeight = window.outerHeight-150;
var customWidth = window.outerWidth-250;

$('.view_drive').click(function(){
	var id = $(this).children('input').val();
	
	$('#modalviedrive').css({width:customWidth+'px',height:customHeight+'px'});
	$('#iframeviewdrive').css({width:100+'%',height:100+'%','border-radius':'4px'});
	$('#iframeviewdrive').contents().find('body').empty();
	$('#iframeviewdrive').contents().find('body').append('<span style="text-align:center;font-size:12px;font-family:verdana;" >Loading contents please wait....</span>');
	
	$('#viewdriveid').val(id);
	$('#viewdrivebutton').click();
	$('#modalviedrive').modal();
	userviewdriveflag = true;
});

/**
*	view user details
*/
$('.view_user_detail').click(function(){
	var id = $(this).children('input').val();
	
	$('#modalviewuserdetail').css({width:(customWidth-400)+'px',height:(customHeight-100)+'px'});
	$('#iframeviewuserdetail').css({width:100+'%',height:100+'%','border-radius':'4px'});
	$('#iframeviewuserdetail').contents().find('body').empty();
	$('#iframeviewuserdetail').contents().find('body').append('<span style="text-align:center;font-size:12px;font-family:verdana;" >Loading contents please wait....</span>');
	
	$('#viewuserdetailid').val(id);
	$('#viewuserdetailbutton').click();
	$('#modalviewuserdetail').modal();
});


/**
 * file activity
 *
$('#usertypes').hide();
$('#category').hide();
$('#date').hide();

$('a').click(function(e){
    var ob = $(this).children('span');
    
    if( ob.hasClass('expand') ){
        if($(this).hasClass('usertypes')){
            $(this).siblings('div #usertypes').slideToggle(300);
        }
        if($(this).hasClass('category')){
            $(this).siblings('div #category').slideToggle(300);
        }
        if($(this).hasClass('date')){
            $(this).siblings('div #date').slideToggle(300);
        }
    }
});*/