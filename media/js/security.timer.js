
var name = $('input[name=InputName]').val();
var seconds = $('.timer #seconds').html() - 0;
$("input[name="+name+"]").focus();
var count = setInterval(countDown, 1000);
function countDown()
{
	
	if (--seconds <= 0) {
		$("button[name=submit]").attr('disabled', 'disabled');
		$("input[name="+name+"]").attr('disabled', 'disabled');
		window.location.reload();
		clearInterval(count);
	}
	$('.timer #seconds').html(seconds);
	
}