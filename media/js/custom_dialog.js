/**
*	@Object - custom dialog for confirmation and alert
*/
var CustomDialog = {
	INFO:'&#59141;', // information icon - CONSTANT
	WARNING:'&#9888;', // warning icon - CONSTANT
	ERROR:'&#59140;', // error icon - CONSTANT
	YESNO:9, //yes and no option - CONSTANT
	OKCANCEL:10, // ok or cancel option - CONSTANT 
	itemName:'', // holds the drive item name - privete variable
	setName: function(name){ //setting the selected drive item name
		this.itemName = name;
	},
	getName: function(){ // getting the selected drive item name
		return this.itemName;
	},
	removeCurrentDialog:function(){ // removing the dialog
		$('#dialogAlert').remove();
		$('#dialogConfirm').remove();
	},
	show: function(){ // show dialog
		$('#dialogConfirm').modal({escapeClose: false,
						showClose: false,
						clickClose: false,
						showSpinner: false});
		$('#dialogAlert').modal({escapeClose: true,
						showClose: false,
						clickClose: false,
						showSpinner: false});				
	},
	/**
	*	@Method Alert - sets an alert dialog
	*	@Parameter 	icon - icon for the dialog
	*				title - title for the dialog
	*				message - content for the dialog
	*/
	Alert:function(icon,title,message){
		this.removeCurrentDialog();
		var dialog = '<div id="dialogAlert" class="modal">'
								+'<div class="modal_icon">'+icon+'</div>'
								+'<div class="modal_Content"><span class="modal_header">'+title+'</span><br>'+message+'</div>'
								+'<span class="modal_Button"><button class="blue" id="closeBtn">OK</button></span>'
								+'</div>';
								
		$('body').prepend(dialog);	
		this.show();
		$('#closeBtn').click(function(){
			$.modal.close();	
		});
	},
	/**
	*	@Method Confirm - sets a confirm dialog
	*	@Parameter 	icon - icon for the dialog
	*				title - title for the dialog
	*				message - content for the dialog
	*				type - option for the dialog e.g. YES or NO
	*				mode - e.i. 'remove','download','delete','restore'
	*/
	Confirm:function(icon,title,message,type,mode,name){
		var btn = (type == this.YESNO)?'<span class="modal_Button"><button class="green" id="modal_Yes" class="blue"><span class="icon">&#10003; </span> Yes</button><button class="red" id="modal_No"><span class="icon">&#10006; </span> No</button></span>':'<span class="modal_Button"><button class="green" id="modal_Yes" class="blue"><span class="icon">&#10003; </span> Ok</button><button class="red" id="modal_No"><span class="icon">&#10006; </span> Cancel</button></span>';
		this.removeCurrentDialog();
		var dialog = '<div id="dialogConfirm" class="modal">'
						+'<div class="modal_icon">'+icon+'</div>'
						+'<div class="modal_Content"><span class="modal_header">'+title+'</span><br>'+message+'</div>'
						+btn
						+'</div>';
		$('body').prepend(dialog);	
		this.show();
		this.modal_Button_Event(mode,name);
	},
	modal_Button_Event:function(mode,data){ // setting events of button in the dialog
                
                $('#modal_No').click(function(){
                       $.modal.close();
		});
		
		$('#modal_Yes').click(function(){
                        
			$('html').off('click');
			if( mode === 'remove'){
				$('#remove_button').click();
			}
			if( mode === 'download'){
				$('#download_button').click();
			}
			if( mode === 'restore'){
				$('#restore_button').click();
			}
			if( mode === 'delete' ){
				$('#delete_button').click();
			}
                        if( mode === 'archive' ){ //for archiving
                            $('#archiveModal').modal({escapeClose: false,
                                                        clickClose: false,
                                                        showSpinner: false,
                                                        showClose: false});
                            
                             init_archive();
                        }
                        if( mode === 'backup'){
                            $('#backupmodal').modal({escapeClose: false,
                                                        clickClose: false,
                                                        showSpinner: false,
                                                        showClose: false});
                            init_tables();
                            return;
                        }
                        
                        if( mode == 'backup_restore'){
                            $('#restoremodal').modal({escapeClose: false,
                                                        clickClose: false,
                                                        showSpinner: false,
                                                        showClose: false});
                            init_restore();
                            return;
                        }
                        
                        if( mode == 'backup_delete'){
                            $('#delete').click();
                            return;
                        }
			deselecting();
		});	
	}
}