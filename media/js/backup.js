var url = window.location.pathname;
var request;
var tableCount;
var tableList = [];
var avatarList = [];
var avatarCount;
var driveList = [];
var driveCount;
var ctr;
var restoreName;

$('#create_backup').click(function(){
    CustomDialog.Confirm(CustomDialog.INFO, "Your are attempting to backup the current Avatars, Drive and Database tables.", "Are you sure?", CustomDialog.YESNO, "backup");
});


$('#backupok').click(function(){
    if($('#backupok').text() == 'Start'){
        request = $.post(url,{log:true,tables:tableList,avatars:avatarList,drives:driveList});
        
        $('#backupok').text('Initializing...');
        
        request.success(function(result,status,xhr){
            if(status == 'success'){
                if(JSON.parse(result)){
                    backup_tables(tableList);
                    return;
                }
            }
            
        });
    }
    
    if($('#backupok').text() == 'Done'){
        window.location.href = url;
    }
});

$('#backupcancel').click(function(){
    if($('#backupok').text() == 'Done'){
        window.location.href = url;
    }else{
        request.abort();
    }
    
    $.modal.close();
    clear();
});


$('#restorecancel').click(function(){
    clearR();
    $.modal.close();
});

$('#restoreok').click(function(){
    if( $(this).text() == 'Start' ){
        start_restore();
    }
    
    if( $(this).text() == 'Done' ){
        clearR();
        $.modal.close();
    }
    
});

function clearR(){
    var robj = $('#restoremodal');
    robj.find('span').text('');
    robj.find('h4').text('Restoring');
    robj.find('div:first').remove('backup_loader');
}

function clear(){
    $('.tables span:last').text('');
    $('.avatars span:last').text('');
    $('.drives span:last').text('');
    
    $('.tables').removeClass('backup_loader');
    $('.avatars').removeClass('backup_loader');
    $('.drives').removeClass('backup_loader');
}

/**
 * @method init_tables
 */
function init_tables(){
    request = $.post(url,{initTable:true});
    
    request.process = function(){
      $('#backupok').text('Please wait...');
      $('.tables').addClass('backup_loader');
      $('.tables span:last').text('Initializing...');
    };
    
    request.process();
    request.success(function(result,status,xhr){
        if(status == 'success'){
            var tab = JSON.parse(result);
            
            tab.forEach(function(value){
                tableList.push(value); 
            });
            tableCount = tab.length;
            
            $('.tables').removeClass('backup_loader');
            $('.tables span:last').text(tableCount+' table(s) to be back up...');
            init_avatar();
        } 
    });
}
/**
 * @method init_avatart
 */
function init_avatar(){
    request = $.post(url,{initAvatar:true});
    
    request.process = function(){
      $('.avatars').addClass('backup_loader');
      $('.avatars span:last').text('Initializing...');
    };
    
    request.process();
    request.success(function(result,status,xhr){
        if(status == 'success'){
            
            var avt = JSON.parse(result);
            
            avt.forEach(function(value){
                avatarList.push(value); 
            });
            avatarCount = avt.length;
            
            $('.avatars').removeClass('backup_loader');
            $('.avatars span:last').text(avatarCount+' avatar(s) to be back up...');
            init_drive();
        } 
    });
}

/**
 * @method init_drive
 */
function init_drive(){
    request = $.post(url,{initDrive:true});
    
    request.process = function(){
      $('.drives').addClass('backup_loader');
      $('.drives span:last').text('Initializing...');
    };
    
    request.process();
    request.success(function(result,status,xhr){
        if(status == 'success'){
            
            var drv = JSON.parse(result);
            var count=[];
            driveCount = 0;
            
            for(var i=0;i<Object.keys(drv).length;i++){
                driveList.push( drv[i].driveItem );
                count.push( drv[i].countContents );
            }
            
            count.forEach(function(value){
                driveCount += value;
            });
            
            $('#backupok').text('Start');
            $('.drives').removeClass('backup_loader');
            $('.drives span:last').text(driveCount+' drive items(s) to be back up...');
            
        } 
    });
    
}

function backup_tables(tableName){
    request = $.post(url,{backupTable:tableName});
    
    request.process = function(){
        $('#backupok').text('Please wait...');    
        $('.tables').addClass('backup_loader');
        $('.tables span:last').text(tableCount+' tables(s)...');
    };
    
    request.process();
    request.success(function(result,status,xhr){
        if(status == 'success'){
            console.log(result);
            var res = JSON.parse(result);
            
            if(res){
                $('.tables').removeClass('backup_loader');
                $('.tables span:last').addClass('backup_icon');
                $('.tables span:last').html('&#10003; Done backing up DB Tables.');
                ctr = 0;
                request=null;
                backup_avatars(avatarList[ctr]);
                return;
            }
        }
    });
    
}

function backup_avatars(avatarName){
    request = $.post(url,{backupAvatar:avatarName});
    
    request.process = function(){
        $('.avatars').addClass('backup_loader');
        $('.avatars span:last').text(avatarCount+' avatar(s) remaining...');
    };
    
    request.process();
    request.success(function(result,status,xhr){
        if(status == 'success'){
            var res = JSON.parse(result);
          
            if(res[0]){
                avatarCount -= res[1];       
                ctr++;

                if(avatarCount>0){
                    $('.avatars').addClass('backup_loader');
                    $('.avatars span:last').text(avatarCount+' avatar(s) remaining...');
                }else{
                    $('.avatars').removeClass('backup_loader');
                    $('.avatars span:last').addClass('backup_icon');
                    $('.avatars span:last').html('&#10003; Done backing up Avatars.');
                    ctr = 0;
                    request=null;
                    
                    backup_drives(driveList[ctr]);
                    return;
                }
                
                if(ctr<avatarList.length){
                    backup_avatars(avatarList[ctr]);
                }
            }
        }
    });
}

function backup_drives(driveName){
    request = $.post(url,{backupDrive:driveName});
    
    request.process = function(){
        $('.drives').addClass('backup_loader');
        $('.drives span:last').text(driveCount+' drive item(s) remaining...');
    };
    
    request.process();
    request.success(function(result,status,xhr){
        if(status == 'success'){
            console.log(result);
            var res = JSON.parse(result);
            
            if(res[0]){
                driveCount -= res[1];       
                ctr++;

                if(driveCount>0){
                    $('.drives').addClass('backup_loader');
                    $('.drives span:last').text(driveCount+' drive item(s) remaining...');
                }else{
                    $('.drives').removeClass('backup_loader');
                    $('.drives span:last').addClass('backup_icon');
                    $('.drives span:last').html('&#10003; Done backing up Drive.');
                    $('#backupok').text('Done');
                }
                
                if(ctr<driveList.length){
                    backup_drives(driveList[ctr]);
                }
            }
        }
    });
}

$('.download_backup').click(function(){
    var name = $(this).parent().siblings('td').find('span').text();
    $('#download').siblings('input[type=hidden]').val(name);
    $('#download').click();
});

$('.restore_backup').click(function(){
    restoreName = $(this).parent().siblings('td').find('span').text();
    $('#restoremodal h4').text($('#restoremodal h4').text()+" "+restoreName);
    CustomDialog.Confirm(CustomDialog.INFO, "Restoring back up "+restoreName, "Are you sure?", CustomDialog.YESNO, "backup_restore",restoreName);
});

$('.delete_backup').click(function(){
    var name = $(this).parent().siblings('td').find('span').text();
    $('#delete').siblings('input[type=hidden]').val(name);
    
    CustomDialog.Confirm(CustomDialog.INFO, "Delete back up "+name, "Are you sure?", CustomDialog.YESNO, "backup_delete");
});

function init_restore(){
    request = $.post(url,{init_restore:true,restore:restoreName});
    
    request.process = function(){
       $('#restoremodal div:first').addClass('backup_loader');
       $('#restoremodal span').text('initializing...');
       $('#restoreok').text('Please wait...');
    };
    
    request.process();
    request.success(function(result,status,xhr){
        if(status == 'success'){
            var obj = JSON.parse(result);
            $('#restoremodal div:first').removeClass('backup_loader');
            $('#restoremodal span').text('Ready to restore.');
            $('#restoreok').text('Start');
        }
    });
}

function start_restore(){
    request = $.post(url,{start_restore:true,restore:restoreName});
    
        request.process = function(){
       $('#restoremodal div:first').addClass('backup_loader');
       $('#restoremodal span').text('Restoring...');
       $('#restoreok').text('Please wait...');
    };
    
    request.process();
    request.success(function(result,status,xhr){
        if(status == 'success'){
            console.log(result);
            var obj = JSON.parse(result);
            $('#restoremodal div:first').removeClass('backup_loader');
            $('#restoremodal span').text('Restored successfully.');
            $('#restoreok').text('Done');
        }
    });
}