{
var items = new Array();
var newFolder = null;
var selectedItems;
$("#btnToolbarMove").click(function(){
  items = new Array();
  if ($("#fileSystem li.selected").length > 0) {
    $("#fileSystem li.selected").each(function(index, element){
      var path = $(element).attr('data-rel-path');
      items.push(path);
    });
    $('#rootFolderListItem_move').children('ul').remove();
    $("#moveItemModal").modal();
    
    selectedItems = items;
    getDirFolders4Move($('input[name=root]').val(), $('#rootFolderListItem_move'));
    

    
  } else {
    CustomDialog.Alert(CustomDialog.INFO, "Nothing selected", "Please select the items to move.");
  }
});
var urlStack = new Array();
var getDirFolders4Move = function(url, element) {
        
	var jqXHR1 = $.getJSON(url, {contents: 'true'},function(data) {
         
		urlStack.push(url);
		var items = [];
		var folders = [];
		var files = [];
                $.each(data, function(key, val) {
                  var same = false;
                  if (selectedItems) {
                    $.each(selectedItems, function(index, element){
                     // console.log('path:'+ val.path);
                     // console.log('element:'+ element);
                      if (val.path == element) {
                        same = true;
                        return;
                      }
                    });
                    
                  }


                  if (val.type == "Folder" && !(val.name == '.publications') && !same) {
                    folders.push('<li class="' + val.type + '"><a rel="' + url + val.name + '/" href="javascript:;" data-name="' + val.name + '"><span class="icon">&#128193;</span>' + val.name + '</a></li>');
                  }
                });
	  	items = folders.concat(files);
		var appendList = $('<ul/>', {
		'class': 'subMenu',
		html: items.join('')
		}).appendTo(element);
		
		bindListItemListener4Move($(appendList).children('li.Folder').children('a'));
		loader = $("#moveItemModal #loaderImage").detach();
		$.modal.resize();
	
	});
	jqXHR1.fail(function(a,b,c) {
		console.log('aw>>> '+b);
	});

}
function bindListItemListener4Move(element) {
         
	$(element).toggle(
		function(){
			var url = $(this).attr('rel');
                        newFolder = url;
			$(this).append(loader);
			getDirFolders4Move(url, $(this).parent());
			$(this).parent().addClass('hasSubMenu');
			$(this).addClass('animated');
			$(this).addClass('flash');
		},
		function(){
			$(this).siblings('.subMenu').remove();
			$(this).removeClass('animated');
			$(this).removeClass('flash');
			$(this).parent().removeClass('hasSubMenu');
		}
	);
        $(element).click(function(){
            newFolder = $(this).attr('rel');
            var name = $(this).attr('data-name');
            $("#moveToFolderName").html('"'+name+'"');
            $('li.Folder').children('a').css('background-color', '#FFF');
            $(this).css('background-color', '#F0F0F0'); 
           // console.log(this);
        });
}
$("html .DriveItemsList li.Folder a").click(function(){
    var name = $(this).attr('data-name');
    newFolder = $(this).attr('rel');
    $("#moveToFolderName").html('"'+name+'"');  
   // console.log(this);
    $('li.Folder').children('a').css('background-color', '#FFF');
    $(this).css('background-color', '#F0F0F0'); 
});

$("#btnMove").click(function(){
  if (items.length > 0) {
    post = {"moveItems": items};
    xhr = $.post(newFolder, post);
    xhr.done(function(data, message, xhr){
	console.log(data);
      data =$.parseJSON(data);
      console.log(data);
      if (confirm('Go to specifed folder?')) {
        window.location = newFolder;
      } else {
        $.modal.close();
        $.each(data, function(index, movedItem){
          if (movedItem.status == true) {
            var elemString = "#fileSystem li[data-rel-path='"+movedItem.sourcePath+"']";
            $(elemString).remove();
          }
        });
        
      }
    });
    xhr.error(function(data, message, xhr){
      data =$.parseJSON(xhr);
      //console.log(data)
    });
    
  }
});

}