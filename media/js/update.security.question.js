	$('.forEnable').click(function(){
		var questions = $(this).parent('td').parent('tr').find('#questions').text().trim();
		var enable = $(this).prop('checked')?'A':'I';
                var id = $(this).attr('data-qid');
		request(enable,questions, id);
	});

	$('.forDisable').click(function(){
		var questions = $(this).parent('td').parent('tr').find('#questions').text().trim();
		var disable = $(this).prop('checked')?'I':'A';
                var id = $(this).attr('data-qid');

		request(disable,questions, id);
	});
	
	function request(action,question, id){
		var xhr = $.post('/admin/security_questions', {action:action,question:question,id:id});
		xhr.done(function(data){
			console.log(data);
		});
	}