$("#commenterList").select2();
$("#btnMute").click(function(){
    $("#mutingModal").modal();
});

$("#toolbarPublish").click(function(){
	if ($("li.file.selected").size() == 1){
            var editable = false;
                var editables = new Array('c','cpp','cs','css','h','htm','html','java','js','json','php','sql','vb','xml');
                $.each(editables, function(index, element){
                  if ($("li.file.selected").hasClass(element)) {
                    publish();
                    editable = true;
                    return true;
                  }
                });
                if (!editable) {
                  CustomDialog.Alert(CustomDialog.ERROR, 'Cannot Publish this type of file', '');
                }

	} else {
		alert("Please select 1 file to publish.");
	}

});

function publish(){
	$("#publishModal").modal();
	$("#txtPublishDescription").val("");
	var file = $("li.file.selected span#driveItem").html();
	$("#txtPublishName").val(file);
	$("#publishPath").val($("li.file.selected").attr('data-rel-path'));
}

var codeEditor = document.getElementById('codeEditor');
{ // code editor main
	var myCodeMirror = CodeMirror.fromTextArea(codeEditor,
		{	
                    lineNumbers: true,
                    readOnly: true,
		    theme: 'eclipse',
		    mode: $("#mime").val()
		}
	);
} // end of code editor

$(".deleteComment").click(function(){
    $("body").addClass("wait");
    var item = $(this);
    var id = item.attr('data-comment-id');
    var request = $.post(window.location, {removeComment: id});
    request.done(function(data, message, xhr){
        data = $.parseJSON(data);
        if (data.status == true) {
            item.parent().remove();
        } else {
            alert('cannot remove comment at this time');
        }
    });
    request.error(function(){
        alert('error trying to remove content.');
    });   
    request.always(function(){
        $("body").removeClass("wait");
    });
});
$("#commenterList").change(function(){
    var id = $(this).val();
   
    var request = $.post('', {muteUser: id});
    request.done(function(data, message, xhr){
        data = $.parseJSON(data);
        
        if (data.status == true) {
            $("#myTable").append('<tr><td>'+data.user+'</td><td><a href="javascript:;" data-id="'+id+'" class="button small unmute">unmute</a></td></tr>');
            window.location = window.location;
        } else {
            alert('cannot mute at this time');
        }
    });
});
$('body').on('click', 'a.unmute', function() {
    var trigger = this;
    var id = $(this).attr('data-id');
    var request = $.post('', {unmuteUser: id});
    request.done(function(data, message, xhr){
        data = $.parseJSON(data);
        if (data.status == true) {
            $(trigger).parent().parent().remove();
            window.location = window.location;
        } else {
            alert('cannot unmute at this time');
        }
    });

});