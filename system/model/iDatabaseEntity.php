<?php
/**
 * @package model
 */
require_once LIBRARY.'Database.php';
interface DatabaseEntity {
        const SQL_DATETIME_FORMAT = 'Y-m-d G:i:s';
        
	public static function getById($id);
	public function updateRecord();
}