<?php
/**
 * @package model
 */

require_once(MODEL.'iDatabaseEntity.php');
require_once LIBRARY.'Database.php';
require_once MODEL.'User.php';
 
class UserType implements DatabaseEntity {

	const TABLE = "usertypes";
	
	const STUDENT = 3;
	const FACULTY = 2;
	const ADMIN = 1;
	const GUEST = 0;	

	private $userTypeID;
	private $description;
	private $storageCapacity;
	private $status;
        
        

	
	public function __construct($arr = null) {
		if ($arr != null){
			$this->setUserTypeID($arr['userTypeID']);
			$this->setDescription($arr['description']);
			$this->setStorageCapacity($arr['storageCapacity']);
			$this->setStatus($arr['status']);
		}
	}
	
	public function setUserTypeID($userTypeID) {
		$this->userTypeID = $userTypeID;
	}
	
	public function setDescription($description){
	$this->description = $description;
	}
	
	public function setStorageCapacity($storageCapacity){
	$this->storageCapacity = $storageCapacity;	
	}
	
	public function setStatus($status){
          $this->status = $status;
	}
	
	public function getUserTypeID() {
		return $this->userTypeID;
	}
	
	public function getDescription(){
		return $this->description;
	}
	
	public function getStorageCapacity(){
		return $this->storageCapacity;
	}
	
	public function getStatus(){
		return $this->status;
	}

        public function updateRecord() {
          return false;
        }

        public static function getById($id) {
          $sql = 'SELECT * FROM ' . self::TABLE . ' WHERE userTypeID = :id LIMIT 1';

          $db = Database::getInstance();
          $db->query($sql);
          $db->bind(':id', $id);
          $result = $db->single();
          if (count($result) > 0) {
            return new UserType($result);
          }
          return null;
        } 
        
        public function __toString() {
          return $this->getDescription();
        }
	
	//get usertype instance
	public static function userTypeInstance($usertype){
		$db = Database::getInstance();
		
		$statement = "SELECT * FROM ".self::TABLE." WHERE userTypeID = :usertype";
		
		$db->query($statement);																							
		$db->bind( ':usertype', $usertype );
		$result = $db->single();
			
		if($db->rowCount() == 0)
			throw new UserException('No such user type', 1);
			
		$usertype = new UserType($result);
		return $usertype;
	}
}
