<?php
require_once MODEL.'iDatabaseEntity.php';
require_once MODEL.'FileActivityProperties.php';
require_once MODEL.'User.php';

class UserActivityProperties implements DatabaseEntity{
        
        /**
         * TABLE
         */
         const TABLE='useractivityproperties';
         
        /**
         *  Constant variables 
         *  For Activate
         */
         const ACTIVATE_ACCOUNT = 010;
         
         /**
          * For Edit
          */
         const UPDATE_LASTNAME=011;
         const UPDATE_FIRSTNAME=012;
         const UPDATE_MIDDLEINITIAL=013;
         const UPDATE_FACULTY_IDNUMBER=014;
         const UPDATE_AVATAR = 015;
         
         /**
          * For Recover
          */
         const RECOVER_PASSWORD=021;
         
         /**
          * For Change
          */
         const CHANGE_EMAIL=031;
         const CHANGE_PASSWORD=032;
         const CHANGE_SECURITY_ANSWER=033;
         const CHANGE_STATUS_ACTIVE = 034;
         const CHANGE_STATUS_SUSPICIOUS = 035;
         const CHANGE_STATUS_SUSPENDED = 036;
         
         /**
          * For Reset
          */
         const RESET_PASSWORD=041;
         
         /**
          * FOR ANSWER
          */
         const ANSWER_SECURITY_QUESTION=051;
         
         /**
          * FOR ADD
          */
         const ADD_SYSTEM_WIDE_FILE_FILTER=061;
         const ADD_SHARED_FILE_FILTER=062;
         const ADD_SECURITY_QUESTION=063;
         
         /**
          * FOR REMOVE
          */
         const REMOVE_SYSTEM_WIDE_FILE_FILETER=071;
         const REMOVE_SHARED_FILE_FILTER=072;
         
         /**
          * FOR CREATE ACCOUNT
          */
         const CREATE_ACCOUNT_STUDENT=074;
         const CREATE_ACCOUNT_FACULTY=075;
         const CREATE_ACCOUNT_ADMIN=076;
        
         /**
          * FOR  ENABLE
          */
         const ENABLE_SECURITY_QUESTION=091;
         const ENABLE_SYSTEM_WIDE_FILE_FILTER_STUDENT=092;
         const ENABLE_SYSTEM_WIDE_FILE_FILTER_FACULTY=093;
         
         /**
          * FOR  DISABLE
          */
         const DISABLE_SECURITY_QUESTION=0101;
         const DISABLE_SYSTEM_WIDE_FILE_FILTER_STUDENT=0102;
         const DISABLE_SYSTEM_WIDE_FILE_FILTER_FACULTY=0103;
         
         
/********************/         
         const ACCOUNT_UPDATE_OWNER = 010;
         const SECURITY_QUESTION_ID = 011;
         const USER_ID = 012;
         const USER_STATUS = 013;
         const FILE_FILTER = 014;
/********************/                  
         
         /**
         * @var
         */
        private $activityID;
        private $id;
        private $value;
         
        /**
	*	Constructor
	*	@Parameter array
	*/
	public function __construct( $arrg=null ){
		if($arrg!=null){
			$this->setActivityID($arrg['activityID']);
			$this->setID($arrg['id']);
			$this->setValue($arrg['value']);
		}
	}
	
	/**
	*	@Method setActivityID
	*	@Parameter int
	*/
	public function setActivityID( $activityID ){
		$this->activityID = $activityID;
	}
	
	/**
	*	@Method setID
	*	@Parameter int
	*/
	public function setID( $id ){
		$this->id = $id;
	}
	
	/**
	*	@Method setValue
	*	@Parameter string
	*/
	public function setValue( $value ){
		$this->value = $value;
	}
	
	/**
	*	@Method getActivityID
	*/
	public function getActivityID(){
		return $this->activityID;
	}
	
	/**
	*	@Method getID
	*/
	public function getID(){
		return $this->id;
	}
	
	/**
	*	@Method getValue
	*/
	public function getValue(){
		return $this->value;
	}
        
        /**
	*	@Method logProperties -logging properties of the logged user activities
	*	@Parameter int,int,string
	*	@Return void
	*/
	public static function logProperties($actID,$indicator,$value){
		$db = Database::getInstance();
		$statement = 'INSERT INTO '.self::TABLE.' (activityID, id, value) VALUES (:actID, :indicator, :value)'; 
		$db->query($statement);
		$db->bind(':actID',$actID);
		$db->bind(':indicator',$indicator);
		$db->bind(':value',$value);
		$db->execute();
                return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
	}
        
        //@Override
	public static function getByID( $activityID ) {
		$db = Database::getInstance();
		$db->query('SELECT * FROM '.self::TABLE.' WHERE activityID = :id');
		$db->bind(':id', $activityID);
		$result = $db->single();
		if ($db->rowCount() == 0)
			throw new UserException('No such Activity Log Properties Record', 1);
		$userlogproperties = new UserActivityProperties($result);
		return $userlogproperties;
	}
	
	//@Override
	public function updateRecord() {
		$db = Database::getInstance();
		$statement = "UPDATE ".self::TABLE.' SET id = :id,
                                value = :value
                                WHERE activityID = :activityID';
		$db->query($statement);
		$db->bind(':id',$this->getID());
		$db->bind(':value',$this->getValue());
		$db->bind(':activityID',$this->getActivityID());
		$db->execute();
		return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
	}
        
        /**
	*	getValue
	*/
	public static function getSpecificValue($actid,$id){
		require_once LIBRARY.'DBQuery.php';
                $db = Database::getInstance();
                
                $query = DBQuery::select('value')->from(self::TABLE)->where('activityID',$actid)->where('id',$id);
                
		$db->query($query->__toString());
		$result = $db->single();
                
                if(count($result)>0){
                    return  $result['value'];
                }
		return null;
	}
        
        public static function getProperties($actid) {
          $sql = 'SELECT * FROM '.self::TABLE.' WHERE activityID = :actid ORDER BY 2';
          $db = Database::getInstance();
          $db->query($sql);
          $db->bind(':actid', $actid);
          $results = $db->resultset();
          $props = array();
          if (count($results) > 0) {
            foreach ($results as $result ) {
              $props[$result['id']] = new UserActivityProperties($result);
            }
          }
          return $props;
        }
        
}
