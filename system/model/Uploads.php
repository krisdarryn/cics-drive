<?php
require_once LIBRARY.'Database.php';

class Uploads implements DatabaseEntity{
	
	/**
	*	Constant - table name
	*/
	const TABLE = 'uploads';

	/**
	* Constants - used in upload status
	*/
	const FILE_FILTER_REJECT = 9;
	const ANTIVIRUS_REJECT = 10;
	const INSUFFICIENT_STORAGE_SPACE = 11;
	const FILE_EXIST = 12;
	const UNKNOWN_FILE = 13;
	const OVERWRITE = 14;
	const UPLOAD_CANCELLED = 15;
	
	/**
	*	@Variables
	*/
	private $uploadID; 
	private $userID;
	private $path;
	private $uploadDate;
	private $status;

	/**
	*	Constructor
	*	@Parameter array
	*/
	public function __construct( $uploads = null ){
		if( $uploads!=null ){
			$this->setUploadID( $uploads['uploadID'] );
			$this->setUserID( $uploads['userID'] );
			$this->setPath( $uploads['path'] );
			$this->setUploadDate( $uploads['uploadDate'] );
			$this->setStatus( $uploads['status'] );
		}
	}
	
	/**
	*	@Method setUploadID
	*	@Parameter int
	*/
	public function setUploadID($uploadID){
		$this->uploadID = $uploadID;
	}
	
	/**
	*	@Method setUserID
	*	@Parameter int
	*/
	public function setUserID($userID){
		$this->userID = $userID;
	}
	
	/**
	*	@Method setPath
	*	@Parameter string
	*/
	public function setPath($path){
		$this->path = $path;
	}
	
	/**
	*	@Method setUploadDate
	*	@Parameter string
	*/
	public function setUploadDate($uploadDate){
		$this->uploadDate = $uploadDate;
	}
	
	/**
	*	@Method setStatus
	*	@Parameter int
	*/
	public function setStatus($status){
		$this->status = $status;
	}
		
	/**
	*	@Method getUploadID
	*	@return int
	*/
	public function getUploadID(){
		return $this->uploadID;
	}
	
	/**
	*	@Method getUserID
	*	@return int
	*/
	public function getUserID(){
		return $this->userID;
	}
	
	/**
	*	@Method getPath
	*	@return string
	*/
	public function getPath(){
		return $this->path;
	}
	
	/**
	*	@Method getUploadDateTime
	*	@return date - format Year/Month/Day Hour/Minute/Second
	*/
	public function getUploadDateTime(){
		$formattedDate = date('Y/m/d H:i:s',strtotime($this->uploadDate));
		return $formattedDate;
	}
	
	/**
	*	@Method getUploadDate
	*	@return date - format Year/Month/Day
	*/
	public function getUploadDate(){
		$formattedDate = date('Y/m/d',strtotime($this->uploadDate));
		return $formattedDate;
	}
	
	/**
	*	@Method getUploadTime
	*	@return date - format Hour/Minute/Second Ante meridiem/Post meridiem
	*/
	public function getUploadTime(){
		$formattedDate = date('H:i:s',strtotime($this->uploadDate));
		return $formattedDate;
	}
	
	/**
	*	@Method getStatus
	*	@return int
	*/
	public function getStatus(){
		return $this->status;
	}
	
	/**
	*	@Method insertUploaded - used to log uploaded items 
	*	@Parameter string,int,int
	*/
	public static function insertUploaded( $path,$userId, $status ){
		$db = Database::getInstance();
		$statement = "INSERT INTO ".self::TABLE." ( uploadID, userID, path, uploadDate, status) VALUES ( DEFAULT, :userid, :path, DEFAULT, :status)";
		$db->query($statement);																							
		$db->bind( ':userid', $userId );
		$db->bind( ':path', $path );
		$db->bind( ':status', $status );
		$db->execute();
		return $db->lastInsertId();
	}
	
	/**
	*	@Method cancelUplaod - update the status of a uploaded file into cancel status
	*	@Parameter string,int,int
	*/
	public static function cancelUplaod( $path, $userId, $status ){
		$db = Database::getInstance();
		$statement = 'UPDATE '.self::TABLE.' SET status="'.Uploads::UPLOAD_CANCELLED.'" WHERE userID=:id AND path=:path AND status=:status';
		$db->query($statement);
		$db->bind( ':id', $userId );
		$db->bind( ':path', $path );
		$db->bind( ':status', $status );
		$db->execute();
	}
	
	//@Override
	public static function getByID( $uploadID ){
		$db = Database::getInstance();
		$db->query('SELECT * FROM '.self::TABLE.' WHERE uploadID = :uploadID');
		$db->bind(':uploadID',$uploadID);
		$result = $db->single();
		if ($db->rowCount() == 0)
			throw new UserException('No such Upload Record', 1);
		$upload = new Uploads($result);
		return $upload;
	}
	
	//@Override
	public function updateRecord(){
		$db = Database::getInstance();
		$statement = 'UPDATE '.self::TABLE.' SET userID = :userID,
												path = :path,
												uploadDate = :uploadDate,
												status = :status
												WHERE uploadID = :uploadID';
		$db->query($statement);	
		$db->bind(':userID',$this->getUserID());	
		$db->bind(':path',$this->getPath());	
		$db->bind(':uploadDate',$this->getUploadDate());	
		$db->bind(':status',$this->getStatus());	
		$db->execute();
		return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
	}
}