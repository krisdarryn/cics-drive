<?php
require_once MODEL.'User.php';
require_once MODEL.'Logins.php';
require_once LIBRARY.'drive/File.php';

class Util{

	/*
	*
	*	setQuestion
	*/
	private function setQuestion($user){
		
		Logins::setQuestions( $user['userId'] );
		$question = Session::get('question');
		Logins::mockAnswers( $question['studentQuestionID'],$user['userId'] );	
	}
	
	/**
	*	convert timestamp time to integer
	*/
	public static function time_to_sec($time) {
		$hours = substr($time, 0, -6);
		$minutes = substr($time, -5, 2);
		$seconds = substr($time, -2);
	
		return $hours * 3600 + $minutes * 60 + $seconds;
	}

	/**
	* get Date
	*/
	public static function getDate($date){
		$formattedDate = date('F d, Y',strtotime($date));
		return $formattedDate;
	}
	
	/**
	*get Time
	*/
	public static function getTime($time){
		$formattedDate = date('h:i a',strtotime($time));
		return $formattedDate;
	}
	
	/**
	*	check duplitcate item inside the drive
	*/
	public static function checkDuplicate($folder,$name){
		$n = $name;
		$type = (strripos($name,'.') > 0)?1:0;//1 for file, 0 for folder
                
		foreach($folder as $key => $it){
			if( strcasecmp($name, $it) == 0 ){
				/** 
				*	when the first ternary operator results into true, it will get the item name. 
				*	Then undergo the second ternary operator to check the type of item, if it is a file it will append '[.[extension]]', 
				*	otherwise it will append an empty string.
				*/
				$name = (strripos($it,']') > 0)?substr($name,0,strripos($name,'[')).( ($type == 1)?'.'.File::getExtension($n):'' ):$name; 
				$temp = (strripos($it,']') > 0)?substr($it,strripos($it,'[')+1,1):0;
				(int)$temp += 1;
				
				if($type === 0){
					$name = $name.'['.$temp.']';
				}else{
					$name = substr($name,0,strripos($name,'.')).'['.$temp.']'.'.'.File::getExtension($n);
				}
			}
		}
		return $name;
	}
	
	/**
	*	@method getFolderPathOnly - use to get the path excluding the filename
	*	@parameter string - path
	*/
	public static function getFolderPathOnly($path){
		
		return strripos($path,'/')>0?'Drive/'.substr($path,1,(strripos($path,'/')-1)):'Drive';
	}
	
	/**
	*	@method getNameOnly - use to get the filename in a path
	*	@parameter  string - path
	*/
	public static function getNameOnly($path){
		return substr($path,strripos($path,'/')+1,strlen($path)); 
	}
	
	/**
	*	@method getItemType - use to get the type of an item
	*	@parameter string - path
	*	@return 'Folder' or 'File' 
	*/
	public static function getItemType($path){
		return strripos($path,'.')>-1?'File':'Folder';
	}
        
        /**
     * @method getFieldType() - returns the data type of a field
     * @param type $table
     * @param type $field
     * @return type 
     */
        public static function getFieldType($table,$field) {
            $db = Database::getInstance();
            $ret = false;   
            $sql = "SHOW COLUMNS FROM ".$table." WHERE Field = '".$field."'";
            try {
                $db->query($sql);
                $result = $db->resultset();
                $ret = $result[0]['Type'];
            } catch (PDOException $e) {
                self::logError($e, "getFieldType", $sql);
            }

            return $ret;
        }
        
       /**
        * @method logFinishArchive - This will put an indicator (DONE or PROCESS) of the item that has been archived/backed up.
        * @param type $file
        * @param type $item
        * @param type $path 
        */
        public static function logFinishArchive($file=null,$item=null,$path=null){
            $str =file_get_contents($file);
            $ar = array_diff(explode("\n", $str), array(""));
            
            
            $index = array_search($item, $ar);
            
            foreach($ar as $key => $a){
                if(strrpos($a, '-DONE') > -1){
                    continue;
                }
                if(strrpos($a,'-PROCESS') > -1){
                    $a = rtrim($a,'-PROCESS');
                }
                
                if($a == $item){
                    $ar[$key] = $a.'-DONE';
                }else{
                    $ar[$key] = $a.'-PROCESS';
                }
            }
            unlink($file);
            $f = fopen($file, 'w');
            foreach($ar as $key => $a){
                if($key < count($ar)){
                    fwrite($f, $a."\n");
                }
            }
            fclose($f);
        }
}


/* Util::getFolderPathOnly($this->reports[$key]['oldPath']) */