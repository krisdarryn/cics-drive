<?php
require_once MODEL.'iDatabaseEntity.php';
require_once MODEL.'FileActivityProperties.php';
require_once MODEL.'User.php';

class FileActivityLogs implements DatabaseEntity {
  
	/**
	*	Constant varialbe
	*	Used by all Users, indicator for Upload/ Edit/ Rename/ Remove/ Delete/ Download/ Create Folder/ Restore
	*/
	const CATEGORY_UPLOAD = 01;
	const CATEGORY_EDIT = 02;
	const CATEGORY_RENAME = 04;
	const CATEGORY_GEN_REMOVE = 06;
	const CATEGORY_GEN_DELETE = 071;
	const CATEGORY_DOWNLOAD = 010;
	const CATEGORY_GEN_RESTORE = 012;
	const CATEGORY_CREATE_FOLDER = 020;
	const CATEGORY_DELETE_ARCHIVE = 030;
	const CATEGORY_DELETE_BACKUP = 040;
	const CATEGORY_RESTORE_BACKUP = 050;
        
	/**
	*	Constant varialbe
	*	Used by Students, indicator for Submit
	*/
	const CATEGORY_SUBMIT = 0100;
	
	/**
	*	Constant varialbe
	*	Used by Faculties, indicator for Share and  Publish
	*/
	const CATEGORY_SHARE = 01000;
	const CATEGORY_PUBLISH = 02000;
	const CATEGORY_ARCHIVE = 03000;
        
	/**
	*	Constant varialbe
	*	Used by the Administrators, indicator for remove
	*/
	const CATEGORY_ADMIN_REMOVE = 010000;
	const CATEGORY_ADMIN_DELETE = 020000;
	const CATEGORY_ADMIN_RESTORE = 030000;
        const CATEGORY_BACKUP = 040000;
  
	/**
	*	Table name
	*/
	const TABLE = 'fileactivitylogs';
	
	/**
	*	@variables
	*/
	private $activityID;
	private $category;
	private $activityDate;
	private $userID;
	
	/**
	*	Constructor
	*	@Parameter array
	*/
	public function __construct( $fileactivitylog=null ) {
		if($fileactivitylog!=null){
			$this->setActivityID( $fileactivitylog['activityID'] );
			$this->setCategory( $fileactivitylog['category'] );
			$this->setActivityDate( $fileactivitylog['date'] );
			$this->setUserID( $fileactivitylog['userID'] );
		}
	}
	
	/**
	*	@Method setActivityID
	*	@Parameter int
	*/
	public function setActivityID( $activityID ){
		$this->activityID = $activityID;
	}
	
	/**
	*	@Method setCategory
	*	@Parameter int
	*/
	public function setCategory( $category ){
		$this->category = $category;
	}
	
	/**
	*	@Method setActivityDate
	*	@Parameter date
	*/
	public function setActivityDate( $activityDate ){
		$this->activityDate = $activityDate;
	}
	
	/**
	*	@Method setUserID
	*	@Parameter int
	*/
	public function setUserID( $userID ){
		$this->userID = $userID;
	}
	
	/**
	*	@Method getActivityID
	*	@return int
	*/
	public function getActivityID(){
		return $this->activityID;
	}
	
	/**
	*	@Method getCategory
	*	@return int
	*/
	public function getCategory(){
		return $this->category;
	}
	
	/**
	*	@Method getActivityDateTime
	*	@return date - format Year/Month/Day Hour:Minute:Second 
	*/
	public function getActivityDateTime(){
		$formattedDate = date('Y/m/d h:i:s',strtotime($this->activityDate));
		return $formattedDate;
	}
	
	/**
	*	@Method getActivityDate
	*	@return date - format Year/Month/Day
	*/
	public function getActivityDate(){
		$formattedDate = date('Y/m/d',strtotime($this->activityDate));
		return $formattedDate;
	}
	
	/**
	*	@Method getActivityTime
	*	@return date - format Hour:Minute:Second
	*/
	public function getActivityTime(){
		$formattedDate = date('h:i:s',strtotime($this->activityDate));
		return $formattedDate;
	}
	
	/**
	*	@Method getUserID
	*	@return int
	*/
	public function getUserID(){
		return $this->userID;
	}
	
	/**
	*	@Method logFileActivity- logging activities that relates to any file
	*	@Parameter int,int
	*	@Return void
	*/
	public static function logFileAcivity($category,$userID){
		$db = Database::getInstance();
		$statement = 'INSERT INTO '.self::TABLE.' (activityID, category, date, userID) VALUES (DEFAULT,:category,DEFAULT,:id)'; 
		$db->query($statement);
		$db->bind(':id',$userID);
		$db->bind(':category',$category);
		$db->execute();
		return $db->lastInsertId();
	}
	
	//@Override
	public static function getByID( $activityID ) {
		$db = Database::getInstance();
		$db->query('SELECT * FROM '.self::TABLE.' WHERE activityID = :id');
		$db->bind(':id', $activityID);
		$result = $db->single();
		if ($db->rowCount() == 0)
			throw new UserException('No such File Activity Log Record', 1);
		$fileactivitylogs = new FileActivityLogs($result);
		return $fileactivitylogs;
	}
	
	//@Override
	public function updateRecord() {
		$db = Database::getInstance();
		$statement = 'UPDATE '.self::TABLE.' SET category = :category,
                                        date = :date,
					userID = :userId
					WHERE activityID = :activityId';
		$db->query($statement);
		$db->bind(':category',$this->getCategory());
		$db->bind(':date',$this->getActivityDate());
		$db->bind(':userId',$this->getUserID());
		$db->bind(':activityId',$this->getActivityID());
		$db->execute();
		return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
	}
        
        
        public static function getFileReports($maxrow=null,$offset=null,$usertype=null,$category=null,$date=null){
           require_once LIBRARY.'DBQuery.php';
           $db = Database::getInstance();
           
           $cols = array('a.activityID','a.category','a.date',
                        'CONCAT(u.firstName," ",u.lastName) as username','u.userId');
           
           $wherevalue = 'u.userType = ? OR a.category = ? OR a.date > ? OR a.date < ? ';
           
           $query = DBQuery::select()->columns($cols, DBQuery::BACKQUOTE_NONE)->from(self::TABLE.' a');
           $query->innerJoin(User::TABLE.' u','a.userID = u.userID',  DBQuery::BACKQUOTE_NONE);
           
           //where clause
           if($usertype!=null){
               $query->where('u.userType',$usertype);
           }
           if($category!=null && !empty($category) && $category != 99999){
               $query->where('a.category',$category);
           }
           if($date!=null){
                   $query->where('a.date BETWEEN ? AND ?',$date);
           }
		   
           $query->limit($maxrow,$offset)->orderBy($cols[0],  DBQuery::DESC);
//            debug_print_r($query->__toString());
           $db->query($query->__toString());
           $result = $db->resultset();
           
           if(count($result)>0){
               return $result;
           }
           return null;
        }
        
        public static function loglist($usertype=null, $category=null, $date=null){
           require_once LIBRARY.'DBQuery.php';
           $db = Database::getInstance();
           
           $query = DBQuery::select('COUNT(*) as logcount')->from(self::TABLE);
           
           $query = DBQuery::select('COUNT(*) as logcount')->from(self::TABLE.' a');
           $query->innerJoin(User::TABLE.' u','a.userID = u.userID',  DBQuery::BACKQUOTE_NONE);
           
            //where clause
           if($usertype!=null){
               $query->where('u.userType',$usertype);
           }
           if($category!=null && !empty($category)){
               $query->where('a.category',$category);
           }
           if($date!=null){
                   $query->where('a.date BETWEEN ? AND ?',$date);
           }
           
           /*debug_print_r($query->__toString());*/
		   
           $db = Database::getInstance();
           $db->query($query->__toString());
           
           $result = $db->single();
            if (count($result) > 0) {
                return $result['logcount'];
            }
        }
        
        public static function categoryList(){
            return array(
				'Deleted Items by the Admin' => self::CATEGORY_ADMIN_DELETE,
				'Removed Items by the Admin' => self::CATEGORY_ADMIN_REMOVE,
				'Restored Items by the Admin' => self::CATEGORY_ADMIN_RESTORE,
				'Archives' => self::CATEGORY_ARCHIVE,
				'Backups' => self::CATEGORY_BACKUP,
				'Create Folders' => self::CATEGORY_CREATE_FOLDER,
				'Downloaded Items' => self::CATEGORY_DOWNLOAD,
				'Editted Files' => self::CATEGORY_EDIT,
				'Deleted Items' => self::CATEGORY_GEN_DELETE,
				'Removed Items' => self::CATEGORY_GEN_REMOVE,
				'Restored Items' => self::CATEGORY_GEN_RESTORE,
				'Published Items' => self::CATEGORY_PUBLISH,
				'Renamed Items' => self::CATEGORY_RENAME,
				'Shared Items' => self::CATEGORY_SHARE,
				'Submitted Items' => self::CATEGORY_SUBMIT,
				'Uploaded Items' => self::CATEGORY_UPLOAD
            );
        }
        
}
