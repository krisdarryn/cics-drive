<?php
require_once MODEL.'iDatabaseEntity.php';
require_once MODEL.'FileActivityProperties.php';
require_once MODEL.'User.php';
require_once LIBRARY.'DBQuery.php';
class UserActivityLogs implements DatabaseEntity{
    
    /**
     *  Constant variable
     *  Used by all Users indicator in edit account/recover password/change email,password/reset password/
     *                                      answered question/add file filter.../remove file filter../
     *                                      create account student.../enable question.../disable question...
    const CATEGORY_UPDATE = 010;
    const CATEGORY_ACTIVATE = 020;
    const CATEGORY_CHANGE = 030;
    const CATEGORY_RESET = 040;
    const CATEGORY_ANSWERED = 050;
    const CATEGORY_REMOVE = 060;
    const CATEGORY_CREATE_ACCOUNT = 070;
    const CATEGORY_ENABLE = 075;
    const CATEGORY_DISABLE = 090;
    const CATEGORY_RECOVER = 0101;
    const CATEGORY_ADD = 0110;
     */

    
    /* CATEGORY_ACCOUNT_UPDATE
     * kung kani nga category nya walay property, ang iyahang kaugalingon 
     * ra nga account ang iyang gi update.
     * Pero kung naay property, Admin ang nag update ug account k?
     */
    const CATEGORY_ACCOUNT_UPDATE = 010;
    
    const CATEGORY_ACCOUNT_ACTIVATE = 011;
    const CATEGORY_ACCOUNT_PASSWORD_RECOVER = 012;
    const CATEGORY_MISSED_QUESTION = 013;
    const CATEGORY_ACCOUNT_PASSWORD_RESET = 014;
    
    
    /* CATEGORY_ACCOUNT_CREATE
     * gamiton ni kung ang admin mag create ug accounts
     * ang property ani kay ang userID sa created Account
     */
    const CATEGORY_ACCOUNT_CREATE = 015;
    
    
    const CATEGORY_ACCOUNT_UPDATE_STATUS = 016;
    const CATEGORY_ACCOUNT_IMPORT = 017;
    
    
    const CATEGORY_SECURITY_QUESTION_ADD = 020;
    const CATEGORY_SECURITY_QUESTION_DISABLED = 0210;
    const CATEGORY_SECURITY_QUESTION_ENABLED = 0211;
    const CATEGORY_SECURITY_QUESTION_ANSWER_CHANGE = 022;
    
   
    const CATEGORY_FILE_FILTER_ADD = 040;
    const CATEGORY_FILE_FILTER_REMOVE = 042;

    const CATEGORY_FILE_FILTER_STUDENT_ENABLE =   043;
    const CATEGORY_FILE_FILTER_STUDENT_DISABLE =  044;
    const CATEGORY_FILE_FILTER_FACULTY_ENABLE =   045;
    const CATEGORY_FILE_FILTER_FACULTY_DISABLE =  046;
    const CATEGORY_FILE_FILTER_UPDATE = 047;
    
    
    
    /**
     * Constant table 
     */
    const TABLE = 'useractivitylogs';
    
    /**
     * @var 
     */
    private $activityID;
    private $category;
    private $date;
    private $userID;
    
    /**
     *  constructor
     *  @param array 
     */
  public function __construct( $arrgs=null ) {
		if($arrgs!=null){
			$this->setActivityID( $arrgs['activityID'] );
			$this->setCategory( $arrgs['category'] );
			$this->setActivityDate( $arrgs['date'] );
			$this->setUserID( $arrgs['userID'] );
		}
	}
	
	/**
	*	@Method setActivityID
	*	@Parameter int
	*/
	public function setActivityID( $activityID ){
		$this->activityID = $activityID;
	}
	
	/**
	*	@Method setCategory
	*	@Parameter int
	*/
	public function setCategory( $category ){
		$this->category = $category;
	}
	
	/**
	*	@Method setActivityDate
	*	@Parameter date
	*/
	public function setActivityDate( $activityDate ){
		$this->date= $activityDate;
	}
	
	/**
	*	@Method setUserID
	*	@Parameter int
	*/
	public function setUserID( $userID ){
		$this->userID = $userID;
	}
	
	/**
	*	@Method getActivityID
	*	@return int
	*/
	public function getActivityID(){
		return $this->activityID;
	}
	
	/**
	*	@Method getCategory
	*	@return int
	*/
	public function getCategory(){
		return $this->category;
	}
	
	/**
	*	@Method getActivityDate
	*	@return date - format Year/Month/Day
	*/
	public function getFormattedActivityDate(){
                $formattedDate = date('Y/m/d h:i:s',strtotime($this->activityDate));
		return $formattedDate;
	}
        
        public function getActivityDate(){
            return $this->date;
        }
	
	/**
	*	@Method getUserID
	*	@return int
	*/
	public function getUserID(){
		return $this->userID;
	}
    
        /**
	*	@Method logActivity- logging activities that relates to user
	*	@Parameter int,int
	*	@Return void
	*/
	public static function logAcivity($category,$userID){
		$db = Database::getInstance();
		$statement = 'INSERT INTO '.self::TABLE.' (activityID, category, date, userID) VALUES (DEFAULT,:category,DEFAULT,:id)'; 
		$db->query($statement);
		$db->bind(':id',$userID);
		$db->bind(':category',$category);
		$db->execute();
		return $db->lastInsertId();
	}
        
        //@Override
	public static function getByID( $activityID ) {
		$db = Database::getInstance();
		$db->query('SELECT * FROM '.self::TABLE.' WHERE activityID = :id');
		$db->bind(':id', $activityID);
		$result = $db->single();
		if ($db->rowCount() == 0)
			throw new UserException('No such User Activity Log Record', 1);
		$useractivitylogs = new UserActivityLogs($result);
		return $useractivitylogs;
	}
	
	//@Override
	public function updateRecord() {
		$db = Database::getInstance();
		$statement = 'UPDATE '.self::TABLE.' SET category = :category,
                                        date = :date,
					userID = :userId
					WHERE activityID = :activityId';
		$db->query($statement);
		$db->bind(':category',$this->getCategory());
		$db->bind(':date',$this->getActivityDate());
		$db->bind(':userId',$this->getUserID());
		$db->bind(':activityId',$this->getActivityID());
		$db->execute();
		return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
	}
        
         public static function getUserReports($maxrow=null,$offset=null,$usertype=null,$category=null,$date=null){
           require_once LIBRARY.'DBQuery.php';
           $db = Database::getInstance();
           
           $cols = array('a.activityID','a.category','a.date',
                        'CONCAT(u.firstName," ",u.middleInitial," ",u.lastName) as username','u.userId');
           
           $wherevalue = 'u.userType = ? OR a.category = ? OR a.date > ? OR a.date < ? ';
           
           $query = DBQuery::select()->columns($cols, DBQuery::BACKQUOTE_NONE)->from(self::TABLE.' a');
           
           $query->innerJoin(User::TABLE.' u','a.userID = u.userID',  DBQuery::BACKQUOTE_NONE);
           
           //where clause
           if($usertype!=null){
               $query->where('u.userType',$usertype);
           }
           if($category!=null && !empty($category)){
               $query->where('a.category',$category);
           }
           if($date!=null){
               $query->where('a.date BETWEEN ? AND ?',$date);
           }
		   
           $query->limit($maxrow,$offset)->orderBy($cols[0],  DBQuery::DESC);
           
           $db->query($query->__toString());
           $result = $db->resultset();
           
           if(count($result)>0){
               return $result;
           }
           return null;
        }
        
        public static function loglist($usertype=null, $category=null, $date=null){
           require_once LIBRARY.'DBQuery.php';
           $db = Database::getInstance();
           
           $query = DBQuery::select('COUNT(*) as logcount')->from(self::TABLE);
           
           $query = DBQuery::select('COUNT(*) as logcount')->from(self::TABLE.' a');
           $query->innerJoin(User::TABLE.' u','a.userID = u.userID',  DBQuery::BACKQUOTE_NONE);
           
            //where clause
           if($usertype!=null){
               $query->where('u.userType',$usertype);
           }
           if($category!=null && !empty($category)){
               $query->where('a.category',$category);
           }
           if($date!=null){
                   $query->where('a.date BETWEEN ? AND ?',$date);
           }
		   
           $db = Database::getInstance();
           $db->query($query->__toString());
           
           $result = $db->single();
           
           if (count($result) > 0) {
               return $result['logcount'];
           }
        }
        
        public static function countReports($results) {
          $count = 0;
          foreach ($results as $result) {
            if ($result['category'] != null) {
              $count++;
            }
          }
          return $count;
        }
        
        public static function generateReport($offset, $limit = 10, $userType = null, $category = null, $dateInterval = null) {
          
          $q1 = DBQuery::SELECT('ual.*, "" as id, "" as value, CONCAT_WS(" ", u.firstName, COALESCE(u.lastName, u.userID)) as user');
          $q1->from('useractivitylogs ual');
          $q1->innerJoin('users u', 'ual.userID = u.userID');
          if ($userType != null) {
            $q1->where('u.userType', $userType);
          } 
          if ($category != null) {
            $q1->where('ual.category', $category);
          }
          
          if ($dateInterval != null) {
            $q1->where('ual.date BETWEEN ? AND ?', array($dateInterval[0], $dateInterval[1]));
          }
          $q1->orderBy('3', DBQuery::DESC);
          $q1->limit($limit, $offset);
          
          
          
//          $sql = "
//(SELECT ual.*, '' AS 'id', '' as 'value', CONCAT_WS(' ', u.firstName, COALESCE(u.lastName, u.userID)) as user
//FROM useractivitylogs ual
//JOIN users u
//ON ual.userID = u.userID LIMIT :offset, :limit)
//UNION
//(SELECT uap.activityID, null, '','', uap.id, uap.value, ''
//FROM useractivityproperties uap
//ORDER BY 1 DESC)
//ORDER BY 1 DESC, 2 DESC, 5 ASC
//";
          //$q1->where('category', UserActivityLogs::CATEGORY_ACCOUNT_UPDATE);
          
          
          $db = Database::getInstance();
          $db->query($q1->__toString());
          $db->bind(':offset', $offset);
          $db->bind(':limit', $limit);
          $result = $db->resultset();
          if ($result > 0) {
            return $result;
          }
          
          return array();
        }
        
        public static function categoryList(){
/*
    const CATEGORY_ACCOUNT_UPDATE = 010;
    const CATEGORY_ACCOUNT_ACTIVATE = 011;
    const CATEGORY_ACCOUNT_PASSWORD_RECOVER = 012;
    const CATEGORY_MISSED_QUESTION = 013;
    const CATEGORY_ACCOUNT_PASSWORD_RESET = 014;
    const CATEGORY_ACCOUNT_CREATE = 015;
    const CATEGORY_ACCOUNT_UPDATE_STATUS = 016;
    const CATEGORY_ACCOUNT_IMPORT = 017;
    const CATEGORY_SECURITY_QUESTION_ADD = 020;
    const CATEGORY_SECURITY_QUESTION_DISABLED = 0210;
    const CATEGORY_SECURITY_QUESTION_ENABLED = 0211;
    const CATEGORY_SECURITY_QUESTION_ANSWER_CHANGE = 022;
    const CATEGORY_FILE_FILTER_ADD = 040;
    const CATEGORY_FILE_FILTER_REMOVE = 042;
    const CATEGORY_FILE_FILTER_STUDENT_ENABLE =   043;
    const CATEGORY_FILE_FILTER_STUDENT_DISABLE =  044;
    const CATEGORY_FILE_FILTER_FACULTY_ENABLE =   045;
    const CATEGORY_FILE_FILTER_FACULTY_DISABLE =  046;
    const CATEGORY_FILE_FILTER_UPDATE = 047;
 */
            return array(
                'Account Update' => self::CATEGORY_ACCOUNT_UPDATE,
                'Activate Account' => self::CATEGORY_ACCOUNT_ACTIVATE,
                'Password Recover' => self::CATEGORY_ACCOUNT_PASSWORD_RECOVER,
                'Unanswered Questions' => self::CATEGORY_MISSED_QUESTION,
                'Reset Password' => self::CATEGORY_ACCOUNT_PASSWORD_RESET,
                'Create Account' => self::CATEGORY_ACCOUNT_CREATE,
                'Changed User Status' => self::CATEGORY_ACCOUNT_UPDATE_STATUS,
                'Import Student Accounts' => self::CATEGORY_ACCOUNT_IMPORT,
                'Disabled Security Question' => self::CATEGORY_SECURITY_QUESTION_DISABLED,
                'Enabled Security Question' => self::CATEGORY_SECURITY_QUESTION_ENABLED,
                'Changed Answer' => self::CATEGORY_SECURITY_QUESTION_ANSWER_CHANGE,
                'Add File Filter' => self::CATEGORY_FILE_FILTER_ADD,
                'Remove File Filter' => self::CATEGORY_FILE_FILTER_REMOVE,
                'Enabled Student File Filter' => self::CATEGORY_FILE_FILTER_STUDENT_ENABLE,
                'Disabled Student File Filter' => self::CATEGORY_FILE_FILTER_STUDENT_DISABLE,
                'Enabled Faculty File Filter' => self::CATEGORY_FILE_FILTER_FACULTY_ENABLE,
                'Disabled Faculty File Filter' => self::CATEGORY_FILE_FILTER_FACULTY_DISABLE
            );
        }
}

?>
