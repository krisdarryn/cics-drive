<?php
/**
 * @package process
 * @subpackage file
 */
class FileFilters{

	//table
	const TABLE = 'filefilters';
	
	//fields
	private $fileFiltersID;
	private $extension;
	private $forFaculty;
	private $forStudent;
	
	//setteers
	public function setFileFiltersID($fileFiltersID){
		$this->fileFiltersID = $fileFiltersID;
	}
	
	public function setExtension($extension){
		$this->extension = $extension;
	}
	
	public function setForFaculty($forFaculty){
		$this->forFaculty = $forFaculty;
	}
	
	public function setForStudent($forStudent){
		$this->forStudent = $forStudent;
	}
	
	//getters
	public function getFileFiltersID(){
		return $this->fileFiltersID;
	}
	
	public function getExtension(){
		return $this->extension;
	}
	
	public function getForFaculty(){
		return $this->forFaculty;
	}
	
	public function getForStudent(){
		return $this->forStudent;
	}
	
	//isFilter
	public static function isFiltered($extension,$userID){
		
	}
	/**
	*	show all the list of file extensions
	*
	*/
	public static function fileList() {
		$db = Database::getInstance();
		$sql="SELECT extension, forFaculty, forStudent FROM filefilters ORDER BY extension DESC";
		$db->query($sql);
		$result = $db->resultSet();
		if($result != null){
			return $result;
		}	
		return array();
	}
	/**
	*	existingExtension
	*	Parameter : string
	*	Checks the file extension if it is existing in the database.
	*	Returns the result set if it is not existing,otherwise false.
	*/
	public static function existingExtension($extension){
		$db = Database::getInstance();
		$db->query("SELECT 	extension FROM filefilters WHERE extension = :extension" );
		$db->bind(':extension', $extension);
		$result = $db->single();
		if (count($result) > 0) {
			return $result;
		}
		return false;
	}
	/**
	*	add file extension
	*	Parameter : string
	*	This is used to add file extension.
	*	If query is successful the values will be set into the current Session.
	*/
	public function addFileExtension($extension,$forFaculty,$forStudent) {
			$db = Database::getInstance();
			$sql = "INSERT INTO filefilters(fileFiltersID, extension, forFaculty, forStudent) VALUES (DEFAULT,:extension,:forFaculty,:forStudent)";
			$db->query($sql);
			$db->bind(':extension', $extension);
			$db->bind(':forFaculty', $forFaculty);
			$db->bind(':forStudent', $forStudent);
			$db->execute();
			return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
	}
	
	/**
	* update file filter table	
	*
	*/
	public static function updateFileFilter($forFaculty,$forStudent,$extension){
		$db = Database::getInstance();
		$sql = "UPDATE filefilters SET forFaculty = :faculty, forStudent = :student WHERE extension = :extension" ;
		$db->query($sql);
		$db->bind(':faculty', $forFaculty);
		$db->bind(':student', $forStudent);
		$db->bind(':extension', $extension);
		$db->execute();
		return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
	}
	
	/**
	* delete file filter table	
	*/
	public static function deleteFromFileFilter($extension){
		$db = Database::getInstance();
		$sql = "DELETE FROM ".self::TABLE." WHERE extension = :extension" ;
		$db->query($sql);
		$db->bind(':extension', $extension);
		$db->execute();
		return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
	}
	
	/**
	*	delete file extension
	*	Parameter : string
	*	This is used to delete file exteension.
	*	If query is successful the values will be deleted to the table.
	*/
	public function deleteFileExtension($extension) {
		$db = Database::getInstance();
		$sql = "DELETE FROM ".self::TABLE." WHERE extension = :extension";
		$db->query($sql);
		$db->bind(':extension', $extension);
		$db->execute();
		return ((int)$db->getErrorCode() == Database::SUCCESS_CODE);
	}
	
	/**
	*	check file extension to be uploaded 
	*	Parameter : string, int
	*	This is used to check file extension to be uploaded is valid
	*	If query is successful it will return a row, otherwise empty i.e. null
	*/
	public static function isExtensionNotValid($extension, $userType){
		$res = false;
		$db = Database::getInstance();
		$statement = 'SELECT * FROM '.self::TABLE.' WHERE extension = :extn';
		$db->query($statement);
		$db->bind(':extn',$extension);
		$result = $db->single();
		
		if(!empty($result)){
			if( $userType == 2 ){
				$res = ($result['forFaculty']==1);
			}else if( $userType == 3 ){
				$res = ($result['forStudent']==1);
			}
		}
		return $res;
	} 	

}
