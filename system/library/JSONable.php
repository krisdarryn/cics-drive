<?php

interface JSONable {
	public function toJSON();
}