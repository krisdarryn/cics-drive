<?php
require_once LIBRARY.'drive/DriveItem.php';
require_once LIBRARY.'drive/File.php';
require_once LIBRARY.'drive/SharedFolder.php';
require_once LIBRARY.'ZipWrapper.php';
require_once LIBRARY.'Session.php';
require_once LIBRARY.'drive/SubmittedFolder.php';
require_once MODEL.'FileActivityLogs.php';
require_once MODEL.'FileActivityProperties.php';
require_once MODEL.'SharedFolder.php';

class Folder extends DriveItem {
    
        //User 
        private $User;
        
        
        /**
        *	Constructor
        *	@parameter array , object(optional)
        */
        public function __construct($folder, $user=null) {
                if ($folder instanceof Folder)
                        $this->setPath($folder);
                else 
                        $this->setPath($folder);
                
                 /*if(!file_exists($folder)){
                     mkdir($folder); 
                 }*/
                
                $this->User = $user;
        }
		
        /**
        *	@method getContents 
        *	@return all the contents (i.e. folder and files)
        */
        public function getContents($recoveryMode = false) {
                if (!file_exists($this->getPath())) {
                  return null;
                }
                $contents =  array_diff(scandir($this->getPath()), array('.', '..'));
                $folders = array();	
                $files = array();
                $temp_user='';
                if($this->User == null){
                    $us = Session::getUser();
                    $us = $us['userId'];
                    $temp_user = User::getByID($us);
                }
                foreach ($contents as $index => $item) {
                        $itemPath = $this->getPath().'/'.$item;
                        if (is_dir($itemPath)) {
                                $folder = new Folder($itemPath, ($this->User==null)?$temp_user:$this->User );
                                $shareFlagFile = new File($folder->getPath().'/.shared',($this->User==null)?$temp_user:$this->User);
                                
                                if (!$recoveryMode && file_exists($shareFlagFile->getPath())) {
                                  $model = \model\SharedFolder::getById((int)$shareFlagFile->getContents());
                                  if ($model != null) { // if a record of the shared folder exists
                                    $folder = new SharedFolder($model,$this->User);
                                  }
                                }
                                if (!$recoveryMode && file_exists($folder->getPath().'/.submitted')) {
                                  $folder = new SubmittedFolder($folder->getPath());
                                  
                                }
                                $folders[] = $folder;
                        } else {
                                $file = new File($itemPath,$this->User);
                                  $files[] = new File($itemPath,($this->User==null)?$temp_user:$this->User);
                        }
                }
                return array_merge($folders, $files);
        }

        /**
        *	@method getFolders
        *	@return all folder
        */
        public function getFolders() {
                $contents =  array_diff(scandir($this->getPath()), array('.', '..'));
                natcasesort($contents);
                
                $folders = array();
                foreach ($contents as $index => $item) {
                        $itemPath = $this->getPath().'/'.$item;
                        if (is_dir($itemPath)) {
                                $folder = new Folder($itemPath);
                                $shareFlagFile = new File($folder->getPath().'/.shared');
                                
                                if (file_exists($shareFlagFile->getPath())) {
                                  $model = \model\SharedFolder::getById((int)$shareFlagFile->getContents());
                                  if ($model != null) { // if a record of the shared folder exists
                                    $folder = new SharedFolder($model);
                                  }
                                }
                                if (file_exists($folder->getPath().'/.submitted')) {
                                  
                                  $folder = new SubmittedFolder($folder->getPath());
                                }
                                $folders[] = $folder;
                        }
                }
                return $folders;
        }
        
        public function getSharedFolders() {
          $currentFolder = $this;
          $folders = array();
          $shares = array();
          while ($currentFolder != null) {
            if ($currentFolder instanceof SharedFolder) {
              $shares[] = $currentFolder;
            }
          }
          return array();
        }
         

        /**
         * @method getFiles 
         * @return File - return all files inside the directory
         */
        public function getFiles() {
                $contents =  array_diff(scandir($this->getPath()), array('.', '..'));
                natcasesort($contents);
                $files = array();
                foreach ($contents as $index => $item) {
                        $itemPath = $this->getPath().'/'.$item;
                        if (!is_dir($itemPath)) {
                          $file = new File($itemPath);
                           if ($file->getName() != '.shared' || $file->getName() != '.submitted')
                             $files[] = new File($itemPath);
                        }
                }
                return $files;
        }
		
        /**
        *	@Method isFile
        *	@return true if file,otherwise false
        */
        public function isFile(){	
                return is_file($this->getPath());
        }
		
        /**
        *	@method recycleRoot
        *	@return folder object points to the recycle bin
        */
        public static function recycleRoot($id){
                return new Folder(rtrim(DRIVE.$id.DriveItem::RECYCLE,'/'));
        }
		
        /**
        *	@method createFolder - used to create folder
        *	@parameter string, boolean(optional)
        *	@return true on success, otherwise false
        */
        public function createFolder($name,$forRestore=false){
                
                foreach($this->getFolders() as $fname){
                        if( strcasecmp($name,$fname->getName()) == 0 )
                                return false;
                }
                $result = mkdir($this->getPath().'/'.$name);
                if(!(boolean)$forRestore && $result ){
                        $actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_CREATE_FOLDER,$this->User->getUserID() );
                        FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_PATH, $this->getRelPath().'/'.$name );
                }
                return $result;
        }
		
        /**
        *	@method remove - remove the folder and move to the Item Recovery 
        *	@parameter string
        *	@return true on success, otherwise false
        */
        public function remove($root){
                $name = $this->getName();
                $recycleItem='';
                $temp_path='';
                $result=false;
                $viewuser='';

                if (Session::get('view_user_id') != null /*&& Session::get('admin_id')==null*/){
                        $us = Session::get('user');
                        $us = $us['userId'];
                        
                        $temp_path = new Folder(DRIVE.';ADMINRECYCLE;/'.Session::get('view_user_id').'/'); 
                        $recycleItem = array_diff(scandir($temp_path->getPath()),array('.','..'));

                        $name = Util::checkDuplicate($recycleItem,$name);
                        $result = rename($this->getPath(),$temp_path->getPath().$name); 

                }else{
                        $recycleItem = array_diff(scandir(Folder::recycleRoot($this->User->getUserID())->getPath()),array('.','..'));
                        $name = Util::checkDuplicate($recycleItem,$name);

                        $result = rename($this->getPath(),$root.DriveItem::RECYCLE.$name); 
                } 

                if($result){
                        if (Session::get('view_user_id') != null /*&& Session::get('admin_id')==null*/){
                            $us = Session::getUser();
                             
                            $actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_ADMIN_REMOVE, Session::get('view_user_id') );
                            FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_OLD_PATH, $this->getRelPath() );
                            FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_NEW_PATH, $name );
                            FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_ADMIN, $us['userId']);
                        }else{
                            $actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_GEN_REMOVE,$this->User->getUserID() );
                            FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_OLD_PATH, $this->getRelPath() );
                            FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_NEW_PATH, $name );
                        }
                }

                return $result;
        }
        
        public function copy($destination) {
          
        }   
		
        /**
        *   @method download - download the folder in to .zip file
        */
        public function download($backup=null) {
		$us = Session::get('user');            
                $us = $us['userId'];		
                $temp_user = User::getByID($us);
                
                // pack the files in an archive
                $archive = new ZipArchive();
                
				if (!file_exists(TEMP))
                        mkdir(TEMP);
						
                $fileName = $this->getName().'.zip';
				
                $archive->open(TEMP.$fileName, ZIPARCHIVE::CREATE) ;
				
                $zipWrapper = new ZipWrapper($temp_user);
				
                $zipWrapper->setZipArchive( $archive );
				 
				if((boolean)$backup){
					$zipWrapper->addDir($this->getPath(),$backup);
                }else{
					$zipWrapper->compressForDownload($this->getPath());
				}
				
				$archive = $zipWrapper->getZipArchive();
                $archive->close();
				
                ob_end_flush();

                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'. $fileName.'"');
                header('Content-Transfer-Encoding: binary');
                header('Connection: Keep-Alive');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Length: ' . filesize(TEMP.$fileName));
				readfile(TEMP.$fileName);
                
                /* unlink(TEMP.$fileName); */
                
                $actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_DOWNLOAD,$temp_user->getUserID() );
                FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_PATH, $this->getRelPath() );
                exit;
        }
		
        /**
        *	@method rename - changing the current name 
        *	@parameter - string, string
        *	@return true on success, othewise false
        */
        public function rename( $oldName, $newName ){
                if ( in_array( $newName,scandir( dirname($this->getPath()) ) ) ){
                        return false;
                }
                $result =  rename( $this->getPath(), dirname($this->getPath()).'/'.$newName );
               
                if($result){
                        $actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_RENAME,$this->User->getUserID() );
                        FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_OLD_PATH, $this->getRelPath() );
                        FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_NEW_PATH, substr($this->getRelPath(),0,strripos($this->getRelPath(),'/')+1).$newName );
                }
                return $result;
        }
		
        /**
        *	@method delete - permanently delete the folder from the drive
        *	@return true on sucess, otherwise false;
        */
        public function delete($archive=null,$backup=null){	
                 
                foreach($this->getContents() as $key =>$item){
                                if($item instanceof File){
                                    $item->delete(null,(boolean)$archive,null,null,(boolean)$backup);
                                }
                                if($item instanceof Folder){
                                    $item->delete((boolean)$archive,(boolean)$backup);
                                }
                }
                if(count($this->getContents()) == 0){
                        $us = Session::getUser();            
                        $us = $us['userId'];
                            
                        if( Session::get('view_user_id')!=null){	
                            $viewuser = User::getByID(Session::get('view_user_id'));
                            
                            $actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_ADMIN_DELETE,$viewuser->getUserID() );
                            FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_PATH, $this->getRecycleRelPath());
                            FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_ADMIN, $us);
                        }else if((boolean)$archive){
                            /*$actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_DELETE_ARCHIVE,$this->User->getUserID() );
                            FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_PATH, $this->getArchiveRelPath() );*/
                        }else if((boolean)$backup){
                            /*$actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_DELETE_BACKUP,$us );
                            FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_PATH, $this->getBackupRelPath() );*/
                        }else{
                            $actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_GEN_DELETE,$us );
                            FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_PATH, $this->getRecycleRelPath() );
                        }
                        return rmdir($this->getPath());
                }			
        }
		
		/**
		*	@method restore - restore the folder and its content to the drive from the recycle bin
		*	@return true on success, otherwise false
		*/
		public function restore(){
			if( Session::get('view_user_id') != null ){
                            $actID = FileActivityProperties::queryActivityID( $this->getName(), FileActivityProperties::INDICATE_NEW_PATH, Session::get('view_user_id') );
                            $entity = FileActivityProperties::getByID($actID);
                        }else{
                            $actID = FileActivityProperties::queryActivityID( $this->getName(), FileActivityProperties::INDICATE_NEW_PATH, $this->User->getUserID() );
                            $entity = FileActivityProperties::getByID($actID);
                        }
                        
			$restoreDone=false;
			$temp;
			$folderPath;
			$toRestore = substr( $entity->getValue(),strripos($entity->getValue(),'/')+1,strlen($entity->getValue()) );
                        
			if( substr_count($entity->getValue(), '/') == 1 ){
				if (Session::get('view_user_id') != null){				
					$viewuser = User::getByID(Session::get('view_user_id'));
					$temp_path = array_diff( scandir(DRIVE.$viewuser->getUserID().'/'),array('.','..',';recycle;') );
					$toRestore = Util::checkDuplicate($temp_path, $toRestore);
					
					$restoreDone = rename($this->getPath(),DRIVE.$viewuser->getUserID().'/'.$toRestore);
					$folderPath = '/';	
				}else{
					$temp_path = array_diff( scandir(DRIVE.$this->User->getUserID().'/'),array('.','..',';recycle;') );
                                        
                                        $toRestore = Util::checkDuplicate($temp_path,$toRestore);
					
					$restoreDone = rename($this->getPath(),DRIVE.$this->User->getUserID().'/'.$toRestore);
					$folderPath = '/';	
				}
			}else{
				$folderPath = substr( $entity->getValue(),0, strripos($entity->getValue(),'/') ); 
				$paths = explode('/',$folderPath);
				
				if (Session::get('view_user_id') != null){
					$viewuser = User::getByID(Session::get('view_user_id'));
					
					$folder = new Folder(DRIVE.$viewuser->getUserID(),$viewuser);
				}else{			
					$folder = new Folder(DRIVE.$this->User->getUserID(),$this->User);
				}
				
				foreach($paths as $key=>$path){
					if(!empty($path)){
						if($key>1){
							
							$folder->createFolder($path,true);
							if (Session::get('view_user_id') != null){
								$viewuser = User::getByID(Session::get('view_user_id'));
								$folder = new Folder($folder->getPath().'/'.$path,$viewuser);
								
								$temp_path = array_diff(scandir($folder->getPath()),array('.','..'));
								$toRestore = Util::checkDuplicate($temp_path,$toRestore);
							}else{
								$folder = new Folder($folder->getPath().'/'.$path,$this->User);
								
								$temp_path = array_diff(scandir($folder->getPath()),array('.','..'));
								$toRestore = Util::checkDuplicate($temp_path,$toRestore);
							}
						}else{
							$folder->createFolder($path,true);
							if (Session::get('view_user_id') != null){
								$viewuser = User::getByID(Session::get('view_user_id'));
								$folder = new Folder($folder->getPath().'/'.$path,$viewuser);
								
								$temp_path = array_diff(scandir($folder->getPath()),array('.','..'));
								$toRestore = Util::checkDuplicate($temp_path,$toRestore);
							}else{
								$folder = new Folder($folder->getPath().'/'.$path,$this->User);
								
								$temp_path = array_diff(scandir($folder->getPath()),array('.','..'));
								$toRestore = Util::checkDuplicate($temp_path,$toRestore);
							}
						}
					}
				}
			$restoreDone = rename($this->getPath(),$folder->getPath().'/'.$toRestore); 
		}
		
		if($restoreDone){
                    if(Session::get('view_user_id')!=null){
                        $us = Session::getUser();
                        
                        $actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_ADMIN_RESTORE,Session::get('view_user_id') );
			FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_PATH, '/'.$this->getName() );
                        FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_ADMIN, $us['userId']);
                    }else{
			$actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_GEN_RESTORE,$this->User->getUserID() );
			FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_PATH, $entity->getValue() );
                    }
                }
		return array($restoreDone,$folderPath);
	}
	
	/**
	*	@method getSize - get the size along with the contents
	*	@return int
	*/
	public function getSize() {
            $size=0;

            foreach($this->getContents() as $key=>$item){
                    if($item instanceof File)
                            $size += $item->getSize();
                    if($item instanceof Folder)
                            $size += $item->getSize();
            }

            if(count($this->getContents()) == 0)
                    $size += 0;

            return $size;
	}
	
        /**
         * @method countInnerContents - This will count all contents in a directory until to the sub directory as well the directory it self
         * @param type $path
         * @return type 
         */
        public static function countInnerContents($path){
            $ite=new RecursiveDirectoryIterator($path);
            $ites = new RecursiveIteratorIterator($ite);
            
            $nbfiles=0;
            $dir=array();
            
            foreach ($ites as $filename=>$cur) {    
                if(!in_array($cur->getPath(),$dir)){
                    $dir[] = $cur->getPath();
                }
                $nbfiles++;
            }
            
            return $nbfiles + count($dir);
        }
        
/*	public function __toString() {
		return '<a href="'SITE_URL.''.$this->getRelPath().'/"><li class="folder">'.$this->getName().'</li></a>'."\n";
	}*/
}
