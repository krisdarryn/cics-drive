<?php
require_once LIBRARY.'drive/Folder.php';
require_once LIBRARY.'drive/PublishedFile.php';
require_once MODEL.'Publication.php';
class PublicationsFolder extends Folder {
  const FOLDER_NAME = '.publications';
  private $files;
  public function __construct(User $owner,$delete=null) {
    $items = Publication::getByUserID($owner->getUserID());
    
    $items = PublishedFile::factory($items);
    
    if($delete!=null){
      foreach($items as $item){
        $model = $item->getModel();
         if( strrpos( $model->getFilePath(),$delete ) > -1){
           $model->setStatus('I');
           $model->updateRecord();
         }
      }
    }
    
    
    if (is_array($items)) {
		
	  foreach ($items as $publishedFile) {
		$model = $publishedFile->getModel();
		if (file_exists($publishedFile->getPath()) && $model->getStatus() == 'A') {
			
			$this->files[] = $publishedFile;
		} else {
			$publishedFile->getModel()->setStatus('I');
			$publishedFile->getModel()->updateRecord();
		}
		
	  }
      
    } else {
      $this->files[] = $items;
    }
    
    
    $this->setPath(DRIVE.$owner->getUserID().'/.publications');
  }
  public function getContents($recoveryMode = false) {
    return $this->files;
  }
  
  public function getContent($name) {
    foreach ($this->files as $file) {
      if ($file->getName() == $name) {
        return $file;
      }
    }
    return null;
  }
  
  public function getName() {
    return 'Publications';
  }  
  
  public function getPublicationFile(){
      return  $this->files;
  }
}

