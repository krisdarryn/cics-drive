<?php
require_once LIBRARY.'drive/Folder.php';
require_once MODEL.'SharedFolder.php';
require_once MODEL.'Submission.php';
require_once MODEL.'SharedFileFilter.php';
class SharedFolder extends Folder {


  private $model;
  
  public function __construct(\model\SharedFolder $model) {
    $this->setModel($model);
    $driveRoot = DRIVE.$this->getModel()->getFacultyUserID();
    $this->setPath($driveRoot.$this->getModel()->getFolderPath());
  }
  
  public function getModel() {
    return $this->model;
  }

  public function setModel(\model\SharedFolder $model) {
    $this->model = $model;
  }

  public function __call($name, $arguments) {
    return $this->model->$name($arguments);
  }
  
  // TODO
  public function delete() {
    return parent::delete();
  }
  
  public function hasUserSubmitted(User $user) {
    return count($this->getUserSubmissions($user)) > 0;
  }
  
  public function getAllSubmissions() {
    return $this->getModel()->getSubmissions();
  }
  
  public function getUserSubmissions(User $user, $includeInactive = false) {
    $submissions = array();
    $sql = "SELECT * FROM ".Submission::TABLE." WHERE sharedFolderID = :sharedID AND studentUserID = :userID";
    if (!$includeInactive) {
      $sql .= " AND status = 'A'";
    }
    $db = Database::getInstance();
    $db->query($sql);
    $db->bind(':sharedID', $this->getModel()->getSharedFolderID());
    $db->bind(':userID', $user->getUserID());
    $resultSet = $db->resultset();
    if (count($resultSet) > 0) {
      foreach ($resultSet as $result) {
        $submissions[] = new Submission($result);
      }
    }
    return $submissions;
  }
  
  public function OrganizeSubmissions(){
    // 1. traverse each folder and check if the folder has submitted folder flag
    // 2. foreach submitted folder
    //      check the submissionID if the record exists in the db
    //      rename the current submitted folder with the name prefix+originalFolderName+suffix
    
    
    $prefix = $this->getSubmissionPrefix();
    $suffix = $this->getSubmissionSuffix();
    
    $submittedFolders = $this->getFolders();
    
    // 1. traverse each folder
    foreach ($submittedFolders as $folder) {
	  $suffix_of_prefix = '';
	  $suffix_of_suffix = '';

	  $prefix = $this->getSubmissionPrefix();
	  $suffix = $this->getSubmissionSuffix();
      $flagPath = $folder->getPath().'/'.'.submitted';
      if (file_exists($folder->getPath().'/'.'.submitted')) { // check if the folder has submitted folder flag
        $submissionID = file_get_contents($flagPath);
        $subModel =Submission::getByID($submissionID);
        $student = User::getByID($subModel->getStudentUserID());
        if ($subModel == null || $student == null) {
          continue;
        }
        
        if ($prefix == "{lastname}") {
          $prefix = $student->getLastName().'-';
		  $suffix_of_prefix = '-'.$student->getFirstName();
        }
        if ($suffix == "{lastname}") {
          $suffix = '-'.$student->getLastName();
		  $suffix_of_suffix = '-'.$student->getFirstName();
        }

        $name = $this->getPath().'/'.$prefix.$subModel->getOriginalFolderName().$suffix;
		if (file_exists($name)) {
			$name = $this->getPath().'/'.$prefix.$suffix_of_prefix.$subModel->getOriginalFolderName().$suffix.$suffix_of_suffix;
		}
        rename($folder->getPath(), $name);
      }
    }
  }
  
  public function acceptSubmission(User $student, $relPath, $force = false) {
    $returnData = array();
    if ($student == null) {
      $returnData['error'] = 4; // unknown student
      return $returnData;
    }
    

    
    $submissionPath = DRIVE.$student->getUserID().$relPath;
    
    /*if (!file_exists($submissionPath)) {
      return 5; // non-existent folder
    }*/
    
    $submittedFolder = new Folder($submissionPath, $student);
    
    if (!$force) {
      if ($this->hasUserSubmitted($student)) {
         $returnData['error'] =  7; // already submitted
         return $returnData;
      }
      if (file_exists($submittedFolder->getPath().'/.submitted')) {
        $id = (int)file_get_contents($submittedFolder->getPath().'/.submitted');
        $sub = Submission::getByID($id);
        if ($sub != null) {
          $returnData['error'] =  7; // already submitted
          return $returnData;
        }
      }
    }
    
    
    $filters = SharedFileFilter::getByFolderID($this->getSharedFolderID());
    $filterArr = array();
    if ($filters != null) {
      foreach ($filters as $filter) {
        $filterArr[] = $filter->getExtension();
      }
    }
    $returnData['filter'] = $filterArr;
    $returnData['filterType'] = $this->getFilterType();

    $prefix = $this->getSubmissionPrefix();
    $suffix = $this->getSubmissionSuffix();
    if ($prefix == "{lastname}") {
      $prefix = $student->getLastName().'-';
    }
    if ($suffix == "{lastname}") {
      $suffix = '-'.$student->getLastName();
    }
    
    $root = new Folder($this->getPath().'/'.$prefix.$submittedFolder->getName().$suffix);
    $path = array();
    
    // the users wants to resubmit
    if ($force && $this->hasUserSubmitted($student)) { 
      // set last record's status to inactive
      $submissions = $this->getUserSubmissions($student);
      $submissions[0]->setStatus(Submission::STATUS_INACTIVE);
      $recordUpdated = $submissions[0]->updateRecord();
      
      if ($recordUpdated)  {// delete his last submission
        $oldFolder = new Folder($this->getPath().'/'.$prefix.$submissions[0]->getOriginalFolderName().$suffix, $student);
        $folderDeleted = $oldFolder->delete();
        if (!$folderDeleted) {
          $returnData['error'] = 8;
          return $returnData;
        }
      } else {
        $returnData['error'] = 8;
        return $returnData;
      }
    }
    
    
    // write the flag to the directory
    $model = Submission::factory($this->getModel()->getSharedFolderID(), $student->getUserID(), $submittedFolder->getName(), $root->getName());
    if (!$model->saveRecord()) {
      $returnData['error'] = 6; // cannot create record
      return $returnData;
    }
  
    $submissionID = $model->getSubmissionID();
    file_put_contents($submittedFolder->getPath().'/.submitted', $submissionID);
    
    $driveItems = $submittedFolder->getContents();
    $files = $submittedFolder->getFiles();
    
    // start the copying of the submitted folder items
    while (count($driveItems) > 0 || count($files) > 0) {
      $currentItem = array_pop($driveItems);
      if ($currentItem instanceof Folder) {
        $path[] = $currentItem->getName();
        $files = $currentItem->getFiles();
      }
      
      foreach ($files as $file) {
        $filterFlag = 0;
        if ($file instanceof File) {
          
          if ($this->getFilterType() == \model\SharedFolder::FILTER_TYPE_REJECT && in_array(File::getExtension($file->getPath()), $filterArr)) {
            $filterFlag = 1;
          } else if ($this->getFilterType() == \model\SharedFolder::FILTER_TYPE_ACCEPT && !in_array(File::getExtension($file->getPath()), $filterArr)) {
            $filterFlag = 2;
          }
          $currentPath = implode('/', $path);
   
          if (!file_exists($root->getPath().'/'.$currentPath)) {
            mkdir($root->getPath().'/'.$currentPath, 0777, true);
          }
          
          if ($filterFlag == 0 || $file->getName() == '.submitted') {            
            $file->copy($root->getPath().'/'.$currentPath);
            if ($file->getName() != '.submitted') {
              $returnData['accepted'][] = $file->getRelPath();
            }
            
          } else {
            $returnData['rejected'][] = $file->getRelPath();
          }
        }
      }
      
      if ($currentItem instanceof Folder) {
        $folders = $currentItem->getFolders();
        foreach($folders as $folder) {
          $driveItems[] = $folder;
        }
        if (count($folders) == 0) {
          array_pop($path);
        }
      }

      $files = array(); // re-initialize files
    }
    
    $returnData['error'] = 0;
    return $returnData;
  }

}

