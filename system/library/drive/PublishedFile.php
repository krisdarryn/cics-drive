<?php

require_once LIBRARY.'drive/File.php';
require_once MODEL.'Publication.php';
class PublishedFile  extends File {
  
  private $model;
  public function __construct(Publication $model) {
    
    $this->model = $model;
    $this->setUser(User::getByID($this->model->getUserID()));
    $path = DRIVE.$this->getUser()->getUserID().'/.publications'.$this->model->getFilePath();
    $this->setPath($path);
  }
  
  
  
  public function __call($name, $arguments) {

  }
  public function getModel() {
	return $this->model;
  }
  public static function factory($model) {
    if (is_array($model)) {
      $arr = array();
      foreach ($model as $publication) {
        if ($publication instanceof Publication) {
          $file = new PublishedFile($publication);
          $arr[] = $file;
        }
      }
      return $arr;
    }
    
    if ($model instanceof Publication) {
      return new PublishedFile($model);
    }
    return null;
  }
}