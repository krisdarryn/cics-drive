<?php
/**
 * @package library
 */

require_once LIBRARY.'drive/Folder.php';
require_once LIBRARY.'drive/File.php';
require_once LIBRARY.'drive/DriveItem.php';
require_once MODEL.'User.php';

/**
 * A base class for all the Drives of the system.
 * StudentDrive, FacultyDrive, AdminDrive should extend this class.
 * @filesource /system/library/Drive.php
 */
 class ItemRecovery {
	
 	/**
 	 * @var Folder The absolute path of the current user's root.
 	 */
 	private $root = null;

 	/**
 	 * @var Folder The current folder being used.
 	 */
 	private $currentFolder = null;
	
	/**
	* @var current storage of the user
	*/
	private $currentStorageCapacity = null;
	
	/**
	* @var user detail
	*/
	private $User = null;
	
	/**
	*	@var use for view user drive by administrator
	*/
	private $viewdrive;
	
	/**
	*	@var use as the user object when viewing a user drive by the administrator
	*/
	private $viewuser;
	
 	/**
 	 *	__construct() - Create an instance of a Drive.
 	 *  If no parameter is set, the current folder will be set to the root.
 	 * @param Folder $currentFolder (Optional) This will set the drive to open 
 	 * the current folder.
 	 */
 	public function __construct($user = null, $viewdrive = null, $viewuser = null) {
		
		$this->User = $user;
		$this->viewdrive = $viewdrive;
		$this->viewuser = $viewuser;
		
		if( $this->viewdrive != null && $this->viewdrive['viewdrive'] ){
			switch($this->viewdrive['viewtype']){ // 0 for user - 1 for admin
				case 0: $rootFolder = new Folder( DRIVE.$this->viewuser->getUserID().DriveItem::RECYCLE,$this->viewuser );
						break;
				case 1: $rootFolder = new Folder( DRIVE.';ADMINRECYCLE;/'.$this->viewuser->getUserID(),$this->viewuser );
						break;
				case 2: $rootFolder = new Folder( DRIVE.$this->User->getUserID(),$this->User );
						break;
			}
		}else{
			$rootFolder = new Folder( rtrim(DRIVE.$this->User->getUserID().DriveItem::RECYCLE, '/'),$this->User ); // points to the ;recycle; folder
		}
                
 		$this->setRoot($rootFolder);
 	}

 	/**
 	 * getRoot() - Gets the user's root directory.
 	 * @return Folder An instance of the folder of the root directory.
 	 */
 	public function getRoot() {
 		return $this->root;
 	}
	
 	/**
 	 *	setRoot() - Sets the location of the root folder of the user.
 	 */
 	final private function setRoot(Folder $root) {
 		$this->root = $root;
 	}
 	
	/**
	 * getCurrentFolder() - Gets the current folder being used.
	 * @return Folder An instance of the folder currently opened.
	 */
 	public function getCurrentFolder() {
 		if ($this->currentFolder == null)
 			$this->setCurrentFolder($this->getRoot());
 		return $this->currentFolder;
 	}
	
	/**
 	 * setCurrentFolder() - Opens a different folder in this instance.
 	 * @return void
 	 */
 	public function setCurrentFolder(Folder $folder) {
 		$this->currentFolder = $folder;
 	}
	
	/**
 	 * getContents() - Gets the contents of the current Folder.
 	 * @return DriveItem[] the items of the drive.
 	 */
 	public function getContents() {
 		return $this->getCurrentFolder()->getContents(true);
 	}
	
 	public function getTrashedContents() {
 		$contents = $this->getContents();
 		$str = '';
		
 		if (count($contents) > 0)
	 		foreach ($contents as $content) {
				if ( strrpos($content->getName(),';') > -1)
						continue;
						
				if ($content instanceof Folder){
					$str .= '<a>';
					$str .= '<li class="folder"  title="'.$content->getName().'">';
					$str .= '<span id="driveItem">'.$content->getName().'</span>';
				}
				
	 			if ($content instanceof File){
					$str .= '<a>';
	 				$str .= '<li class="file '.File::getExtension($content->getPath()).'" data-rel-path="'.$content->getRecycleRelPath().'"  title="'.$content->getName().'">';
					$str .= 'Name:  <span id="driveItem">'.$content->getName().'</span>';
					$str .= '<br>'. 'Size:  <strong id="itemSize">'.$content->getFormatedSize().'</strong>';
				}
				
				
	 			$str .= '</li></a>';
	 		}
 		return $str;
 	}
	
	//delete item
	public function delete($driveItem){
		$currentFolder = rtrim($this->getCurrentFolder()->getPath(),'/').'/'.$driveItem;
		$item = DriveItem::createDriveItem( $currentFolder, $this->User );
		
		if($item!=null){
			if($item instanceof File){
				return $item->delete();
			}
			if($item instanceof Folder){
				return $item->delete();
			}
		}
	}	
	
	//restore item
	public function restore($driveItem){
		$currentFolder = rtrim($this->getCurrentFolder()->getPath(),'/').'/'.$driveItem;
		$item = DriveItem::createDriveItem( $currentFolder, $this->User );

		if($item!=null){
			if($item instanceof File){
				return $item->restore();
			}
			if($item instanceof Folder){
				return $item->restore();
			}
		}
	}	
}
