<?php
/**
 * The base exception of the whole system.
 * 
 *@package library
 */

require_once LIBRARY.'Page.php';

class DriveRuntimeException extends RuntimeException {
	
	protected $page;
	
	public function __construct($message = "", $code = 0, $previous = null) {
		parent::__construct($message, $code, $previous);
		$this->page = new Page();
		$pageData = array(
						'message' => parent::__toString(),
						'stacktrace' => parent::getTrace()
					);
		$this->page->bindData($pageData);
	}
}