<?php

class ZipWrapper {

	private $zipArchive;
	private $user;
	
	public function __construct($user){
		$this->user = $user;
	}
	
	public function setZipArchive(ZipArchive $arch, $user=null) {
		$this->zipArchive = $arch;
	}

	public function getZipArchive() {
		return $this->zipArchive;
	}
	
	//download a folder transform into a zip file
	public function compressForDownload($path){
		
		if( is_dir($path) ){
			$folder = new Folder($path);
			$this->zipArchive->addEmptyDir( str_replace(DRIVE.$this->user->getUserID().'/', '', $folder->getPath()) );
			
			if( count($folder->getContents()) > 0 ){
					foreach( $folder->getContents() as $key => $content){
						if( $content instanceof Folder ){
							$this->compressForDownload($content->getPath());
						}
						if( $content instanceof File ){
							$this->zipArchive->addFile($content->getPath(), str_replace(DRIVE.$this->user->getUserID().'/', '', $content->getPath()) );
						}
					}					
			}else{
				return;
			}
		}
		if( is_file($path) ){
			$this->zipArchive->addFile($path, str_replace(DRIVE.$this->user->getUserID().'/', '', $path) );
		}
	}
	
	//multiple download
	public function multipleDownload( $argItem ){
		
		foreach( $argItem as $items ){
			if( is_dir($items) ){
				debug_print_r($items.'/"*"');
				debug_print_r(glob($items.'/"*"'));
				return;
/* 				$innerItems =  array_diff(scandir($items),array('.','..'));
				foreach($innerItems as $inneritem ){
					
				} */
				/* $this->zipArchive->addEmptyDir( str_replace(DRIVE.$this->user->getUserID().'/', '', $items) ); */
			}
			if( is_file($items) ){
				$this->zipArchive->addFile($items, str_replace(DRIVE.$this->user->getUserID().'/', '',  $items) );
			}
		}
	}
	
	
	public function addDir($path,$backup=null) { 
	    
        $this->zipArchive->addEmptyDir(str_replace(DRIVE, '', $path)); 
	    $nodes = glob($path . '/*'); 
		
            foreach ($nodes as $node) { 
                if( is_dir($node) && ($this->user != null && $this->user->getUserType()==2) && ( (strpos($node, ';recycle;') > -1)  ) ){
                    continue;
                }
                if( (strpos($node, 'LOGS') > -1) && $backup ){
                    continue;
                }
	        if (is_dir($node)) {
                    $this->addDir($node); 
                } else if (is_file($node))  {
                    $this->zipArchive->addFile($node, str_replace(DRIVE, '', $node)); 
                }
	    } 
	} 
        
        public function addNodeToArchive($path,$drive_path){
            $result = false;
            
            if(is_file($path)){
                if( $this->zipArchive->addFile($path, str_replace($drive_path, '', $path)) ){
                    $result = true;
                }
            }
            if(is_dir($path)){
                $nodes = glob($path.'/*');
                
                if( count($nodes)>0 ){
                    foreach ($nodes as $node) {
                        
                        if (is_dir($node)) {
                            $this->addNodeToArchive($node, $drive_path);
                        }else if(is_file($node)){
                           
                           if( $this->zipArchive->addFile($node, str_replace($drive_path, '', $node)) ){
                               $result = true;
                           } 
                        }
                    }
                }else{
                    
                    if( $this->zipArchive->addEmptyDir(str_replace($drive_path, '', $path)) ){
                       $result = true;
                    }
                }
            }
            /*debug_print_r(var_dump($result));
            debug_print_r($path);*/
          
            return $result;
        }
}
