<?php
/**
 * A class to handle all the inputs from the client. It should be used to access post and get data and filter against XSS.
 * It also includes access to $_FILES
 * @package library
 * @filesource /system/library/Input.php
 */
class Input {
    
    private static function filter($data) {
        if (is_array($data)) {
            $arr = array();
            foreach ($data as $key => $value) {
                if (is_array($value))
                    return self::filter($value);
                $arr[$key] = $value;
            }
            return $arr;
        }
        
        return htmlentities($data);
    }

  /**
   * post()
   * 
   * @param string $name The name of the POST index.
   * @param boolean $xssFilter Apply the XSS Filter if true otherwise just return the value.
   * @return string|null Will return the value of the POST data specified by $name or null if $_POST[$name] does not exist.
   */
  public static function post($name, $xssFilter = true) {
    if (isset($_POST[$name])) {
	  if(is_array($_POST[$name]))
		return $_POST[$name];
      if (!$xssFilter)
        return $_POST[$name];
      return self::filter($_POST[$name]);
    }
    return null;
  }
  
  /**
   * get()
   * @param string $name The name of the GET index.
   * @param boolean $xssFilter Apply the XSS Filter if true otherwise just return the value.
   * @return string|null Will return the value of the GET data specified by $name or null if $_GET[$name] does not exist.
   */
  public static function get($name, $xssFilter = true) {
    if (isset($_GET[$name])) {
      if (!$xssFilter)
        return $_GET[$name];
      return self::filter($_GET[$name]);
    }
    return null;
  }

  /**
   * file()
   * @param string $name Basically just $_FILES[$name].
   * @return array Will return the array of the file uploaded or null if the index does not exist.
   */
  public static function file($name) {
    if (isset($_FILES[$name])) {
      return $_FILES[$name];
    }
    return null;
  }

  /**
   * hasPost() - Checks for any POST data.
   * @return boolean True if there is POST data, false otherwise.
   */
  public static function hasPost() {
    return count($_POST) > 0;
  }

   /**
   * hasGet() - Checks for any GET data.
   * @return boolean True if there is GET data, false otherwise.
   */
  public static function hasGet() {
    return count($_GET) > 0;
  }

  /**
   * hasFile() - Checks for any files uploaded.
   * @return boolean True if there are files uploaded, otherwise false.
   */
  public static function hasFile() {
    return count($_FILES) > 0;
  }
}