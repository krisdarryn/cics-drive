<?php
require_once LIBRARY.'Database.php';
require_once LIBRARY.'ZipWrapper.php';
require_once MODEL.'Util.php';
require_once LIBRARY.'drive/StudentDrive.php';
require_once LIBRARY.'drive/Folder.php';
require_once LIBRARY.'drive/File.php';

class Backup{
    
    private $listTable;
    
    private $backup_name;
    
    private $table_log;
    
    private $drive_log;
    
    private $avatar_log;
    
    public function __construct() {
       /* $this->backup_name = ( Session::get('backup_path')!=null )?Session::get('backup_path'):null;
        
        if( !file_exists( $this->backup_name ) ){
            mkdir( $this->backup_name );
        }*/
        
        $this->populateList();
    }
    
    /**
     * 
     */
    public static function createBackupPath(){
        Session::set('backup_path',date('F_d_Y_h_i_s_a'));
    }
    
    /**
     * 
     */
    private function populateList(){
        $db = Database::getInstance();
        $db->query("SHOW TABLES");
        $result = $db->resultset();
        
        $this->listTable = array();
                
        foreach($result as $key => $res){
            if(is_array($res)){
                foreach($res as $r){
                    $this->listTable[] = $r;    
                }
            }else{
                $this->listTable[] = $res;
            }
        }
    }
    
    /**
     * @method backupDrive - copying all user drive
     */
    public function backupDrive($driveName,$log_path,$path){
       
        $this->copyItemForBackup(new Folder(DRIVE.$driveName), $path);
        
        Util::logFinishArchive($log_path, $driveName);
        
        return true;
    }
    
    /**
     *  @method backupAvatar - copy all avatar in the current state of the system
     */
    public function backupAvatar($avatarName,$log_path,$path){
        
        $this->copyItemForBackup(new File(AVATAR.$avatarName), $path);
        
        Util::logFinishArchive($log_path, $avatarName);
        
        return true;
    }
    
    /**
     * 
     * @param type $item
     * @param type $drivepath
     * @return type 
     */
    public function copyItemForBackup($item,$drivepath,$file=null){
        if( $item instanceof Folder ){
            if(!file_exists($drivepath.$item->getName())){
                mkdir($drivepath.$item->getName(),0777);
            }
            if(count($item->getContents())>0){
                foreach($item->getContents() as $key => $itm ){
                    $this->copyItemForBackup($itm, $drivepath.$item->getName().'/');
                }
            }
        }
        if( $item instanceof File ){
            copy($item->getPath(),$drivepath.$item->getName());
        }
    }
    
    /**
     * @method getListTable - returning array of table names
     * @return type 
     */
    public function getListTable(){
        return $this->listTable;
    }
    
    /**
     * @method exportDBTable - exporting all data in every table in the DB
     */
    public function exportDBTable($tableNames=null,$log_path=null,$path=null,$sqlName=null){
        
        $importSql = MYSQLDUMP.' --user='.DB_USERNAME.' --password="'.DB_PASSWORD.'" '.DB_NAME.' > cicsdrive2.sql';
        $out=0;
        $int=0;
        exec($importSql,$out,$int);
        foreach($tableNames as $key => $table){
            Util::logFinishArchive($log_path, $table);
        }
        return $int==0?true:false;
    }
    
    
    /**
     * @method exportTofile - export table to a text file
     * @param type $result
     * @param type $file
     * @param type $tbl_name 
     */
    private function exportTofile($result, $file, $tbl_name){
        if( count($result) > 0 ){
           $ctr=0;
           foreach($result as $in => $res){
                ++$ctr;
                $count = 0;
                fwrite($file, '(');
               foreach($res as $key => $r){
                   ++$count;
                   
                   if( strpos(Util::getFieldType($tbl_name, $key),'int') > -1 || strpos(Util::getFieldType($tbl_name, $key),'decimal') > -1  ){
                       fwrite($file, $r);
                   }else{
                       fwrite($file, "'".$r."'");
                   }
                   
                   if($count != count($res) ){
                        fwrite($file,',');    
                   }
               }
               
               if($ctr != count($result)){
                   fwrite($file,"),\n");
               }else{
                   fwrite($file,")\n");
               }
           }
       }
    }
    
    public function restoreDB($dbname){
        $sqlCom = MYSQL.' --user='.DB_USERNAME.' --password="'.DB_PASSWORD.'" '.DB_NAME.' < '.$dbname;
        $out=0;
        $int=0;

        exec($sqlCom,$out,$int);
    
        if($int==0){
             unlink($dbname);
        }
        return $int==0?true:false;
    }
    
    public function restoreAvatar($folder){
        foreach($folder->getContents() as $key => $content){
            $this->restoreItemFromBackup($content, AVATAR);
        }
        return true;
    }
    
    public function restoreDrive($folder){
        foreach($folder->getContents() as $key => $content){
            $this->restoreItemFromBackup($content, DRIVE);
        }
        return true;
    }
    
    public function restoreItemFromBackup($item,$drivepath){
        if( $item instanceof Folder ){
            if( !file_exists($drivepath.$item->getName()) ){
                mkdir($drivepath.$item->getName(),0777);
            }
            if(count($item->getContents())>0){
                foreach($item->getContents() as $key => $itm ){
                    $this->restoreItemFromBackup($itm, $drivepath.$item->getName().'/');
                }
            }
        }
        if( $item instanceof File ){
            copy($item->getPath(),$drivepath.$item->getName());
        }
    }
}

?>
