<?php
require_once LIBRARY.'drive/File.php';
require_once LIBRARY.'Page.php';
require_once LIBRARY.'Editor.php';
require_once MODEL.'Publication.php';
require_once MODEL.'Comment.php';

class Publish {
	private $page;
	private $file;
    private $template;
    private $publication;
	public function __construct(Page $page, Publication $publication, File $file, $template) {
		$this->page = $page;
		$this->page->addAsset('editor/lib/codemirror.js');
		$this->page->addAsset('editor/mode/php/php.js');
		$this->page->addAsset('editor/mode/clike/clike.js');
		$this->page->addAsset('editor/mode/css/css.js');
		$this->page->addAsset('editor/mode/xml/xml.js');
		$this->page->addAsset('editor/mode/vb/vb.js');
		$this->page->addAsset('editor/mode/sql/sql.js');
		$this->page->addAsset('editor/mode/javascript/javascript.js');
		$this->page->addAsset('editor/mode/htmlmixed/htmlmixed.js');
		$this->page->addAsset('editor/theme/eclipse.css');
		$this->page->addAsset('codemirror.css');
		$this->page->addAsset('publish.js');
		$this->page->addAsset('publish.css');

		$this->file = $file;
                $this->publication = $publication;
        
        $this->setTemplate($template);        
	}


	public function getEditorContents() {

		$pageData['document'] = $this->file->getName();
        $pageData['description'] = $this->publication->getDescription();
		$pageData['mimeType'] = $this->file->getMimeType(true);
		$pageData['fileContents'] = $this->file->getContents();
        $pageData['comments'] = $this->getComments();
		$this->page->setContent($this->getTemplate(), $pageData);
	}
    
    public function getPageData() {
		$pageData['document'] = $this->file->getName();
        $pageData['description'] = $this->publication->getDescription();
		$pageData['mimeType'] = $this->file->getMimeType(true);
		$pageData['fileContents'] = $this->file->getContents();
        $pageData['comments'] = $this->getComments();
        return $pageData;
    }
    
    public function removeComment($commentID) {
      $comment = Comment::getById($commentID);
      if ($comment != null) {
       $comment->setStatus(Comment::STATUS_INACTIVE);
       return $comment->updateRecord();
      }
      return false;
    }
    
    public function getCommenters() {
      return $this->publication->getCommenters();
    }
    
    public function getMutes() {
      return $this->publication->getMutes();
    }

	public function getComments() {
        return Comment::getByPublicationID($this->publication->getPublicationID());
    }
    
    public function getTemplate() {
      return $this->template;
    }

    public function setTemplate($template) {
      $this->template = $template;
      return $this;
    }


}
