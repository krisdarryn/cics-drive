<?php

require_once LIBRARY.'BreadCrumb.php';
class GenericBreadCrumb extends BreadCrumb {
  
	public function getTrailHtml($label = null) {
 		$str = '<a href="'.$this->getBaseUrl().'">'.$label.'</a>';
 		$path = '';
 		foreach ($this->getTrail() as $i => $trail) {
 			$path .= '/'.$trail;
 			$str .= '<span class="icon breadcrumb">&#59234;</span> <a href="'.$this->getBaseUrl().$path.'">'.$trail.'</a>';
 		}
 		return $str;
	}
}

