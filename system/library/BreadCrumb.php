<?php

class BreadCrumb {
	private $baseUrl;
	private $trail = array();
	private $viewuser;
	
	public function __construct($base, $trailString,$viewuser = false) {
		$this->setBaseUrl($base);
		$this->setTrail($trailString);
		$this->viewuser = $viewuser;
	}

	public function setBaseUrl($url) {
		$this->baseUrl = $url;
	}

	public function getBaseUrl() {
		return $this->baseUrl;
	}

	public function setTrail($trail) {
 		$breadcrumb = explode('/', $trail); // split the string by the / delimiter and exclude the first element
 		unset($breadcrumb[0]);
 		$this->trail = $breadcrumb;
	}

	public function getTrail() {
		return $this->trail;
	}
	public function getTrailHtml() {
		$temp = ($this->viewuser)?'User\'s Drive':'My Drive';
 		$str = '<a href="'.$this->getBaseUrl().'">'.$temp.'</a>';
 		$path = '';
 		foreach ($this->getTrail() as $i => $trail) {
 			$path .= '/'.$trail;
 			$str .= '<span class="icon breadcrumb">&#59234;</span> <a href="'.$this->getBaseUrl().$path.'">'.$trail.'</a>';
 		}
 		return $str;
	}

    public function getBreadcrumb($baseUrl, $trail) {

            return new BreadCrumb($baseUrl, $trail);
    }

	public function __toString() {
		return $this->getTrailHtml();
	}
}

require_once LIBRARY.'DriveRuntimeException.php';
class InvalidBreadCrumbUrlException extends DriveRuntimeException {

}