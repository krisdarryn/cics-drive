<?php
require_once LIBRARY.'drive/File.php';
class Image {
  
  

  private $imagePath;
  private $type;
  private $image;
  
  public function __construct($path) {
    
    if (!self::isExtensionLoaded()) {
      throw new ImageException('GD extension is required.');
    }
    $this->setImagePath($path);
    $this->getType();
    $this->createResource();
  }
  
  private function createResource() {
    $type = $this->getType();
    switch($type) {
        case IMAGETYPE_GIF:
          $this->image = imagecreatefromgif($this->getImagePath());
          break;
        case IMAGETYPE_JPEG:       
          $this->image = imagecreatefromjpeg($this->getImagePath());
          break;
        case IMAGETYPE_PNG :
           $this->image = imagecreatefrompng($this->getImagePath());
          break;
        default:
          throw new ImageException('not an acceptable image type');
    }
  }
  
  public function getType() {
    if ($this->type == null) {
      $type = exif_imagetype($this->getImagePath());
      switch ($type) {
        case IMAGETYPE_GIF:
        case IMAGETYPE_JPEG:       
        case IMAGETYPE_PNG :
          $this->type = $type;
          break;
        default:
          throw new ImageException('not an acceptable image type');
      }
    }
    return $this->type;
  }
  
  public function getImagePath() {
    return $this->imagePath;
  }

  public function setImagePath($imagePath) {
    if (count(preg_match('/[\w]+.jpg|[\w]+.jpeg|[\w]+.png|[\w]+.gif/', $imagePath)) == 0) {
      throw new ImageException('not an acceptable image type');
    }
    $this->imagePath = $imagePath;
  }
  
  public function createThumbnail($thumbWidth, $thumbHeight, $filename) {
    $resizedImage = self::abstractImageFactory($thumbWidth, $thumbHeight);
    // starting point for the drawing of the thumbnail
    $dstX = 0;
    $dstY = 0;

    // the size of the source image
    $sourceWidth = imagesx($this->image);
    $sourceHeight = imagesy($this->image);
    $sourceX = 0;
    $sourceY = 0;

    // get the max size of a square in the source image para kung asa ta mag sugod ug kuha ug pixels 
    $min = min($sourceWidth, $sourceHeight);
    $max = max($sourceWidth, $sourceHeight);
    $padding = ceil($max - $min);

    // kung tall ang image
    if ($sourceHeight == $max) {
      $sourceY = $padding; // padding sa height;
      $sourceX = 0;
    }
    // kung wide ang image
    if ($sourceWidth == $max) {
      $sourceX = $padding; // padding sa width
      $sourceY = 0;
    }

    $sourceWidth = $min;
    $sourceHeight = $min;

    imagecopyresampled($resizedImage, $this->image, $dstX, $dstY, $sourceX, $sourceY, $thumbWidth, $thumbHeight, $sourceWidth, $sourceHeight);
    $type = $this->getType();
    switch ($type) {
      case IMAGETYPE_GIF:
        imagegif($resizedImage, $filename);
        break;
      case IMAGETYPE_JPEG:       
        imagejpeg($resizedImage, $filename);
        break;
      case IMAGETYPE_PNG :
         imagepng($resizedImage, $filename);
        break;
    }
   }
  

  public static function isExtensionLoaded() {
    return function_exists('gd_info');
  }
  
  public static function abstractImageFactory($width, $height) {
    return imagecreatetruecolor($width, $height);
  }
}

class ImageException extends DriveRuntimeException {
  
}
