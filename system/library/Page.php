<?php
require_once LIBRARY.'drive/File.php';

/**
 * A wrapper for HTML Pages.
 * A page is divided into 3 segments that are each optional:
 * <ul>
 *  <li>header</li>
 *  <li>content</li>
 *  <li>footer</li>
 * </ul>
 * 
 * A Page object can be echoed directly to show the whole html page.
 *
 *
 * <h3>Loading a template</h3>
 * <p>This example shows how to load a template and display it to the browser:</p>
 *<code>
 * $page = new Page();
 * $page->setHeader('guest/header.php'); // loads the template file at template/guest/header.php
 * $page->setFooter('guest/footer.php'); // same concept as above
 * $page->setContent('guest/login.php');
 * $page->setTitle('Login - CICS Drive'); // sets the value of the title tag
 * echo $page; // echo the page directly to the client
 *</code>
 *
 * <p>A shorter version of this is:</p>
  *<code>
 * $page = new Page('guest/header.php', 'guest/footer.php', 'guest/login.php');
 * $page->setTitle('Login - CICS Drive'); // sets the value of the &lt;title&gt; tag
 * echo $page; // echo the page directly to the client
 *</code>
 *
 * The second example has the same effect as the first one.
 * 
 * <h3>Binding data to your templates</h3>
 * <p>It is possible to bind data to your templates. After binding the data, 
 * they can be used as normal variables inside the template files.</p>
 *<code>
 * $pageData = array('name' => 'Paul Daniel');
 * $page->setContent('guest/login.php', $pageData);
 *</code>
 * Inside the template file:
 *<code>
 * ... some html ...
 *  Welcome &lt;?php echo $name;?&gt; &lt;!-- displays &quot;Welcome Paul Daniel&quot; --&gt;
 * ... some html ...
 *</code>
 * In the last example the variable $name from the template file is taken 
 * from $pagedata that was included in the arguments of setContent().
 *
 *
 * <h3>Assets</h3>
 * <p>Assets are css and javascript files. This class provides and a way to manage your 
 * assets without the need to edit the template files. When adding an asset, the class 
 * will automatically determine the type of asset added by its file extension. CSS files 
 * are loaded inside the header tag while Javascript files are loaded before the &lt;body&gt;'s closing tag.
 * The class will automatically add the html for the assets during the loading of the template.</p>
 * <p>To add an asset, simply invoke the addAsset() method:</p>
 *<code>
 * $page = new Page('guest/header.php', 'guest/footer.php', 'guest/login.php');
 * $page->addAsset('styles.css');
 * $page->addAsset('jquery.css');
 * echo $page; // echo the page directly to the client
 *</code>
 * Make sure the asset you have added is actually accessible by the web browser.
 *
 *
 * @package library
 */
class Page {
	/**
	 * Holds the filename of the header template.
	 * @var string
	 */
	protected $header = null;
	
	/**
	 * Holds the filename of the content template.
	 * @var string
	 */
	protected $content = null;
	
	/**
	 * Holds the filename of the footer template.
	 * @var string
	 */
	protected $footer = null;
	
	/**
	 * Holds the filenames of the html assets.
	 * @var string[][]
	 */
	protected $assets = array();
	
	/**
	 * The value of the &lt;title&gt;
	 * @var string
	 */
	protected $title;
	
	/**
	 * The data of the html page to be bound.
	 * @var mixed[]
	 */
	protected $pageData = array(
							'header' => array(),
							'content' => array(),
							'footer' => array()
						);

	/**
	 * The alert messages to be displayed.
	 * @var string[]
	 */
	protected $alerts = array();

	/**
	 * __construct() - Creates and instance.
	 * Initializes the properties.
	 * @param string $header 	The filename of the header template.
	 * @param string $content 	The filename of the content template.
	 * @param string $footer 	The filename of the footer template.
	 */
	public function __construct($header = '', $footer = '', $content = '') {
		if (!empty($header))
		$this->setHeader($header);
		if (!empty($footer))
		$this->setFooter($footer);
		if (!empty($content))
			$this->setContent($content);

		$this->assets['js'] = array();
		$this->assets['css'] = array();
	}
	
	/**
	 * getHeader() - Loads the header template file.
	 * @return string The template files with the bound data.
	 */
	public function getHeader() { 
		try {
			return $this->loadTemplate($this->header, $this->pageData['header']); 	
		} catch (PageException $e) {
			return $e->getMessage();
		}
	} 

	/**
	 * getContent() - Loads the content template file.
	 * @return string The template files with the bound data.
	 */
	public function getContent() { 
		try {
			return $this->loadTemplate($this->content, $this->pageData['content']); 	
		} catch (PageException $e) {
			return $e->getMessage();
		}
	} 

	/**
	 * getContent() - Loads the footer template file.
	 * @return string The template files with the bound data.
	 */
	public function getFooter() {
		try {
			return $this->loadTemplate($this->footer, $this->pageData['footer']); 	
		} catch (PageException $e) {
			return $e->getMessage();
		}
		
	} 

	/**
	 * getAssets() - Gets the current assets as an array.
	 * @return string[][] An array of the filenames of the assets.
	 */
	public function getAssets() { 
		return $this->assets; 
	} 

	/**
	 * getTitle() - Gets the title page value.
	 * @return string The title.
	 */
	public function getTitle() { 
		return $this->title; 
	} 

	/**
	 * setHeader() - Sets the filename of the header template.
	 * @param string $fileName the file name of the header template.
	 * @param mixed[] $vars An array of the data to bind to the template.
	 * @return void
	 */
	public function setHeader($fileName, $vars = array()) {
		$this->header = $fileName; 
		$this->pageData['header'] = $vars;
	} 
	/**
	 * setContent() - Sets the filename of the content template.
	 * @param string $fileName the file name of the content template.
	 * @param mixed[] $vars An array of the data to bind to the template.
	 * @return void
	 */
	public function setContent($fileName, $vars = array()) {
		$this->content = $fileName; 
		$this->pageData['content'] = $vars;
	} 

	/**
	 * setFooter() - Sets the filename of the footer template.
	 * @param string $fileName the file name of the footer template.
	 * @param mixed[] $vars An array of the data to bind to the template.
	 * @return void
	 */
	public function setFooter($fileName, $vars = array()) {
		$this->footer = $fileName;
		$this->pageData['footer'] = $vars;
	} 

	/**
	 * loadTemplate() - Loads a template file specified by a file name.
	 * Data may be attached or bound to the template by including the second parameter.
	 *<code>
	 * $pageData = array('name' => 'Paul Daniel');
	 * Page::loadTemplate('guest/login.php', $pageData);
	 *</code>
	 * Inside the template file:
	 *<code>
	 * ... some html ...
	 *  Welcome &lt;?php echo $name;?&gt; &lt;!-- displays &quot;Welcome Paul Daniel&quot; --&gt;
	 * ... some html ...
	 *</code>
	 * 
	 * It includes() the template file and buffers the output to disable PHP 
	 * from directly displaying the template during the include.
	 * @return string The string of the process template. This includes the data which was bound.
	 */
	private function loadTemplate($fileName, $vars = array()) {

		if ($fileName == null || empty($fileName))
			throw new PageException('No template filename specified');
		ob_start();
		if (count($vars) > 0 )
			extract($vars);
		include TEMPLATE.$fileName;
		
		$template = ob_get_contents();

		ob_end_clean();
		return $template;
	}
	/**
	 *@deprecated 1.0	No use for this method.
	 */
	public function bindData($key, $value = null) {

		if (is_array($key)) {
			$this->pageData = array_merge($this->pageData, $key);
			return;
		}

		if ($value == null)
			throw new PageException("value cannot be null", 1);
			
		$this->pageData[$key] = $value;

	}
	
	/**
	 * getCssAssets() - Gets all the css assets in HTML form.
	 * @return string The hypertext markup of the linking of css files.
	 */
	public function getCssAssets() {
		$css = '';
		foreach ($this->assets['css'] as $cssFile) {
			$css .= '<link rel="stylesheet" href="'.CSS.$cssFile.'" media="all" />
';
		}
		return $css;
	}
	/**
	 * getJsAssets() - Gets all the Javascript assets in HTML form.
	 * @return string The hypertext markup of the linking of Javascript files.
	 */
	public function getJsAssets() {
		$js = '';
		foreach ($this->assets['js'] as $jsFile) {
			$js .= '<script src="'.JS.$jsFile.'"></script>
';
		}
		return $js;
	}
	
	/**
	 * setTitle() - Sets the value of the &lt;title&gt;
	 * @param string $title A string value of the title of the page.
	 * @return void
	 */
	public function setTitle($title) { 
		$this->title = $title; 
	}
	

	/**
	 * addAsset() - Adds either a js or css asset to the page.
	 * @param string $name The filename of an asset excluding its path.
	 * @return void
	 */
	public function addAsset($name) {

		$type = File::getExtension($name);

		if ($type == null)
			throw new PageException("Unknown asset type: null", 2);

		if ($type != 'js' && $type != 'css')
			throw new PageException("Unknown asset type: $type", 2);

		if (!in_array($name, $this->assets[$type]))
			$this->assets[$type][] = $name;
			
	}

	public function __toString() {
		$str = '';

		if ($this->header != null)
			$str .= $this->getHeader();
		if ($this->content != null)
			$str .= $this->getContent();
		if ($this->footer != null)
			$str .= $this->getFooter();
		return $str;
	}

	/** 
	 * addAlert() - Add an alert message to the page.
	 *
	 * types of alerts:
	 *   <ul>
	 * 		<li>success: Color green</li>
	 *		<li>warning: color orange</li>
	 * 		<li>error: color red</li>
	 *   </ul>
	 * @param string $message The messsage to display to the client.
	 * @param string $type The type of message to display.
	 * @return void
	 */
	public function addAlert($message, $type = 'notice') {
		$this->alerts[$type][] = $message;
	}

	/**
	 *	getAlertString() - Gets the HTML for the alerts.
	 *  Usually used in templates.
	 *  @return string The HTML of the alerts.
	 */
	public function getAlertString() {
		$alertString = '';
		foreach ($this->alerts as $type => $messages) {
			switch ($type) {
				case 'error':
					$color = 'red';
					break;
				case 'success':
					$color = 'green';
					break;
				default:
					$color = 'orange';
			}
			foreach ($messages as $message) {
				$alertString .= '
					<section class="alert">
						<div class="'.$color.'">	
							<p>'.$message.'</p>
							<span class="close">✖</span>
						</div>
					</section>
				';
			}
		}
		return $alertString;
	}

	/**
	 * getAlertHtml() - Alias of Page::getAlertString()
	 */
	public function getAlertHtml() {
		return $this->getAlertString();
	}
	/**
	 * getImageUrl() - Prepends the URL of the image.
	 * @param string $filename The image's filename.
	 * @return string The URL of the image.
	 */
	public static function getImageUrl($fileName) {
		return IMAGES.$fileName;
	}
}		

/**
 * An subclass of DriveRuntimeException to enable the proponents to handle the Page errors.
 */
require_once LIBRARY.'DriveRuntimeException.php';
class PageException extends DriveRuntimeException {
	public function __construct($message = "", $code = 0) {
		parent::__construct($message, $code, null);
	}
}