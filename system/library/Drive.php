<?php
/**
 * @package library
 */

require_once LIBRARY.'drive/Folder.php';
require_once LIBRARY.'drive/File.php';
require_once LIBRARY.'JSONable.php';
require_once LIBRARY.'BreadCrumb.php';
require_once MODEL.'Uploads.php';
require_once MODEL.'User.php';
require_once MODEL.'UserType.php';
require_once LIBRARY.'ZipWrapper.php';

/**
 * A base class for all the Drives of the system.
 * StudentDrive, FacultyDrive, AdminDrive should extend this class.
 * @filesource /system/library/Drive.php
 */
 abstract class Drive implements JSONable {
   
	const FOLDER_FILTER_PATTERN = "/[\/\\:*?\"<>\|\$]/";
        
	
 	/**
 	 * @var Folder The absolute path of the current user's root.
 	 */
 	private $root = null;

 	/**
 	 * @var Folder The current folder being used.
 	 */
 	private $currentFolder = null;

	/**
	* @var user detail
	*/
	private $user = null;
	
	/**
	*	@var use to hold drive items that are being searched
	*/
	public $resultArray = array();
	
	/**
	*	@var use for view user drive by administrator
	*/
	private $viewdrive;
	
	/**
	*	@var use as the user object when viewing a user drive by the administrator
	*/
	private $viewuser;
	
 	/**
         *
         * @var
         */
        private $arc;
        
 	/**
 	 *	__construct() - Create an instance of a Drive.
 	 *  If no parameter is set, the current folder will be set to the root.
 	 * @param Folder $currentFolder (Optional) This will set the drive to open 
 	 * the current folder.
 	 */
 	public function __construct($currentFolder = null, $viewdrive = null, $viewuser = null,$archive=null) {
 		
		$user = Session::getUser();
		$this->user = User::getByID($user['userId']);
		$this->viewdrive = $viewdrive;
		$this->viewuser = $viewuser;
                $this->arc = $archive;

                
		if($this->viewdrive!=null){//for user drive view
			switch($this->viewdrive){// 2 for viewing user drive - 1 for viewing user item recovery
				case 2:	$rootFolder = new Folder( DRIVE.$this->viewuser->getUserID(),$this->viewuser );
						break;
				case 1:	$rootFolder = new Folder( DRIVE.';ADMINRECYCLE;/'.$this->viewuser->getUserID(),$this->viewuser );
			}
		}else if($archive){
                    $rootFolder = new Folder( DRIVE,$this->user ); // root folder for archive
		}else{
			$rootFolder = new Folder( DRIVE.$this->user->getUserID(),$this->user ); // Find the user's root folder
		}
		
                if(!$archive){
                    
                    if (!file_exists($rootFolder->getPath())){ // kung wala pay drive folder ang user kay himo-an
 			mkdir($rootFolder->getPath(), 0777, true);
                    }
                    if (!file_exists($rootFolder->getPath().'/;recycle;') && (strpos($rootFolder->getPath(),'/;recycle;/')===false)){ //creates ;recylcle; folder 
			mkdir($rootFolder->getPath().'/;recycle;', 0777, true);
                    }
		if($this->viewdrive){
                        if ( !file_exists( ADMINRECYCLE.$this->viewuser->getUserID() ) )
                            mkdir(ADMINRECYCLE.$this->viewuser->getUserID(), 0777, true);
		}
                }else if($archive){
                    if (!file_exists(ARCHIVE.$this->user->getUserID().'/LOGS')) // kung wala pay drive folder ang user kay himo-an
                        mkdir(ARCHIVE.$this->user->getUserID().'/LOGS', 0777, true);
                    }
		
 		$this->setRoot($rootFolder);
 		
		if ($currentFolder != null) {

 			if ($currentFolder instanceof Folder) // wala man tingali ni gamit. tangtangon lang siguro ni
 				$this->setCurrentFolder($currentFolder);
			
 			if (is_string($currentFolder)) {
				if($archive){
                                    $folder = new Folder( ARCHIVE.$this->user->getUserID(),$this->user );
                                }else{
				$folder = new Folder( $this->getRoot()->getPath().$currentFolder,$this->user );
                                }
                                
				if($folder->isFile()){
                                  $file =new File($folder->getPath());
                                  if ($file->isAnImage()) {
                                    header('Content-Type:'.$file->getMimeType());
                                    readfile($folder->getPath());
                                  } else {
                                        $file->download();                                    
                                  }
                                  exit();
				}
				$this->setCurrentFolder($folder); 
			}
		}
 		else {
 			$this->setCurrentFolder($this->getRoot());
                        
                }

 		if (!file_exists($this->getCurrentFolder()->getPath()))
 			throw new NoSuchFolderException();
	}

 	/**
 	 * getRoot() - Gets the user's root directory.
 	 * @return Folder An instance of the folder of the root directory.
 	 */
 	public function getRoot() {
 		return $this->root;
 	}

 	/**
	 * getCurrentFolder() - Gets the current folder being used.
	 * @return Folder An instance of the folder currently opened.
	 */
 	public function getCurrentFolder() {
 		if ($this->currentFolder == null)
 			$this->setCurrentFolder($this->getRoot());
 		return $this->currentFolder;
 	}

 	/**
 	 * setCurrentFolder() - Opens a different folder in this instance.
 	 * @return void
 	 */
 	public function setCurrentFolder(Folder $folder) {
 		$this->currentFolder = $folder;
 	}
	
 	/**
 	 *	setRoot() - Sets the location of the root folder of the user.
 	 */
 	final private function setRoot(Folder $root) {
 		$this->root = $root;
 	}
 	/**
 	 * getContents() - Gets the contents of the current Folder.
 	 * @return DriveItem[] the items of the drive.
 	 */
 	public function getContents() {
 		return $this->getCurrentFolder()->getContents();
 	}
	
 	public function getContentsFormatted($uri) {
             
 		$contents = $this->getContents();

        
 		$str = '';
		/* $finfo = new finfo; */
	 	$editorUri = preg_replace('/\/drive\//', '/editor/', $uri, 1);
	 	$editorUri = preg_replace('/\/drive/', '/editor', $uri, 1);
		//$finfo->file($content->getPath(), FILEINFO_MIME) get the mime type of a file
    
 		if (count($contents) > 0)
           
	 		foreach ($contents as $content) {	
				if (!$content instanceof DriveItem) {
					continue;
				}
	 			$itemName = strlen($content->getName()) > 25 ? substr($content->getName(), 0, 25).'...':$content->getName();
	 			if ($content instanceof Folder){
                                        if($this->arc){
                                            continue;
                                        }
	 				if($content->getName() != ';recycle;' && $content->getName() != '.publications'){
						$str .= '<a class="dItem" href="'.$uri.'/'.$content->getName().'">';
                                            
                        if ($content instanceof SharedFolder) {
                          $locked = '';
                          if ($content->getModel()->getAccessMode() == \model\SharedFolder::ACCESS_MODE_READ && $content->getModel()->getPassword() != null) {
                            $locked = ' private';
                          }
                          $str .= '<li class="folder shared'.$locked.'" data-rel-path="'.$content->getRelPath().'" data-id="'.$content->getSharedFolderID().'" title="'.$content->getName().'">';
                        } else if ($content instanceof SubmittedFolder) {
                          $str .= '<li class="folder submitted" data-rel-path="'.$content->getRelPath().'" title="'.$content->getName().'">';
                        } else {
						$str .= '<li class="folder" data-rel-path="'.$content->getRelPath().'"  title="'.$content->getName().'">';
                        }
						$str .= '<span id="driveItem">'.$itemName.'</span>';
					}
				}
                
                if ($content instanceof PublishedFile) {
					$us = Session::getUser();
					$us = $us['userId'];
					$user_temp = User::getById($us);
                  $str .= '<a class="dItem" href="'.$uri.'/'.$content->getModel()->getPublicationID().'">';
                  $str .= '<li class="file '.File::getExtension($content->getPath()).'" data-rel-path="'.$content->getRelPath().'" title="'.$content->getName().'">';
                  $str .= 'Name:  <span id="driveItem">'.$itemName.'</span><br/>';
                  $str .= 'Size:  <strong>'.$content->getFormatedSize().'</strong><br/>';
                  $str .= 'Last Modified:  <strong>'.date("F d Y H:i:s.", fileatime($content->getPath())).'</strong>';
                  if($user_temp->getUserType() == 2){
					$str .= '<form action="" method="post"><button class="small pull-right" style="margin-top:-50px; margin-right:10px;" name="delete" value='.$content->getName().'>Delete</button></form>';
                  }
                  $str .= '</li></a>';

				  continue;
                }
				
	 			if ($content instanceof File){
                    if ($content->getName() == '.shared' || $content->getName() == '.submitted' || $content->getName() == '.deleted') {
                      continue;
                    }
                    $extraClass = '';
                    if ($content->isEditable()) {
	 					$str .= '<a class="dItem" href="'.$editorUri.'/'.$content->getName().'">';
                        $extraClass = 'editable';
                    } else  {
	 					$str .= '<a class="dItem" href="'.$uri.'/'.$content->getName().'">';
                    }

	 				$str .= '<li class="file '.File::getExtension($content->getPath()).' '.$extraClass.'" data-rel-path="'.$content->getRelPath().'" title="'.$content->getName().'">';
                                    $str .= 'Name:  <span id="driveItem">'.$itemName.'</span><br/>';
                                    $str .= 'Size:  <strong>'.$content->getFormatedSize().'</strong><br/>';
                                    $str .= 'Last Modified:  <strong>'.date("F d Y H:i:s.", fileatime($content->getPath())).'</strong>';
				}
		
	 			$str .= '</li></a>';
                                
                                
	 		}
 		return $str;
 	}
	
    public function toJSON() {
            $folderContents = $this->getContents();
            $arr = array();
            $names = array();
            $types = array();
            foreach ($folderContents as $key=>$driveItem) {
                    $class = get_class($driveItem);
                    if ($class == "SharedFolder" || $class == "SubmittedFolder") {
                      $class = "Folder";
                    }
                    if ($driveItem->getName() == ";recycle;") {
                      continue;
                    }
                    $arr[] = array(
                                    'name' => $driveItem->getName(),
                                    'type' => $class,
                                    'path' => $driveItem->getRelPath(),
                                    'editable' => $driveItem instanceof File && $driveItem->isEditable() ? 1:0
                            );
            }
            return json_encode($arr);
    }
	
	//create folder
	public function createFolder($name){
		return $this->getCurrentFolder()->createFolder($name);
	}
	
	//upload file
	public function uploadFile($file, $storageCapacity, $overwrite=null){
		$item = new File( rtrim($this->getCurrentFolder()->getPath(), '/'), $this->user );
		$result =  $item->initUpload($file, $storageCapacity, $overwrite);
		return $result;
	}
	
	//cancel upload file
	public function cancelUpload( $name,$overwrite=null ){
		$item = new File(rtrim($this->getCurrentFolder()->getPath(), '/').'/'.$name , $this->user);
                $result = $item->delete(true,null,$overwrite,$name);
		return $result;
	}	
	
	//remove file from drive
	public function remove($driveItem){
		
		$currentFolder = rtrim($this->getCurrentFolder()->getPath(),'/').'/'.$driveItem;
		
		$item = DriveItem::createDriveItem($currentFolder , $this->user);

		if($item!=null){
			if($item instanceof Folder){
				return $item->remove($this->getRoot()->getPath());
			}
			if($item instanceof File){
				return $item->remove($this->getRoot()->getPath());
			}
		}
	}
	
	//rename folder/file
	function rename($new_name,$old_name){
		$currentFolder = rtrim($this->getCurrentFolder()->getPath(), '/');
       

		$extn = strrchr($old_name,'.');
		$item = DriveItem::createDriveItem($currentFolder.'/'.$old_name, $this->user);
        
		if ($item!=null){
			if ($item instanceof File)
				return $item->rename($old_name,$new_name.$extn);
			if ($item instanceof Folder)
				return $item->rename($old_name,$new_name);
		}
	}
	
    // $driveItem must be a direct descendant of the current directory
    public function download($driveItem) {
		if( count($driveItem) == 1 ){
			$currentFolder = rtrim($this->getCurrentFolder()->getPath(), '/'); // kuhaon nalang daan ang trailing slash bisan naa ba or wala
			$item = DriveItem::createDriveItem($currentFolder.'/'.$driveItem[0]);
			if ($item != null)
				$item->download();
			else
				throw new DriveRuntimeException('Invalid file name for download');
		}else{
			$userID = Session::getUser();
			$temp_user = User::getByID($userID['userId']);
			$archive = new ZipArchive();
			$currentFolder = rtrim($this->getCurrentFolder()->getPath(), '/'); // kuhaon nalang daan ang trailing slash bisan naa ba or wala
			$argItem = array();
			
			foreach( $driveItem as $key => $i){
				$argItem[] = $currentFolder.'/'.$i;
			}
			
			if (!file_exists(TEMP))
				mkdir(TEMP);
						
			$fileName = 'CICSDrive_'.date('h_s').'.zip';
			$archive->open(TEMP.$fileName, ZIPARCHIVE::CREATE) ;
			
			$zipWrapper = new ZipWrapper($temp_user);
			$zipWrapper->setZipArchive( $archive );
			foreach( $argItem as $aitem){
				$zipWrapper->compressForDownload($aitem);
			}
			
			$archive = $zipWrapper->getZipArchive();
			$archive->close();
			
			$filez = new File(TEMP.$fileName);
			$filez->download();
		}	
    }
	
	//breadcrumb
	public function getBreadcrumb($baseUrl, $trail,$view = false) {
            return new BreadCrumb($baseUrl, $trail,$view);
    }
	
	/**
	*	@method searchDeeper - used to search an item in the drive
	*	@parameter Object - Folder object
	*			   String - to be searched
	*/
	public function searchDeeper($folder,$toCompare){
		if(count($folder->getContents())==0){
			return;
		}
		else{
			foreach($folder->getContents() as $key => $content){
				
				if($content->getName() == ';recycle;'){
					continue;
 }
 
				if($content instanceof Folder){
					$this->searchDeeper($content,$toCompare);
				}

				if( strpos( strtolower($content->getName()),strtolower($toCompare) ) !== false ){
						$this->resultArray[] = array(	'type'=> ($content instanceof File)?'file':'folder',
														'id' => $content->getName(),
														'tag' => $content->getName(),
														'size' => ($content instanceof File)?$content->getFormatedSize():-1,
														'extension' => File::getExtension($content->getName()),
														'path' => $content->getRelPath(),
														'modified' => date("F d Y H:i:s.", filectime($content->getPath())) );
				}
			}
		}
		return;
	}
        
        public static function isReservedName($name) {
          $arr = array(
                ';recycle;' => true,
                '.publications' => true,
                '.submitted' => true,
                '.shared' => true,
                'con' => true, 
                'prn' => true, 
                'aux' => true, 
                'nul' => true, 
                'com1' => true, 
                'com2' => true, 
                'com3' => true, 
                'com4' => true, 
                'com5' => true, 
                'com6' => true, 
                'com7' => true, 
                'com8' => true, 
                'com9' => true, 
                'lpt1' => true, 
                'lpt2' => true, 
                'lpt3' => true, 
                'lpt4' => true, 
                'lpt5' => true, 
                'lpt6' => true, 
                'lpt7' => true, 
                'lpt8' => true, 
                'lpt9' => true,
                  );
          return (key_exists(strtolower($name), $arr));
        }
	
 }
 

require_once LIBRARY.'DriveRuntimeException.php';
class NoSuchFolderException extends DriveRuntimeException {

}