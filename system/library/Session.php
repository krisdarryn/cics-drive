<?php

class Session {

  public static function start() {
    session_start();
    Session::updateFlash();
  }

  public static function set($name, $value) {
    return $_SESSION[$name] = $value;
  }

  public static function get($name) {
    if (!isset($_SESSION[$name]))
      return null;
    return $_SESSION[$name];
  }
  
  private static function updateFlash() {
    if (isset($_SESSION['flash'])) {
      foreach ($_SESSION['flash'] as $key => &$flashData) {
        $flash = &$_SESSION['flash'][$key];
        if ($flash[1] > 0) {
          unset($_SESSION['flash'][$key]);
          continue;
        }
        $flash[1]++;
      }
    }
  }

  public static function remove($name) {
    if (isset($_SESSION[$name]))
      unset($_SESSION[$name]);
  }

  public static function isLoggedIn() {
    $userSession = Session::get('user');
    return isset($userSession['userId']) && $userSession['userId'] != null;
  }
  
  public static function logout() {
    if (self::isLoggedIn()){
      unset($_SESSION['user']);
      unset($_SESSION['answer']);
	}
  }
  
  public static function getUser() {
    return Session::get('user');
  }
  
  public static function setFlash($name, $value) {
    $_SESSION['flash'][$name][0] = $value;
    $_SESSION['flash'][$name][1] = 0;
  }
  
  public static function getFlash($name) {
    if (!isset($_SESSION['flash'][$name]))
      return null;
    return $_SESSION['flash'][$name];
  }
  
	/*
	*	addInfo
	*	Parameter : mixed not array, mixed not array, mixed
	*	Add value to the specified session using the 1st parameter as the 1st index value and 2nd parameter as the 2nd index value, the last parameter as the value to be added.
	*	Set the added value into the specified session.
	*	<code>
	*		$arr = ('kris','darryn','dela');
	*		Session::set('registration',$arr);
	*		$value = 'password';
	*		Session::addInfo('registration',3,$value);
	*		~Structure:
	*			$_SESSION{
	*				'registration'{
	*					0 => 'kris';
	*					1 => 'darryn';
	*					2 => 'dela';
	*					3 => 'password';
	*				}
	*			}
	*	<code>
	*	example above shows the use of the function
	*/
	public static function addInfo($name = '',$index = '',$value = ''){
		$_SESSION[$name][$index] = $value;
	}
}