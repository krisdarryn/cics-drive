<?php

require_once PROCESS.'FacultyProcess.php';
require_once MODEL.'SharedFolder.php';
require_once MODEL.'User.php';
/**
 * @package process
 * @subpackage student
 */
class SubmissionReport extends FacultyProcess {
  
	public function run() {
		parent::run();
        $sharedFolder = \model\SharedFolder::getById($this->request->id);
		$submissions = $sharedFolder->getSubmissions();
        $students = array();
        $report = array();
        foreach ($submissions as $submission) {
          $report[$submission->getSubmissionID()]['action'] = 'resubmitted';
          if (!key_exists($submission->getStudentUserID(), $students)) {
            $students[$submission->getStudentUserID()] = User::getByID($submission->getStudentUserID());
            $report[$submission->getSubmissionID()]['action'] = 'submitted';
          }
          
          $report[$submission->getSubmissionID()]['name'] = $students[$submission->getStudentUserID()]->getFirstName() .' '. $students[$submission->getStudentUserID()]->getLastName();
          $report[$submission->getSubmissionID()]['folder'] = $submission->getOriginalFolderName();
          $report[$submission->getSubmissionID()]['date'] = $submission->getSubmissionDate()->format(DateTime::W3C);
        }

        $arr['deadline'] = $sharedFolder->getDeadlineDate()->format(DateTime::W3C);
        $arr['shareDate'] = $sharedFolder->getSharedDate()->format(DateTime::W3C);
        $arr['total'] = count($students);
        $arr['reports'] = $report;
        echo json_encode($arr);
        
	}
}