<?php

require_once PROCESS.'FacultyProcess.php';
require_once LIBRARY.'drive/StudentDrive.php';
require_once LIBRARY.'drive/DriveItem.php';
require_once MODEL.'FileActivityLogs.php';
require_once MODEL.'FileActivityProperties.php';

class ArchiveProcess extends FacultyProcess {
   
  private $arcItem;
  
  private $arcRootItem;
  
  public function run(){
    
    parent::run();
    
    $this->createArchiveFolder();
		
    $uri = $this->request->params();
    
    if (isset($this->request->archiveName)) {
      $path = ARCHIVE.$this->getUser()->getUserID().'/'.urldecode($this->request->archiveName);
      if (file_exists($path)) {
        $file = new File($path);
        $file->download();
        die();
      }
    }
    
    $archive = new StudentDrive(';ARCHIVE;',null,null,true);
    
    if(Input::post('archive')){
        $this->arcItem = array();
        
        $drive = new Folder(DRIVE.$this->getUser()->getUserID(),$this->getUser());
        
        if(count($drive->getContents())==1){
            die(json_encode(0));
        }
        
        $ctr=0;
        foreach($drive->getContents() as $key => $content ){
            
           if($content instanceof Folder){
               if($content->getName() != ';recycle;'){
                   $this->arcRootItem[$ctr]['rootItem'] = $content->getName();
                   $this->arcRootItem[$ctr++]['countContents'] = Folder::countInnerContents($content->getPath())>0?Folder::countInnerContents($content->getPath()):1;
               }
           }
           if($content instanceof File){
               $this->arcRootItem[$ctr]['rootItem'] = $content->getName();
               $this->arcRootItem[$ctr++]['countContents'] = 1;
           }
        }
        die(json_encode($this->arcRootItem));
    }
    
    if(Input::post('items')!=null){
        Session::set('archive_path',date('F_d_Y_h_i_s_a'));
        $log_path = $this->getArchiveFolderLog().Session::get('archive_path').'.txt';
        
        $file = fopen($log_path,'a');
        foreach(Input::post('items') as $key => $item){
            fwrite($file, $item."\n");
        }
        fclose($file);
        die( json_encode(true) );
    }
    
    if(Input::post('item')!=null){
        $item = DRIVE.$this->getUser()->getUserID().'/'.Input::post('item');
        $item_path = DRIVE.$this->getUser()->getUserID().'/';
        $log_path = $this->getArchiveFolderLog().Session::get('archive_path').'.txt';
        
		$ctr=0;
        $state='';
        
        $archive_path = $this->getArchiveFolder().Session::get('archive_path').'.zip';
        
        $zip = new ZipArchive();
        $wrapper = new ZipWrapper($this->getUser());
        
        $zip->open($archive_path, ZipArchive::CREATE);
        $wrapper->setZipArchive($zip);
        
        if(is_dir($item)){
            $ctr = Folder::countInnerContents($item)>0?Folder::countInnerContents($item):1;
            $wrapper->addNodeToArchive($item, $item_path);
			
            Util::logFinishArchive($log_path, Input::post('item'), $this->getArchiveFolderLog());	
        }
        if(is_file($item)){
            $ctr = 1;
            $state = $wrapper->addNodeToArchive($item, $item_path);
            
            Util::logFinishArchive($log_path, Input::post('item'), $this->getArchiveFolderLog());
        }
        
        $zip = $wrapper->getZipArchive();
        $zip->close();
        
		$temp = DriveItem::createDriveItem($item);
		
		if($temp instanceof Folder){
			if( strcmp($temp->getName(),'.publications') == 0 ){
				foreach($temp->getContents() as $key => $con ){
					if($con instanceof File){
						$con->delete(null,true);
					}
				}
			}else{
				foreach($temp->getContents() as $key => $con){
					if( strcmp($con->getName(),'.shared') == 0 ){
						$temp->delete(true);
						break;
					}
				}
			}
		}
        
        die( json_encode($ctr) );
    }
    
    if(Input::get('done')){
        $file = new File($archive_path = $this->getArchiveFolder().Session::get('archive_path').'.zip');
        $this->getUser()->setUsedStorage($this->getUser()->getUsedStorage() + $file->getSize());
        $this->getUser()->updateRecord();
        
        $actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_ARCHIVE,$this->getUser()->getUserID() );
        FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_PATH, '/' ) ;
        
        Session::remove('archive_path');
    }
    
    if(Input::post('deleteArchive')){
        $file = new File($this->getArchiveFolder().Input::post('deleteArchive'),$this->getUser());
        $file->delete(null, true);
    }
    
    if(Input::post('dlArchive')!=null){
        $file = new File($this->getArchiveFolder().Input::post('dlArchive'));
        $file->download();
    }
    

    
    $pageData['contentString'] = $archive->getContentsFormatted(SITE_URL.trim($uri[0], '/'));

    $this->page->addAsset('dropzone.css');
    $this->page->addAsset('jquery.contextMenu.css');
    $this->page->addAsset('jquery.modal.css');
    $this->page->addAsset('drive.menu.css');
    $this->page->addAsset('our.modal.css');
    $this->page->addAsset('editor.css');
    $this->page->addAsset('drive.css');
    $this->page->addAsset('archive.css');


    $this->page->addAsset('select2.min.js');
    $this->page->addAsset('custom_dialog.js');
    $this->page->addAsset('jquery.contextMenu.js');
    $this->page->addAsset('drive.functions.js');
    $this->page->addAsset('drive.js');
    $this->page->addAsset('archive.js');
		
    
    $this->page->setTitle('Archiving - CICS Drive');
    
    
    $this->page->setContent('faculty/archive.php',$pageData);
    
    echo $this->page;
  }
}

