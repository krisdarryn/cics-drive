<?php
/**
 * @package process
 * @subpackage student
 */

require_once PROCESS.'FacultyProcess.php';
require_once LIBRARY.'/drive/StudentDrive.php';
require_once LIBRARY.'drive/Folder.php';
require_once LIBRARY.'drive/SharedFolder.php';
require_once MODEL.'SharedFolder.php';

class DriveProcess extends FacultyProcess {

	/**
	 * @var Drive $drive An instance of a Drive class.
	 */
	private $drive;
	
	/**
	*	@var use as a path of the item that was removed 
	*/
	private $remove_path;
	
	public function run() {
		
		parent::run();
		$uri = $this->request->params();
		
		$storageCopacity = $this->getUserType()->getStorageCapacity();
		
		
		switch($this->getUser()->getUserType()){
			case 1: $this->remove_path = SITE_URL.'admin/item_recovery';
					break;
			case 2:	$this->remove_path = SITE_URL.'faculty/item_recovery';
					break;
			case 3:	$this->remove_path = SITE_URL.'student/item_recovery';			
		}
		
		if (count($uri) > 1) {
			if( array_key_exists(1,$uri) ){
				$uri[1] = '/'.urldecode($uri[1]);
				$uri[1] = rtrim($uri[1], '/');
				
				try {
					$this->drive = new StudentDrive($uri[1]);
				} catch(NoSuchFolderException $e) {
					$this->response->redirect('/404');
				}
			}else{
				$this->drive = new StudentDrive();
			}
		} else { 
			$this->drive = new StudentDrive();
        }
		
        if ( Input::post('moveItems')) {
          $items = array();
          foreach (Input::post('moveItems') as $key => $item) {
            $relPath = urldecode($item);
            $item = DriveItem::createDriveItem(DRIVE.$this->getUser()->getUserID().$relPath);
			
            if ($item instanceof Folder) {
              //$shares = $item->getSharedFolders();
            }
            $items[$key]['destination'] = $this->drive->getCurrentFolder()->getPath().'/'.$item->getName();
            $items[$key]['sourcePath'] = $relPath;
            $items[$key]['exists'] = file_exists($this->drive->getCurrentFolder()->getPath().'/'.$item->getName());
            if (file_exists($this->drive->getCurrentFolder()->getPath().'/'.$item->getName()) === true) {
              $items[$key]['status'] = false;
            } else {
              $items[$key]['status'] = $item->move($this->drive->getCurrentFolder()->getPath());
            }
            
          }
          echo json_encode($items);
          die();
        }
                
        // get shared folder data
        if (Input::get('sharedFolderID')) {
          $this->getSharedFolder(Input::get('sharedFolderID'));
        }
		
		//search item
		if(Input::get('search')){
			$folder = new Folder( DRIVE.$this->getUser()->getUserID(),$this->getUser() );
			$this->drive->searchDeeper($folder,Input::get('search'));
			
			echo json_encode($this->drive->resultArray);
			die();
		}		

		//create folder
		if( Input::post('folder_name') ){
			$match = preg_match(Drive::FOLDER_FILTER_PATTERN, Input::post('folder_name'), $arr);
			if ((!$match || count($arr) == 0) && !Drive::isReservedName(Input::post('folder_name'))){
				$folderName = trim(Input::post('folder_name'));
				$folderName = trim($folderName, '!@#$%^&*()+-=[]{}/\\|;:,.<>"\'');
				if(strlen($folderName) > 0){
                            if ($this->drive->createFolder($folderName))
                                    $this->page->addAlert('Folder <span style="color:#000;">'.$folderName.'</span>  has successfully created.','success');
                            else
                                    $this->page->addAlert('Folder <span style="color:#000;">'.$folderName.'</span> already exist.','notice');
				}	
			} else {
                if (Drive::isReservedName(Input::post('folder_name'))) {
                  $this->page->addAlert('Folder <span style="color:#000;">'.Input::post('folder_name').'</span> is a reserved name','notice');
                } else {
                  $this->page->addAlert('Folder <span style="color:#000;">'.Input::post('folder_name').'</span> Special characters are not allowed','notice');
                }
				
			}
			
			
		} 
		
        if (Input::get('unshare')) {
          $json = array('status' => false);
          $model = \model\SharedFolder::getById(Input::get('unshare'));
          $shared = new SharedFolder($model);
          $shared->getModel()->setStatus(\model\SharedFolder::STATUS_INACTIVE);
          if ($shared->getModel()->updateRecord() == 0) {
            if (file_exists($shared->getPath().'/.shared')) {
              unlink($shared->getPath().'/.shared');
              $json = array('status' => true);
            } else {
              $json['reason'] = 1;
            }
          } else {
            $json['reason'] = 2;
          }
          echo json_encode($json);
          die();
        }
        
		//upload file
		if(!empty($_FILES)){
                        
			$overwrite = Input::post('overwrite') != null?Input::post('overwrite'):false;									
			echo json_encode($this->drive->uploadFile( $_FILES, $storageCopacity, $overwrite  )); 
			exit();
		}
		
		//cancel uplaod file
		if( Input::post('del') ){
			echo json_encode( $this->drive->cancelUpload( Input::post('del'),Input::post('overwrite') ) );
			exit();
		}
		
		//remove folder/file
		if(Input::post('remove')){	
			$items = Input::post('remove');
			$count=0;
			foreach($items as $item){
				if($this->drive->remove($item)){
					$count++;
				}
			}
			$this->page->addAlert($count.' Item(s) has been moved to the <a href="'.$this->remove_path.'" style="color:blue;">Item Recovery</a>','success');
		}
		
		//download folder/file
		if ( Input::post('download') ) {
			try {
				$temp = array();
				foreach( Input::post('download') as $i ){
					$temp[] = urldecode($i);
				}
				$this->drive->download($temp);
			} catch (DriveRuntimeException $e) {
				$this->page->addAlert($e->getMessage().': <strong>'.Input::get('download').'</strong>', 'warning');
			}
		}
		
		//rename folder/file
		if(Input::post('renameItem') && Input::post('current_name') ){
			$match = preg_match(Drive::FOLDER_FILTER_PATTERN, Input::post('renameItem'), $arr);
			if ((!$match || count($arr) == 0) && !Drive::isReservedName(Input::post('renameItem'))){
				$folderName = trim(Input::post('renameItem'));
				$folderName = trim($folderName, '!@#$%^&*()+-=[]{}/\\|;:,.<>"\'');
				if($folderName!=null && trim(Input::post('current_name'))!=null){
		
					if($this->drive->rename($folderName,Input::post('current_name'))){
						if(is_dir($this->drive->getCurrentFolder()->getPath().'/'.$folderName))
							$this->page->addAlert('Folder <span style="color:#000;">'.Input::post('current_name').'</span>  has been renamed into '.'<span style="color:#000;">'. $folderName.'</span>','success');
						else
							$this->page->addAlert('File <span style="color:#000;">'.Input::post('current_name').'</span>  has been renamed into '.'<span style="color:#000;">'. $folderName.strrchr(Input::post('current_name'),'.').'</span>','success');
					}else{
						if(is_dir($this->drive->getCurrentFolder()->getPath().'/'.Input::post('renameItem')))
							$this->page->addAlert('Folder <span style="color:#000;">'. $folderName.'</span> is already exist','notice');
						else
							$this->page->addAlert('Folder <span style="color:#000;">'.$folderName.strrchr(Input::post('current_name'),'.').'</span> is already exist','notice');		
					}
		
				}
			} else {
              if (Drive::isReservedName(Input::post('renameItem'))) {
                  $this->page->addAlert('Folder <span style="color:#000;">'.Input::post('renameItem').'</span> is a reserved name','notice');
              } else {
                  $this->page->addAlert('Folder <span style="color:#000;">'.Input::post('renameItem').'</span> Special characters are not allowed','notice');
              }
			}
		}
		
        // shared folders
        if (Input::post('sharedFolderMode') != null) {
          if (Input::post('folderID')) {
            $this->updateSharedFolder();
          } else {
            $this->createSharedFolder();
          }
        }
		
		// used by code editor	
        if ($this->request->isAjax() && Input::get('contents')) { // for ajax requests
                $this->page->setHeader(null);
                $this->page->setFooter(null);
       
                die($this->drive->toJSON());
        }
        
        $deleteFlag = $this->drive->getRoot()->getPath().'/.deleted';
        if (file_exists($deleteFlag)) {
          $deletes = explode("\n", file_get_contents($deleteFlag));
          foreach ($deletes as $d) {
            if (strlen($d) > 0) {
              $this->page->addAlert('An administrator has removed '.$d, 'warning');
            }
              
          }
          unlink($deleteFlag);
        }
        
		$pageData['contentString'] = $this->drive->getContentsFormatted(SITE_URL.trim($uri[0], '/'));
		$pageData['dir'] = urldecode($uri[0]);
		$pageData['status'] = $this->getUser()->getStatus();
		
		$this->page->addAsset('drive.css');
		$this->page->addAsset('select2.css');
		$this->page->addAsset('dropzone.css');
		$this->page->addAsset('jquery.contextMenu.css');
		$this->page->addAsset('jquery.modal.css');
		$this->page->addAsset('drive.menu.css');
		$this->page->addAsset('our.modal.css');
		$this->page->addAsset('our.modal.css');
			
		
		$this->page->addAsset('select2.min.js');
		$this->page->addAsset('jquery.modal.min.js');
		$this->page->addAsset('jquery.modal.js');
		$this->page->addAsset('custom_dialog.js');
		$this->page->addAsset('jquery.contextMenu.js');
		$this->page->addAsset('drive.functions.js');
		$this->page->addAsset('our.modal.css');
			
		$this->page->addAsset('drive.view.js');
		$this->page->addAsset('jquery.modal.min.js');
		$this->page->addAsset('jquery.modal.js');
		$this->page->addAsset('custom_dialog.js');
		$this->page->addAsset('jquery.contextMenu.js');
		$this->page->addAsset('drive.functions.js');
		$this->page->addAsset('drive.js');
		$this->page->addAsset('drive.view.js');
		$this->page->addAsset('sharedfolder.js');
		$this->page->addAsset('jquery.simple-dtpicker.js');
		$this->page->addAsset('jquery.simple-dtpicker.css');
		$this->page->addAsset('publish.js');
		$this->page->addAsset('item.move.js');
		
		if (!isset($uri[1]))
			$uri[1] = null;
		$pageData['breadcrumb'] = $this->drive->getBreadCrumb(urldecode(SITE_URL.'faculty/drive'), $uri[1]);
		
		$pageData['userType'] = $this->userType->getUserTypeID();
		$this->page->setContent('student/student_drive.php', $pageData);
		
		echo $this->page;
	
	}
	
    protected function createSharedFolder() {
      $sharedFolder = null;
      
      $mode = Input::post('sharedFolderMode');
      $path = ltrim(Input::post('sharedFolderPath'), '.');
      $userID = $this->getUser()->getUserId();
      $sharedFolder = null;
      switch ($mode) {
        case model\SharedFolder::ACCESS_MODE_READ:
          $password = null;
          if (Input::post('sharedFolderPassword')) {
            $password = Input::post('sharedFolderPassword');
          }
          $sharedFolder = model\SharedFolder::readOnlyFolderFactory($userID, $path, $password);
          break;
        case model\SharedFolder::ACCESS_MODE_WRITE:
          
          $deadline = DateTime::createFromFormat('Y-m-d H:i', Input::post('sharedFolderDeadline'));
          $password = Input::post('sharedFolderPassword');
          $prefix = Input::post('sharedFolderPrefix');
          $suffix = Input::post('sharedFolderSuffix');
          $filterType = Input::post('filterMode');
          $filters = array();
          if (is_array(Input::post('sharedFolderFilters'))) {
            $filters = Input::post('sharedFolderFilters');
          }
          
          $sharedFolder = model\SharedFolder::writableFolderFactory($userID, $path, $deadline, $password, $prefix, $suffix, $filterType, $filters);
          break;
        default:
          $this->page->addAlert('Invalid access mode', 'error');
          return;
      }
      $folder = new SharedFolder($sharedFolder);
      if (file_exists($folder->getPath().'/.shared')) {
        $file = new File($folder->getPath().'/.shared');
        $id = $file->getContents();
        $this->page->addAlert('Folder is already shared with ID:'.$id, 'warning');
        return;
      }
      
      $sharedFolderID = $sharedFolder->saveRecord();
      
      if ($sharedFolderID == 0) {
        $this->page->addAlert('Cannot share folder at this time.', 'error');
        return;
      }
      

      $fileName = $this->drive->getRoot()->getPath().$path.'/.shared';
      File::createFile($fileName, $sharedFolderID);
      $this->page->addAlert(Input::post('sharedFolderName').' is now shared.', 'success');
    }
    
    public function getSharedFolder($id) {
      $model = \model\SharedFolder::getById($id);
      if ($model != null) {
        $arr['id'] = $model->getSharedFolderID();
        $arr['path'] = $model->getFolderPath();
        if ($model->getDeadlineDate() != null) {       
          $arr['deadline'] = $model->getDeadlineDate()->format('Y-m-d\TH:i:s');
        }
        $arr['accessMode'] = $model->getAccessMode();
        $arr['filterType'] = $model->getFilterType();
        $arr['prefix'] = $model->getSubmissionPrefix();
        $arr['suffix'] = $model->getSubmissionSuffix();
        $arr['password'] = $model->getPassword();
        $arr['status'] = $model->getStatus();
        $arr['status'] = $model->getStatus();
        $filters = SharedFileFilter::getByFolderID($model->getSharedFolderID());
        if ($filters != null) {
          foreach ($filters as $filter) {
            $a['extension'] = $filter->getExtension();
            $a['id'] = $filter->getSharedFileFilterID();
            $arr['filters'][] = $a;
          }
        }

        
        
        echo json_encode($arr);  
      }
      
      die();
    }
    
    public function updateSharedFolder() {
      
      
      $model = \model\SharedFolder::getById(Input::post('folderID'));
      
      if ($model == null) {
        $this->page->addAlert('no Shared folder record');
        return;
      }
      $sharedFolder = new SharedFolder($model);
	  
      $model->setFolderPath(Input::post('sharedFolderPath'));
      $model->setAccessMode(Input::post('sharedFolderMode'));

      if (Input::post('sharedFolderDeadline') != '') {
        $deadline = DateTime::createFromFormat('Y-m-d H:i', Input::post('sharedFolderDeadline'));
        if (!$deadline){
          $deadline = DateTime::createFromFormat('Y-m-d\TH:i:s', Input::post('sharedFolderDeadline'));
        }
        $model->setDeadlineDate($deadline);
      }
      
      $model->setPassword(Input::post('sharedFolderPassword'));
      $model->setSubmissionPrefix(Input::post('sharedFolderPrefix'));
      $model->setSubmissionSuffix(Input::post('sharedFolderSuffix'));
      $model->setFilterType(Input::post('filterMode'));
      $filters = array();
      if (is_array(Input::post('sharedFolderFilters'))) {
        foreach (Input::post('sharedFolderFilters') as $key => $filter) {
          $filters[] = new SharedFileFilter($filter);
        }
      }
      $model->setFileFilters($filters);
      if ($model->updateRecord() == 0) {
        $folder = new SharedFolder($model);
        $folder->OrganizeSubmissions();
        $this->page->addAlert('Shared folder updated', 'success');
      }
    }
}