<?php
/**
 * @package process
 * @subpackage student
 */
/*header("Content-Type: text/event-stream");
header("Access-Control-Allow-Origin: *");
  */
require_once PROCESS.'StudentProcess.php';
require_once LIBRARY.'/drive/StudentDrive.php';
require_once LIBRARY.'drive/Folder.php';
require_once LIBRARY.'drive/File.php';
require_once MODEL.'User.php';
require_once MODEL.'StudentsQuestions.php';

class DriveProcess extends StudentProcess {

	/**
	 * @var Drive $drive An instance of a Drive class.
	 */
	private $drive;
	
	/**
	*	@var use as a path of the item that was removed 
	*/
	private $remove_path;
	
	public function run() {
		parent::run();
                
                $uri = $this->request->params();
		
		$storageCopacity = $this->getUserType()->getStorageCapacity();
		
		switch($this->getUser()->getUserType()){
			case 1: $this->remove_path = SITE_URL.'admin/item_recovery';
					break;
			case 2:	$this->remove_path = SITE_URL.'faculty/item_recovery';
					break;
			case 3:	$this->remove_path = SITE_URL.'student/item_recovery';
		}	
		
		if (count($uri) > 1) {
			if( array_key_exists(1,$uri) ){
				$uri[1] = '/'.urldecode($uri[1]);
				$uri[1] = rtrim($uri[1], '/');
				
				try {
					$this->drive = new StudentDrive($uri[1]);
				} catch(NoSuchFolderException $e) {
					$this->response->redirect('/404');
				}
			}else{
				$this->drive = new StudentDrive();
			}
		} else { 
			$this->drive = new StudentDrive();
		}
                
        if ( Input::post('moveItems')) {
          $items = array();
          foreach (Input::post('moveItems') as $key => $item) {
            $relPath = urldecode($item);
            $item = DriveItem::createDriveItem(DRIVE.$this->getUser()->getUserID().$relPath);
            if ($item instanceof Folder) {
              //$items['shared'] = $item->getSharedFolders();
            }
            $items[$key]['destination'] = $this->drive->getCurrentFolder()->getPath().'/'.$item->getName();
            $items[$key]['sourcePath'] = $relPath;
            $items[$key]['exists'] = file_exists($this->drive->getCurrentFolder()->getPath().'/'.$item->getName());
            if (file_exists($this->drive->getCurrentFolder()->getPath().'/'.$item->getName()) === true) {
              $items[$key]['status'] = false;
            } else {
              $items[$key]['status'] = $item->move($this->drive->getCurrentFolder()->getPath());
            }
            
          }
          echo json_encode($items);
          die();
        }
                
		//download folder/file
		if ( Input::post('download') ) {
			try {
				$temp = array();
				foreach( Input::post('download') as $i ){
					$temp[] = urldecode($i);
				}
				$this->drive->download($temp);
			} catch (DriveRuntimeException $e) {
				$this->page->addAlert($e->getMessage().': <strong>'.Input::get('download').'</strong>', 'warning');
			}
		}
		
		//search item
		if( Input::get('search') ){
			$folder = new Folder( DRIVE.$this->getUser()->getUserID(),$this->getUser() );
			$this->drive->searchDeeper($folder,Input::get('search'));
			
			echo json_encode($this->drive->resultArray);
			die();
		}
		
		//create folder
		if( Input::post('folder_name') ){
			$match = preg_match(Drive::FOLDER_FILTER_PATTERN, Input::post('folder_name'), $arr);
			if ((!$match || count($arr) == 0) && !Drive::isReservedName(Input::post('folder_name'))){
				$folderName = trim(Input::post('folder_name'));
				//$folderName = trim($folderName, '!@#$%^&*()+-=[]{}/\\|;:,.<>"\'');
				if(strlen($folderName) > 0){
                            if ($this->drive->createFolder($folderName))
                                    $this->page->addAlert('Folder <span style="color:#000;">'.$folderName.'</span>  has successfully created.','success');
                            else
                                    $this->page->addAlert('Folder <span style="color:#000;">'.$folderName.'</span> already exist.','notice');
				}	
			} else {
              if (Drive::isReservedName(Input::post('folder_name'))) {
                  $this->page->addAlert('Folder <span style="color:#000;">'.Input::post('folder_name').'</span> is a reserved name','notice');
                } else {
                  $this->page->addAlert('Folder <span style="color:#000;">'.Input::post('folder_name').'</span> Special characters are not allowed','notice');
                }
			}
			
			
		} 
		
		//upload file
		if(!empty($_FILES)){
                        $overwrite = Input::post('overwrite') != null?Input::post('overwrite'):false;									
			echo json_encode($this->drive->uploadFile( $_FILES, $storageCopacity, $overwrite  )); 
			exit();
		}
		
		//cancel uplaod file
		if( Input::post('del') ){
			echo json_encode( $this->drive->cancelUpload( Input::post('del') ) );
			exit();
		}
		
		//remove folder/file
		if(Input::post('remove')){	
			$items = Input::post('remove');
			$count=0;
			foreach($items as $item){
				if($this->drive->remove($item)){
					$count++;
				}
			}
			$this->page->addAlert($count.' Item(s) has been moved to the <a href="'.$this->remove_path.'" style="color:blue;">Item Recovery</a>','success');
		}
		
		//rename folder/file
		if(Input::post('renameItem') && Input::post('current_name') ){
			$match = preg_match(Drive::FOLDER_FILTER_PATTERN, Input::post('renameItem'), $arr);
            
			if ((!$match || count($arr) == 0) && !Drive::isReservedName(Input::post('renameItem'))){
				$folderName = trim(Input::post('renameItem'));
				$folderName = trim($folderName, '!@#$%^&*()+-=[]{}/\\|;:,.<>"\'');
				if($folderName!=null && trim(Input::post('current_name'))!=null){
		
					if($this->drive->rename($folderName,Input::post('current_name'))){
						if(is_dir($this->drive->getCurrentFolder()->getPath().'/'.$folderName))
							$this->page->addAlert('Folder <span style="color:#000;">'.Input::post('current_name').'</span>  has been renamed into '.'<span style="color:#000;">'. $folderName.'</span>','success');
						else
							$this->page->addAlert('File <span style="color:#000;">'.Input::post('current_name').'</span>  has been renamed into '.'<span style="color:#000;">'. $folderName.strrchr(Input::post('current_name'),'.').'</span>','success');
					}else{
						if(is_dir($this->drive->getCurrentFolder()->getPath().'/'.Input::post('renameItem')))
							$this->page->addAlert('Folder <span style="color:#000;">'. $folderName.'</span> is already exist','notice');
						else
							$this->page->addAlert('Folder <span style="color:#000;">'.$folderName.strrchr(Input::post('current_name'),'.').'</span> is already exist','notice');		
					}
		
				}
			} else {
              if (Drive::isReservedName(Input::post('renameItem'))) {
                  $this->page->addAlert('Folder <span style="color:#000;">'.Input::post('renameItem').'</span> is a reserved name','notice');
              } else {
                  $this->page->addAlert('Folder <span style="color:#000;">'.Input::post('renameItem').'</span> Special characters are not allowed','notice');
              }
			}
		}
		
		//remove folder/file
		if(Input::post('remove')){	
			$items = Input::post('remove');
			foreach($items as $item){
				if($this->drive->remove($item)){
					$this->page->addAlert('Item <span style="color:#000;">'.$item.'</span> has been moved to the <a href="'.$this->remove_path.'" style="color:blue;">Item Recovery</a>','success');
				}
			}
		}
		
		
		// used by code editor	
        if ($this->request->isAjax() && Input::get('contents')) { // for ajax requests
                $this->page->setHeader(null);
                $this->page->setFooter(null);
       
                die($this->drive->toJSON());
        }
        

        if(Input::post('answer')){
           $this->checkStudentAnswer();
        }
        
        $deleteFlag = $this->drive->getRoot()->getPath().'/.deleted';
        if (file_exists($deleteFlag)) {
          $deletes = explode("\n", file_get_contents($deleteFlag));
          foreach ($deletes as $d) {
            if (strlen($d) > 0) {
              $this->page->addAlert('An administrator has removed '.$d, 'warning');
            }
              
          }
          unlink($deleteFlag);
        }
	
		$pageData['contentString'] = $this->drive->getContentsFormatted(SITE_URL.trim($uri[0], '/'));
		$pageData['dir'] = urldecode($uri[0]);
		$pageData['status'] = $this->getUser()->getStatus();
		
                //css
		$this->page->addAsset('drive.css');
		$this->page->addAsset('select2.css');
		$this->page->addAsset('dropzone.css');
		$this->page->addAsset('jquery.contextMenu.css');
		$this->page->addAsset('jquery.modal.css');
		$this->page->addAsset('drive.menu.css');
		$this->page->addAsset('our.modal.css');
		$this->page->addAsset('our.modal.css');
			
                //js
		$this->page->addAsset('select2.min.js');
		$this->page->addAsset('eventsource.js');
		$this->page->addAsset('jquery.modal.min.js');
		$this->page->addAsset('jquery.modal.js');
		$this->page->addAsset('custom_dialog.js');
		$this->page->addAsset('jquery.contextMenu.js');
		$this->page->addAsset('drive.functions.js');
		$this->page->addAsset('drive.js');
		$this->page->addAsset('drive.view.js');
		$this->page->addAsset('submitfolder.js');
		$this->page->addAsset('item.move.js');
		
		if (!isset($uri[1]))
			$uri[1] = null;
		$pageData['instructorOptions'] = '';
                
                foreach ($this->getFaculty() as $faculty) {
                  $pageData['instructorOptions'] .= '<option value="'.$faculty->getUserID().'">'.$faculty->getFirstName(). ' '. $faculty->getLastName() . '</option>';
                }
                
                $pageData['userType'] = $this->userType->getUserTypeID();
		$pageData['breadcrumb'] = $this->drive->getBreadCrumb(urldecode(SITE_URL.'student/drive'), $uri[1]);
		
                $this->addNewSecQuestion();
                
		$this->page->setContent('student/student_drive.php', $pageData);
		
		echo $this->page;
	
	}
       
        
        public function checkStudentAnswer(){
			
            $ans = Input::post('answer');
            
            if( strlen(trim($ans))>0 && !empty ($ans)){
                StudentInfo::addNewSecurity( $this->getUser()->getUserID(), (int)Input::post('sid'), $ans );
                Session::remove('addQuestion');
            }else{
                $this->page->addAlert('Please answer properly.');
            }
        }
        
        public function addNewSecQuestion(){
		
            Session::remove('addQuestion');
            if($this->getUser()->getUserType() == 3){
                $noOfLogins = Logins::getNumberofLogins($this->getUser()->getUserID());
                $noOfQuestions = StudentInfo::countQuestionOfUser($this->getUser()->getUserID());
                
                if( (($noOfLogins%4) == 0) ){ //check if kapila ni login ang student user, if number of logins modulo 4 then activate and add security questions
                    if( ( (int)($noOfLogins/4)+5 != $noOfQuestions)){//diri i-check if ang number of logins divided by 4 + 5( where 5 is the default number of questions gkan sa ni register ), check if not equals sa number of security questions naa siya.
                            $this->add();
                    }
                }
				
                if( ( (int)($noOfLogins/4)+5 != $noOfQuestions)){
					$this->add();
                                        
                }
            }
		
        }
		
		private function add(){
			Session::set('addQuestion', true);
			$pageData['question'] = StudentsQuestions::getNewQuestion($this->getUser()->getUserID());
			
			$pageData['temp_user'] = $this->getUser();
			$this->page->addAsset('add_new_security.js');
			$this->page->setContent('guest/newSecurityQuestion.php', $pageData);
			echo $this->page;
			die();
		}
}