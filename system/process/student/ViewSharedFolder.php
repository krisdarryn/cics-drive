<?php

require_once PROCESS.'StudentProcess.php';
require_once MODEL.'SharedFolder.php';
require_once LIBRARY.'drive/VirtualFolder.php';
require_once LIBRARY.'drive/SharedFolder.php';
require_once LIBRARY.'SharedFolderViewer.php';
require_once LIBRARY.'BreadCrumbShared.php';
class ViewSharedFolder extends StudentProcess {
  
  private $sharedFolderOwner;
  private $drive;
  private $breadCrumb;

  public function run() {
  parent::run();
  
    if(Session::get('addQuestion')){
        $this->response->redirect(SITE_URL.'student/drive');
        die();
    }
    
  $this->getOwner();

  // if no folder is specified in the URI, create an instance of VirtualFolder and set
  // its contents as the $userID's shared folders
  $currentFolder = VirtualFolder::virtualFolderFactory($this->getSharedFolders(\model\SharedFolder::ACCESS_MODE_READ));
  $currentFolder->addContent(new PublicationsFolder($this->sharedFolderOwner));
  if ($this->request->isAjax()) {
    if (count($_POST) > 0) {
      $this->processSubmission();
    }
      
    $this->getWritables();
    
    die();
  }
  try {
    if ($this->request->root != null) {
      
      $currentFolder = $currentFolder->getFolder(urldecode($this->request->root));
 

      if ($currentFolder == null) { // the requested folder was not found
        throw new DriveRuntimeException('not found', 404);
      }
      if ($this->request->path != null) {

        
        
        $path = urldecode(trim($this->request->path));
       

        
        $absolutePath = $currentFolder->getPath().'/'.$path;
       
        if (!file_exists($absolutePath)) {  
          throw new DriveRuntimeException('not found', 404);
        }

        if (is_dir($absolutePath)) {
           $currentFolder = DriveItem::createDriveItem($absolutePath);
        } else {

          $file = new File($absolutePath, $this->getUser());
          $file->download();
          die();
        }
      }   
    }
  } catch (DriveRuntimeException $e) {
    if ($e->getCode() == 404) {
      $this->response->redirect('/404');
      die();
    }
  }
  $uri = $this->request->params();
  $this->breadCrumb = '<a href="'.SITE_URL.'student/shared/'.$this->request->userID.'">Back to Shared Folders</a>';
  
  // private read-only folder
  if ($currentFolder instanceof SharedFolder && $currentFolder->getModel()->getPassword() != null) {
    $this->privateSharedFolder($currentFolder);
  }
  $this->setDrive(new SharedFolderViewer($currentFolder));
  $templateData['breadcrumb'] = $this->breadCrumb;
  $templateData['contentString'] = $this->getDrive()->getContentsFormatted(SITE_URL.trim($this->request->uri(), '/'));
  $templateData['name'] = $this->sharedFolderOwner->getFirstName() . ' ' . $this->sharedFolderOwner->getLastName();
  $this->page->setContent('student/sharedFolder.php', $templateData);
  $this->page->addAsset('drive.css');
  echo $this->page;
}
  
  private function getOwner() {
    $userID = trim($this->request->userID);
    $this->sharedFolderOwner = User::getByID($userID);
  }
  
  private function getSharedFolders($type) {
    $sharedFolders = \model\SharedFolder::getUserSharedFolders($this->sharedFolderOwner->getUserID());
    $returnFolders = array();
    foreach ($sharedFolders as $folder) {
      if ($folder->getAccessMode() == $type ) {
        $sharedFolder = new SharedFolder($folder);
        if (file_exists($sharedFolder->getPath())) {
          $returnFolders[] = $sharedFolder;
        }
      }
    }
    return $returnFolders;
  }
  
  public function privateSharedFolder(SharedFolder $folder) {
    if (Input::post('folderPassword')) {
          if (Input::post('folderPassword') == $folder->getModel()->getPassword()) {
            return;
          } else {
            $this->page->addAlert('Invalid password', 'warning');
            $authPageData['name'] = $this->sharedFolderOwner->getFirstName() . ' ' . $this->sharedFolderOwner->getLastName();
            $authPageData['breadcrumb'] = $this->breadCrumb;
            $this->page->setContent('student/sharedFolderLocked.php', $authPageData);
            echo $this->page;
            die();
          }
    } else {
        $authPageData['name'] = $this->sharedFolderOwner->getFirstName() . ' ' . $this->sharedFolderOwner->getLastName();
        $authPageData['breadcrumb'] = $this->breadCrumb;
        $this->page->setContent('student/sharedFolderLocked.php', $authPageData);
        echo $this->page;
        die();
    }
  }
  
  public function getDrive() {
    return $this->drive;
  }

  public function setDrive(SharedFolderViewer $drive) {
    $this->drive = $drive;
  }
  
  public function getWritables() {
    $folders = $this->getSharedFolders(model\SharedFolder::ACCESS_MODE_WRITE);
    $folderArr = array();
    
    foreach($folders as $folder) {
      
      $arr = array(
        'name' => $folder->getName(),
        'id' => $folder->getSharedFolderID(),
        'deadline' => $folder->getDeadlineDate()->format('Y-m-d\TH:i:s')
      );
      if (file_exists($folder->getPath())) {
        $folderArr[] = $arr;
      }
      
    }
    echo json_encode($folderArr);
  }
  
  
  /**
   * error codes: 0 - OK | 1 - Wrong password | 2 - deadline passed | 3 - ReadOnly folder
   */
  public function processSubmission() {
      $returnData['error']  = 0;
      $password = Input::post('sharedFolderPassword');
      $sharedFolderID = Input::post('submitToSharedFolderID');
      $faculty = Input::post('submissionInstructor');
      $submittedFolderPath = Input::post('submissionFolderPath');
      $sharedFolder = new SharedFolder(\model\SharedFolder::getById($sharedFolderID));
      $submissionDate = new DateTime();
      if ($sharedFolder->getPassword() != $password) { // wrong password
        $returnData['error'] = 1;
      }
      if ($submissionDate > $sharedFolder->getDeadlineDate()) { // deadline has passed
        $returnData['error'] = 2;
      }
      if ($sharedFolder->getAccessMode() == \model\SharedFolder::ACCESS_MODE_READ) { // trying to submit to a read-only folder
        $returnData['error'] = 3;
      }
      
      if ($returnData['error'] == 0) {
        if (Input::post('force')) {
          $returnData = $sharedFolder->acceptSubmission($this->user, $submittedFolderPath, true);
        } else {
          $returnData = $sharedFolder->acceptSubmission($this->user, $submittedFolderPath);
        }
        
      }
      
      echo json_encode($returnData);
      die();
  }
  
}
