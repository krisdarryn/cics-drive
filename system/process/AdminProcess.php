<?php
/**
 * @package process
 */	
 
require_once MODEL.'User.php';

class AdminProcess extends Process {
	
	public function __construct(_Request $request = null , _Response $response = null) {

		parent::__construct($request, $response);
                
                if(!file_exists(BACKUP)){
                    mkdir(BACKUP, 0777, true);
                }
	}

	protected function loadDefaultTemplate() {
		
		try { 	// get the user data
			$user = Session::get('user');
			$this->setUser(User::getById($user['userId']));
			$headerData['user'] = $this->getUser();	
		} catch(UserException $e) {
			die('Error: User not found');
		}
			
		
		$headerData['notifications'] = $this->getNotifications();	
		
		$this->addDefaultAssets();
		$this->page->setTitle('CICS Drive');
		$this->page->setHeader('admin/header.php', $headerData);// default header and footer for student process
		$this->page->setFooter('admin/footer.php');
	}

	public function run() {
		$this->allowedUserType = UserType::ADMIN;
		$this->authenticate();
		$this->loadDefaultTemplate();
	}
      
        
        public function getBackupLog(){
            $path = BACKUP.Session::get('backup_path').'/LOGS/'; 
          
            if(!file_exists($path)){
                mkdir($path, 0777, true);
            }
            return $path;
        }
        
        public function creatBackupSubFolders(){
            if(!file_exists(BACKUP.Session::get('backup_path').'/TABLES/')){
                mkdir(BACKUP.Session::get('backup_path').'/TABLES/', 0777, true);
            }
            if(!file_exists(BACKUP.Session::get('backup_path').'/AVATARS/')){
                mkdir(BACKUP.Session::get('backup_path').'/AVATARS/', 0777, true);
            }
            if(!file_exists(BACKUP.Session::get('backup_path').'/DRIVES/')){
                mkdir(BACKUP.Session::get('backup_path').'/DRIVES/', 0777, true);
            }
        }
        
	private function addDefaultAssets() {
	}
        
	public function getNotifications() {
		$str = '
				<li class="unread">
					<hgroup>
						<h1>An admin has removed a file from your drive.</h1>
						<h2>File: My Drive/Test/kamandag.c</h2> 
					</hgroup>
					<p><span>14:24</span></p>
				</li>
		';
		return $str;
	}
}