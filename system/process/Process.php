<?php
/**
 * @package process
 */
require_once LIBRARY.'Input.php';
require_once LIBRARY.'Session.php';
require_once MODEL.'User.php';
require_once MODEL.'UserType.php';
require_once MODEL.'UserActivityLogs.php';
require_once MODEL.'UserActivityProperties.php';
require_once MODEL.'FileActivityLogs.php';
require_once MODEL.'FileActivityProperties.php';

abstract class Process {

	protected $user;
	protected $userType;
	protected $page;
	protected $request;
	protected $response;
	protected $allowedUserType = UserType::GUEST; 
	
	public function __construct(_Request $request = null, _Response $response = null) {
		$this->request = $request;
		$this->response = $response;
		
		Session::start();
		if (!Session::isLoggedIn()) {
			Session::set('user', array('userType' => 0));
		}
		$this->page = new Page();
                

		// load the default assets
		//css
		$this->page->addAsset('style.css');
		$this->page->addAsset('editor.css');
		//js
		$this->page->addAsset('jquery-1.9.js');
		$this->page->addAsset('modernizr.custom.64105.js');

		$this->page->addAsset('jquery.modal.js');
		$this->page->addAsset('custom.js');
		$flashMessage = Session::getFlash('alert');	

		if ($flashMessage != null) {
			$alert = $flashMessage[0];
			if (isset($alert['type']))
				$this->page->addAlert($alert['message'], $alert['type']);
			else 
				$this->page->addAlert($alert['message']);
		}
		// puno ug try catch inig human sa tanan para dili ma kita ang error messages.
		$this->run();
	}
	
	public function getPage() { return $this->page; } 
	public function getRequest() { return $this->request; } 
	public function getResponse() { return $this->response; } 
	public function setPage($x) { $this->page = $x; } 

	abstract public function run();

	protected function authenticate() {
		if (!Session::isLoggedIn()) {
			$this->response->redirect('/login');
		}
		$user = Session::getUser();
		
		$this->setUser(User::getByID($user['userId']));
		$this->setUserType( UserType::userTypeInstance($user['userType']) );
			
		if ($user['userType'] != $this->allowedUserType)
			Session::logout();


	}
	
	public function setUserType(UserType $userType){
		$this->userType = $userType;
	}
	
	public function setUser(User $user) {
		$this->user = $user;
	}

	public function getUser() {
            return $this->user;
	}
	
	public function getUserType(){
            return $this->userType;
	}
        
        /**
         *  @method createArchiveFolder - This method will attemp to create a directory DRIVE.;ARCHIVE; 
         */
        public function createArchiveFolder(){
            if( !file_exists(ARCHIVE) ){
                mkdir(ARCHIVE, 0777, true);
            }
        }
        
        /**
         *  @method createAdminBackupFolder - This method will attemp to create a directory DRIVE.;BACKUP;
         */
        public function createAdminBackupFolder(){
            if( !file_exists(BACKUP) ){
                mkdir(BACKUP, 0777, true);
            }
        }
        
        /**
         *  @metho createAdminRecycleFolder - This method will attempt to create a directory DRICE.;ADMINRECYCLE;
         */
        public function createAdminRecycleFolder(){
            if( !file_exists(ADMINRECYCLE) ){
                mkdir(ADMINRECYCLE, 0777, true);
            }
        }
        
        /**
         *  @method setUserActivityLog
         */
        public function setUserActivityLog($category=null,$userID=null){
            return UserActivityLogs::logAcivity($category, $userID);
        }
        
        /**
         * @method setUserActivityProperties
         */
        public function setUserActivityProperties($actid=null, $id=null, $value=null){
            UserActivityProperties::logProperties($actid, $id, $value);
        }
	
}