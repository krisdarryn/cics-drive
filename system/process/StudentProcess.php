<?php
/**
 * @package process
 */	
 
require_once MODEL.'User.php';
require_once MODEL.'UserType.php';
require_once 'student/DriveProcess.php';

class StudentProcess extends Process {
	
	public function __construct(_Request $request = null , _Response $response = null) {

		parent::__construct($request, $response);
		
	}

	protected function loadDefaultTemplate() {
		
		try { 	
			$headerData['user'] = $this->getUser();	
		} catch(UserException $e) {
			die('Error: User not found');
		}
			
        $this->page->addAsset('select2.min.js');
        $this->page->addAsset('select2.css');
        $this->page->addAsset('facultySearch.js');
        
		$headerData['storageindicator'] = '<div class="storageIndicator">'.
											$this->getUser()->getUsedStoragePercent().'%'.
											'<div class="innerIndicator" style="width:'.round($this->getUser()->getUsedStoragePercent()).'%;">'.
											'</div>'.
										  '</div>';
										  
		$headerData['notifications'] = $this->getNotifications();	
        $headerData['facultyOptions'] = '';
        foreach ($this->getFaculty() as $faculty) {
          $headerData['facultyOptions'] .= '<option value="'.$faculty->getUserID().'">'.$faculty->getFirstName(). ' '. $faculty->getLastName() . '</option>';
        }
        
		$this->page->setTitle('CICS Drive');
		$this->page->setHeader('student/header.php', $headerData);// default header and footer for student process
		$this->page->setFooter('student/footer.php');
	}

	public function run() {
		$this->allowedUserType = UserType::STUDENT;
		$this->authenticate();
		$this->loadDefaultTemplate();
                if($this->getUser()->getStatus() == 'K'){
			$this->page->addAlert('Your account is marked for suspicious activity. Please approach the lab supervisors ASAP.', 'warning');
		}
	}
   
	public function getNotifications() {
		$str = '
				<li class="">
					<hgroup>
						<h1>An admin has removed a file from your drive.</h1>
						<h2>File: My Drive/Test/kamandag.c</h2> 
					</hgroup>
					<p><span>14:24</span></p>
				</li>
		';
		return $str;
	}


	protected function authenticate() {
		parent::authenticate();

		$user = Session::get('user');
		if (isset($user['validate'])) 
			$this->response->redirect(SITE_URL.'question');
	}
    
    public function getFaculty(){
      return User::getUserList(UserFlags::FACULTY | UserFlags::ACTIVE | UserFlags::ASC, 1000);
    }
}
