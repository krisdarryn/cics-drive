<?php
require_once LIBRARY.'CSV.php';
require_once MODEL.'User.php'; 
require_once PROCESS.'AdminProcess.php';
/**
 * @package process
 * @subpackage student
 */
class ImportAccount extends AdminProcess {

	public function run() {
		parent::run();
		if (Input::hasPost()){
			$this->checkCSV();
			//$this->importUserAccount();
		}
		
		$pageData['user'] = $this->getUser();
		$this->page->setTitle('Import Account - CICS Drive');
		$this->page->setContent('admin/import_account.php', $pageData);
		echo $this->page;
	}
	
	private function checkCSV(){
	
		if(Input::hasFile()){
			$csv = Input::file('csv');
			$csvflag = 3;
			$csvcheck = strlen($csv['name']) - $csvflag;
			$flag = strtolower(substr($csv['name'],$csvcheck));
			if($flag == 'csv'){
				 $csvs = Array();
				 $rowcount = 0;
				if (($handle = fopen($csv['tmp_name'], "r")) !== FALSE) { // open csv
					$max_line_length = defined('MAX_LINE_LENGTH') ? MAX_LINE_LENGTH : 10000; // ??
					$header = fgetcsv($handle, $max_line_length);   // 
					$header_colcount = count($header);
					while (($row = fgetcsv($handle, $max_line_length)) !== FALSE) { 
						$row_colcount = count($row);
						if ($row_colcount == $header_colcount) { 
							$csvs[] = $row;
						}else {
							$this->page->addAlert("csvreader: Invalid number of columns at line " . ($rowcount + 2) . " (row " . ($rowcount + 1) . "). Expected=$header_colcount Got=$row_colcount");
							return null;
						}
						$rowcount++;
					}fclose($handle);
				}else{
					$this->page->addAlert('csvreader: Could not read CSV \"$csvfile\"','error');
					return null;
				}
				if($row_colcount != 3){
					$this->page->addAlert('Number of rows format is invalid','error');
				}else{
					$this->importUserAccount($csvs);
				}
			}else{
				$this->page->addAlert('Invalid file not a CSV format.', 'error');
			}
		}
	}
	
	private function importUserAccount($csvs){
		$csv = $csvs;
		$c = Input::file('csv');
		$csvname = $c['name'];
		$ctr = 0;
		$ctrs = 0;
		 foreach($csv as $key => $csvalue){
			$f = $csvalue;
			if(strlen($csvalue[0]) == 8){
				$flag = $csvalue[0];
			}else{
				$flag = substr($csvalue[0],1);
			}
			if(User::getUserValidation($flag)!=null){
				$ctr = 1 + $ctr;
			}else{
				$ctrs = 1 + $ctrs;
				$fname = ucwords(strtolower($f[2]));
				$lname = ucwords(strtolower($f[1]));
				User::importCSV($flag,$lname,$fname);
			}
		}
		if($ctrs!=0){
			$key = $key + 1;
			$this->page->addAlert('Success importing "'.$csvname.'" out of "'.$key.'" there are only "'.$ctrs.'" import in the database while "'.$ctr.'" already exist', 'success');
                        UserActivityLogs::logAcivity(UserActivityLogs::CATEGORY_ACCOUNT_IMPORT, $this->getUser()->getUserID());
		}else{
			$this->page->addAlert('All of the data inside the "'.$csvname.'" file aready exist in the database.', 'error');
		}
	}
}