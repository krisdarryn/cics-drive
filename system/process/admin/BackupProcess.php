<?php
require_once PROCESS.'AdminProcess.php';
require_once LIBRARY.'Input.php';
require_once LIBRARY.'Backup.php';
require_once MODEL.'FileActivityLogs.php';
require_once MODEL.'FileActivityProperties.php';
/**
 * @package process
 * @subpackage student
 */
class BackupProcess extends AdminProcess{
        
	public function run() {
		parent::run();
                
                $this->createAdminBackupFolder();
                
                $backup  = new Backup();
                
//                $sqlCom = 'mysql --user='.DB_USERNAME.' --password="'.DB_PASSWORD.'" '.DB_NAME.' < '.ROOT.'cicsdrive2.sql';
//                debug_print_r($sqlCom);
//                exec($sqlCom);
                
                if(Input::post('initTable')){
                    $tables = $backup->getListTable();
                    
                    die(json_encode( $tables ) );
                }
                
                if(Input::post('initAvatar')){
                    $avatarItem = array();

                    $avatar = new Folder(AVATAR);
                    
                    foreach($avatar->getContents() as $key => $content ){
                       if($content instanceof File){
                           $avatarItem[] = $content->getName();
                       }
                    }
                    die(json_encode($avatarItem) );
                }
                
                if(Input::post('initDrive')){
                    $driveItem = array();
                    
                    $drive = new Folder(DRIVE);
                    $ctr=0;
                    
                    foreach($drive->getContents() as $key => $content){
                           if($content instanceof Folder){
                               if($content->getName() != ';BACKUP;'){
                                   $driveItem[$ctr]['driveItem'] = $content->getName();
                                   $driveItem[$ctr++]['countContents'] = Folder::countInnerContents($content->getPath())>0?Folder::countInnerContents($content->getPath()):1;
                               }
                           }
                           if($content instanceof File){
                              if($content->getName() != 'dummy'){
                                $driveItem[$ctr]['druveItem'] = $content->getName();
                                $driveItem[$ctr++]['countContents'] = 1;
                              }
                           }
                    }
                    
                    die(json_encode( $driveItem ));
                }
                
                if(Input::post('log')){
                    Session::set('backup_path',date('F_d_Y_h_i_s_a'));
                    
                    $table_log = $this->getBackupLog().'tables.txt';
                    $avatar_log = $this->getBackupLog().'avatars.txt';
                    $drive_log = $this->getBackupLog().'drives.txt';
                    
                    //tables
                    $file = fopen($table_log,'a');
                    foreach(Input::post('tables') as $key => $item){
                        
                        fwrite($file, $item);
                        if( $key!=count(Input::post('tables')) ){
                            fwrite($file, "\n");
                        }
                    }
                    fclose($file);
                    $drive_log = $this->getBackupLog().'drives.txt';

                    //avatars
                    $file = fopen($avatar_log,'a');
                    foreach(Input::post('avatars') as $key => $item){
                        
                        fwrite($file, $item);
                        if( $key!=count(Input::post('avatars')) ){
                            fwrite($file, "\n");
                        }
                    }
                    fclose($file);
                    
                    //tables
                    $file = fopen($drive_log,'a');
                    foreach(Input::post('drives') as $key => $item){
                        
                        fwrite($file, $item);
                        if( $key!=count(Input::post('drives')) ){
                            fwrite($file, "\n");
                        }
                    }
                    fclose($file);
                    
                    $this->creatBackupSubFolders();
                    die( json_encode(true) );
                }
                
                if(Input::post('backupTable')){
                    $res = $backup->exportDBTable(Input::post('backupTable'),BACKUP.Session::get('backup_path').'/LOGS/tables.txt',BACKUP.Session::get('backup_path').'/TABLES/',Session::get('backup_path'));
                    if($res){
                        rename(ROOT.'cicsdrive2.sql', BACKUP.Session::get('backup_path').'/TABLES/cicsdrive2.sql');
                    }
                    die( json_encode($res) );
                }
                
                if(Input::post('backupAvatar')){
                    $res = $backup->backupAvatar(Input::post('backupAvatar'),BACKUP.Session::get('backup_path').'/LOGS/avatars.txt',BACKUP.Session::get('backup_path').'/AVATARS/');
                
                    die( json_encode(array($res,1)) );
                }
                
                if(Input::post('backupDrive')){
                    $res = $backup->backupDrive(Input::post('backupDrive'),BACKUP.Session::get('backup_path').'/LOGS/drives.txt',BACKUP.Session::get('backup_path').'/DRIVES/');
                    $ctr = Folder::countInnerContents(DRIVE.Input::post('backupDrive'))>0?Folder::countInnerContents(DRIVE.Input::post('backupDrive')):1;
                    
                    $actId = FileActivityLogs::logFileAcivity( FileActivityLogs::CATEGORY_BACKUP,$this->getUser()->getUserID() );
                    FileActivityProperties::logFileProperties( $actId, FileActivityProperties::INDICATE_PATH, Session::get('backup_path'));
                    die( json_encode(array($res,$ctr)) );
                }
                
                if(Input::post('download')){
                    $download_backup =  new Folder(BACKUP.Input::post('download'));
                    $download_backup->download(true);
                }
                
                if(Input::post('init_restore')){
                    $path =  BACKUP.Input::post('restore').'/';
                    $tables = new Folder($path.'TABLES/');
                    $avatars = new Folder($path.'AVATARS/');
                    $drives = new Folder($path.'DRIVES/');
                    
                    $table_list=array();
                    $avatar_list=array();
                    $drive_list=array();
                    
                    foreach($tables->getContents() as $key => $content){
                        $table_list[$key]['name'] = $content->getName();
                        $table_list[$key]['count'] = 1;
                    }
                    
                    foreach($avatars->getContents() as $key => $content){
                        $avatar_list[$key]['name'] = $content->getName();
                        $avatar_list[$key]['count'] = 1;
                    }
                    
                    foreach($drives->getContents() as $key => $content){
                        $drive_list[$key]['name'] = $content->getName();
                        $drive_list[$key]['count'] = Folder::countInnerContents($content->getPath())>0?Folder::countInnerContents($content->getPath()):1;
                    }
                   
                    die( json_encode( array($table_list,$avatar_list, $drive_list) ) );
                }
                
                if(Input::post('start_restore')!=null){
                    $path =  BACKUP.Input::post('restore').'/';
                    $dbName = $path.'TABLES/cicsdrive2.sql';
                    copy($dbName, ROOT.'cicsdrive2.sql');
                    
                    $avatars = new Folder($path.'AVATARS/');
                    $drives = new Folder($path.'DRIVES/');
                    
                    $backup->restoreDB(ROOT.'cicsdrive2.sql');
                    $backup->restoreAvatar($avatars);
                    $backup->restoreDrive($drives);
                    
                    die( json_encode(true) );
                }
                
                if(Input::post('delete')){
                    $delete_backup =  new Folder(BACKUP.Input::post('delete'));
                    $delete_backup->delete(null,true);
                }
                
                
                $this->page->addAsset('drive.menu.css');
                $this->page->addAsset('jquery.modal.css');
                $this->page->addAsset('our.modal.css');
                $this->page->addAsset('backup.css');
                
                $this->page->addAsset('jquery.modal.js');
                $this->page->addAsset('custom_dialog.js');
                $this->page->addAsset('backup.js');
                
                $pageData['backupRecord'] = array_reverse($this->getBackupRecord());
		$this->page->setTitle('System Backup - CICS Drive');
		$this->page->setContent('admin/backup.php',$pageData);
		echo $this->page;		
	}
        
        public function getBackupRecord(){
            $backupList = new Folder(BACKUP);
            
            return $backupList->getContents();
        }
}
	