<?php
/**
 * @package process
 * @subpackage student
 */

require_once PROCESS.'AdminProcess.php';
require_once LIBRARY.'/drive/StudentDrive.php';
require_once LIBRARY.'drive/Folder.php';

class AccountDrive extends AdminProcess {

	/**
	 * @var Drive $drive An instance of a Drive class.
	 */
	private $drive;
	
	public function run() {
		parent::run();
		
		$uri = $this->request->params();

		if (count($uri) > 1) {
			if( array_key_exists(1,$uri) ){
				$uri[1] = '/'.urldecode($uri[1]);
				$uri[1] = rtrim($uri[1], '/');
				
				try {
					$this->drive = new StudentDrive($uri[1]);
				} catch(NoSuchFolderException $e) {
					$this->response->redirect('/404');
				}
			}else{
				$this->drive = new StudentDrive();
			}
		} else { 
			$this->drive = new StudentDrive();
		 } 
		
		//create folder
		if( Input::post('folder_name') ){
			if(trim(Input::post('folder_name')!=null)){
				if(ctype_alnum(Input::post('folder_name'))){
					$value = $this->drive->createFolder(Input::post('folder_name'));
					if($value){
						$this->page->addAlert('Folder <span style="color:#000;">'.$uri['folder_name'].'</span>  has successfully created.','success');
					}else{
						$this->page->addAlert('Folder <span style="color:#000;">'.$uri['folder_name'].'</span> already exist.','notice');
					}
				}else{
					$this->page->addAlert('Folder <span style="color:#000;">'.$uri['folder_name'].'</span> contain special character.','notice');
				}
			}
		} 
		
		//upload file
		if(!empty($_FILES)){
			echo $str = $this->drive->upload($_FILES); 	
			exit();
		}
		
		//rename folder/file
		if(Input::post('renameItem') && Input::post('current_name')){
			if(trim(Input::post('renameItem')!=null) && trim(Input::post('current_name'))!=null){
				if(ctype_alnum(Input::post('renameItem'))){	
					$value = $this->drive->rename(Input::post('renameItem'),Input::post('current_name'));
					if($value){	
						if(is_dir($this->drive->getCurrentFolder()->getPath().'/'.Input::post('renameItem'))){
							$this->page->addAlert('<span style="color:#000;">'.Input::post('current_name').'</span>  has been renamed into '.'<span style="color:#000;">'.Input::post('renameItem').'</span>','success');
						}else{
							$this->page->addAlert('<span style="color:#000;">'.Input::post('current_name').'</span>  has been renamed into '.'<span style="color:#000;">'.Input::post('renameItem').strrchr(Input::post('current_name'),'.').'</span>','success');
						}
					}else{
						$this->page->addAlert('Failed to rename','notice');
					}
				}else{
					$this->page->addAlert('Cant rename into <span style="color:#000;">'.Input::post('renameItem').'</span> contain special character.','notice');
				}
			}
		}
		
		//delete folder/file
		if( $this->request->isAjax() && Input::post('name') && Input::post('isDelete') ){
			$val = $this->drive->delete(Input::post('name'),Input::post('isDelete'));
			if(Input::post('isDelete')){
				if(is_array($val)){
					die('folder');
				}
				die('file');
			}else{
				
			}
		} 
		
		// used by code editor	
        if ($this->request->isAjax() && Input::get('contents')) { // for ajax requests
                $this->page->setHeader(null);
                $this->page->setFooter(null);
       
                die($this->drive->toJSON());
        }
		$pageData['contentString'] = $this->drive->getContentsFormatted(SITE_URL.trim($this->request->uri(), '/'));
		$pageData['dir'] = urldecode($uri[0]);
		$this->page->addAsset('drive.css');
		$this->page->addAsset('dropzone.css');
		$this->page->addAsset('jquery.contextMenu.css');
		$this->page->addAsset('ui.css');
		$this->page->addAsset('jquery.modal.css');
		$this->page->addAsset('drive.menu.css');
		$this->page->addAsset('dropzone.css');
		$this->page->addAsset('drive.js');
		$this->page->addAsset('jquery.contextMenu.js');
		$this->page->addAsset('jquery.modal.js');
		$this->page->addAsset('jquery.modal.min.js');
		if (!isset($uri[1]))
			$uri[1] = null;
		$pageData['breadcrumb'] = $this->drive->getBreadCrumb(urldecode(SITE_URL.'student/drive'), $uri[1]);

		$this->page->setContent('student/student_drive.php', $pageData);
		
		echo $this->page;
	
	}
	
}