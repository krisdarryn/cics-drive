<?php

require_once MODEL.'User.php';
require_once PROCESS.'AdminProcess.php';
require_once LIBRARY.'Input.php';
/**
 * @package process
 * @subpackage student
 */
class ResetUserPassword extends AdminProcess {
	
	public function run() {
		parent::run();
		
		if ( Input::hasPost() ){
			
			$this->resetPassword();
		}
		$pageData['users'] = User::userList();
		$this->page->setTitle('Reset User Password - CICS Drive');

		$this->page->setContent('admin/reset_user_password.php',$pageData);
		echo $this->page;
	}
	
	private function resetPassword(){
		$newUser = new User();
		if(Input::post('reset')){
			$password = strtolower(Input::post('password'));
			$a = $newUser->resetUserPassword(Input::post('reset'),$password);
			
			 if ($a){
				$this->page->addAlert('Account password was successfuly reset to "'.$password.'"', 'success');
			}else{
				$this->page->addAlert('Error during update.', 'error');
			}
		}
		

	}
}