<?php

require_once MODEL.'User.php';
require_once MODEL.'StudentsQuestions.php';
require_once PROCESS.'AdminProcess.php';
require_once LIBRARY.'Input.php';
/**
 * @package process
 * @subpackage student
 */
class SecurityQuestion extends AdminProcess{
	
	public function run() {
		
		parent::run();
		
		if(Input::post('action')!=null){
                        
                        if (StudentsQuestions::updateQuestionStatus(Input::post('action'),Input::post('question'))) {
                          $logId = 0;
                          switch (Input::post('action')) {
                            case 'A':
                                $logId = UserActivityLogs::logAcivity(UserActivityLogs::CATEGORY_SECURITY_QUESTION_ENABLED, $this->getUser()->getUserID());
                              break;
                            case 'I':
                                $logId = UserActivityLogs::logAcivity(UserActivityLogs::CATEGORY_SECURITY_QUESTION_DISABLED, $this->getUser()->getUserID());
                              break;
                          }
                          
                          if ($logId > 0) {
                            echo UserActivityProperties::logProperties($logId, UserActivityProperties::SECURITY_QUESTION_ID, Input::post('id'));
                            
                          }

                        }
			die();
		}
		
		if ( Input::post('question')!= null  ){
			$this->addQuestion();
		}
		if(Input::post('change')!=null){
			$this->changeStatus();
		}
		
		$this->page->addAsset('update.security.question.js');
		$pageData['studentquestions'] = StudentsQuestions::securityQuestion();
		$this->page->setTitle('Security Question - CICS Drive');
		$this->page->setContent('admin/security_question.php',$pageData);
		echo $this->page;		
	}
	private function addQuestion(){
	
		$question = trim(Input::post('question'));
	
		if( StudentsQuestions::existingSecurityQuestion(Input::post('question')) != null){	
			$this->page->addAlert('Question already exist');
		} else {			
                    if(!empty($question)){
                            $qid = StudentsQuestions::addSecurityQuestion($question);
                            $this->page->addAlert('New question is added','success');
                            $logID = UserActivityLogs::logAcivity(UserActivityLogs::CATEGORY_SECURITY_QUESTION_ADD, $this->getUser()->getUserID());
                            UserActivityProperties::logProperties($logID, UserActivityProperties::SECURITY_QUESTION_ID, $qid);
                    }else{
                            $this->page->addAlert('Invalid input, enter a correct data');
                    }
		}
	}
	
	private function changeStatus(){
		$info = StudentsQuestions::validateSecurityQuestion(Input::post('change'));
		if($info['status']!='A'){
			StudentsQuestions::updateQuestionStatus('A',Input::post('change'));
			$message = 'Security Question is now Activated';
			$this->page->addAlert($message,'success');
                        
		}else{
			StudentsQuestions::updateQuestionStatus('I',Input::post('change'));
			$message = 'Security Question is now Deactivated';
			$this->page->addAlert($message,'success');
		}
		
	}
}
	