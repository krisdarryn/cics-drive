<?php

require_once MODEL.'User.php';
require_once MODEL.'FileActivityLogs.php';
require_once MODEL.'Util.php';
require_once PROCESS.'AdminProcess.php';
require_once LIBRARY.'Paginator.php';
/**
 * @package process
 * @subpackage student
 */
class FileActivity extends AdminProcess {
    
	/**
	*
	*/	
        public $logs;
	
	/**
	*
	*/
	public $reports;
	
        
        /**
         * 
         */
        public $usertype;
        
	public function run() {
            parent::run();
            
            if( Session::get('file_user_type')!=null ){
                $usertype=Session::get('file_user_type');
            }else{
                $usertype=array(1,2,3);
            }
            
            if(Session::get('file_category')!=null){
                $category=Session::get('file_category');
            }else{
                $category='';
            }
            
            if(Session::get('file_date')!=null){
                $d = Session::get('file_date');
                $date = array();
                $date[] =  $d[0];
                $date[] =  $d[1];
            }else{
                $date = array();
            }
            
            $this->reports = array();
			
            if (Input::post('userType') != null && is_array(Input::post('userType'))) {
                Session::set('file_user_type',Input::post('userType'));
                $usertype = Input::post('userType');
            }
            
            if(Input::post('category')!=null){
                Session::set('file_category',Input::post('category'));
                if(Input::post('category') == 99999){
                    $category = '';
                }else{
                    $category = Session::get('file_category');
                }   
            }
            
            if(Input::post('startdate')!=null && Input::post('enddate')!=null){
                Session::set( 'file_date',array(Input::post('startdate'),Input::post('enddate')) );
                $d = Session::get('file_date');
                $date[] =  $d[0];
                $date[] =  $d[1];
            }
            
            $maxRows = 15;
            
            $params = $this->request->params();
            
            $baseUrl = SITE_URL.ltrim($params[0], '/');

            $baseUrl = preg_replace('/\/page\/[0-9]*/', '/', $baseUrl);
            
            $offset = ($this->request->page!=null)?($this->request->page - 1) * $maxRows:0 * $maxRows;
            
            $this->logs = FileActivityLogs::getFileReports($maxRows,$offset,$usertype,$category,$date);
            
            $this->initializeReport($this->logs);
            $rowcount = FileActivityLogs::loglist($usertype,$category,$date);
			
            $pageData['flag'] = $usertype;
            $pageData['cat'] = $category;
            $pageData['date'] = $date;
            $pageData['pagination'] = new Paginator($baseUrl, $rowcount, $maxRows, $this->request->page);
            $pageData['report'] = $this->reports;
            $pageData['categories'] = FileActivityLogs::categoryList();
            ksort($pageData['categories']) ;
            
            //css
            $this->page->addAsset('jquery.simple-dtpicker.css');
            $this->page->addAsset('jquery.modal.css');
            $this->page->addAsset('admin.css');
            
            //js
            $this->page->addAsset('jquery.simple-dtpicker.js');
            $this->page->addAsset('jquery.modal.js');
            $this->page->addAsset('admin.js');
            
            
            $this->page->setTitle('File Activity Logs - CICS Drive');
            $this->page->setContent('admin/file_list.php',$pageData);
            
            echo $this->page;
			
        }
	
        public function initializeReport($logs){
			if(count($logs)<1){
				return;
			}
			
            foreach($logs as $key => $log){
                    
                switch($logs[$key]['category']){
                    case FileActivityLogs::CATEGORY_ADMIN_DELETE:   $this->reports[$key]['report'] = $this->setImage('&#59177;').'Administrator '.$this->getAdmin( $logs[$key]['activityID'], FileActivityProperties::INDICATE_ADMIN ).' has deleted '.$logs[$key]['username'].'\'s '.$this->specify($logs[$key]['activityID'], FileActivityProperties::INDICATE_PATH, null, null ,'admin_delete');break;
                    case FileActivityLogs::CATEGORY_ADMIN_REMOVE:   $this->reports[$key]['report'] = $this->setImage('&#9749;').'Administrator '.$this->getAdmin( $logs[$key]['activityID'], FileActivityProperties::INDICATE_ADMIN ).' has removed '.$logs[$key]['username'].'\'s '.$this->specify($logs[$key]['activityID'],  null, FileActivityProperties::INDICATE_OLD_PATH,  FileActivityProperties::INDICATE_NEW_PATH,1);break;
                    case FileActivityLogs::CATEGORY_ADMIN_RESTORE:  $this->reports[$key]['report'] = $this->setImage('&#59249;').'Administrator '.$this->getAdmin( $logs[$key]['activityID'], FileActivityProperties::INDICATE_ADMIN ).' has restored '.$logs[$key]['username'].'\'s '.$this->specify($logs[$key]['activityID'],  FileActivityProperties::INDICATE_PATH);break;
                    case FileActivityLogs::CATEGORY_ARCHIVE:        $this->reports[$key]['report'] = $this->setImage('&#128188;').$logs[$key]['username'].' has acrhived '.$this->specify($logs[$key]['activityID'],  FileActivityProperties::INDICATE_PATH,null,null,'archive');break;
                    case FileActivityLogs::CATEGORY_BACKUP:         $this->reports[$key]['report'] = $this->setImage('&#128230;').$logs[$key]['username'].' has backed up '.$this->specify($logs[$key]['activityID'],  FileActivityProperties::INDICATE_PATH,null,null,'backup');break;
                    case FileActivityLogs::CATEGORY_CREATE_FOLDER:  $this->reports[$key]['report'] = $this->setImage('&#128193;').$logs[$key]['username'].' has created '.$this->specify($logs[$key]['activityID'],  FileActivityProperties::INDICATE_PATH);break;
                    case FileActivityLogs::CATEGORY_DOWNLOAD:       $this->reports[$key]['report'] = $this->setImage('&#128229;').$logs[$key]['username'].' has download '.$this->specify($logs[$key]['activityID'],  FileActivityProperties::INDICATE_PATH);break;
                    case FileActivityLogs::CATEGORY_EDIT:           $this->reports[$key]['report'] = $this->setImage('&#9998;').$logs[$key]['username'].' has editted '.$this->specify($logs[$key]['activityID'],  FileActivityProperties::INDICATE_PATH);break;
                    case FileActivityLogs::CATEGORY_GEN_DELETE:     $this->reports[$key]['report'] = $this->setImage('&#59177;').$logs[$key]['username'].' has deleted '.$this->specify($logs[$key]['activityID'],  FileActivityProperties::INDICATE_PATH);break;
                    case FileActivityLogs::CATEGORY_GEN_REMOVE:     $this->reports[$key]['report'] = $this->setImage('&#9749;').$logs[$key]['username'].' has removed '.$this->specify($logs[$key]['activityID'],  null, FileActivityProperties::INDICATE_OLD_PATH,  FileActivityProperties::INDICATE_NEW_PATH,1);break;
                    case FileActivityLogs::CATEGORY_GEN_RESTORE:    $this->reports[$key]['report'] = $this->setImage('&#59249;').$logs[$key]['username'].' has restored '.$this->specify($logs[$key]['activityID'],  FileActivityProperties::INDICATE_PATH);break;
                    case FileActivityLogs::CATEGORY_PUBLISH:        $this->reports[$key]['report'] = $this->setImage('&#128227;').$logs[$key]['username'].' has published '.$this->specify($logs[$key]['activityID'],  FileActivityProperties::INDICATE_PATH);break;
                    case FileActivityLogs::CATEGORY_RENAME:         $this->reports[$key]['report'] = $this->setImage('&#57347;').$logs[$key]['username'].' has renamed '.$this->specify($logs[$key]['activityID'],  null, FileActivityProperties::INDICATE_OLD_PATH,  FileActivityProperties::INDICATE_NEW_PATH,3);break;
                    case FileActivityLogs::CATEGORY_SHARE:          $this->reports[$key]['report'] = $this->setImage('&#59196;').$logs[$key]['username'].' has shared '.$this->specify($logs[$key]['activityID'],  FileActivityProperties::INDICATE_PATH);break;
                    case FileActivityLogs::CATEGORY_SUBMIT:         $this->reports[$key]['report'] = $this->setImage('&#59200;').$logs[$key]['username'].' has submitted '.$this->specify($logs[$key]['activityID'],  FileActivityProperties::INDICATE_PATH);break;
                    case FileActivityLogs::CATEGORY_UPLOAD:         $this->reports[$key]['report'] = $this->setImage('&#128228;').$logs[$key]['username'].' has uploaded '.$this->specify($logs[$key]['activityID'],  FileActivityProperties::INDICATE_PATH);
                }
                $this->reports[$key]['date'] = Util::getDate($logs[$key]['date']); 
                $this->reports[$key]['time'] = Util::getTime($logs[$key]['date']); 
            }
        }
        
        public function specify($actid,$path=null,$old=null,$new=null,$cat=null){
            
            if($path!=null){
                $value = FileActivityProperties::getSpecificValue($actid, $path);
                switch($cat){
                    case 'archive' : return 'the Current<strong> Drive</strong>.';
                    case 'backup'  : return 'the current<strong> System State</strong>.';
                    case 'admin_delete' :return Util::getItemType($value).' <strong>'.Util::getNameOnly($value).'</strong>';
                    default: return ' '.Util::getItemType($value).' <strong>'.Util::getNameOnly($value).'</strong> at <strong>'.Util::getFolderPathOnly($value).'</strong>.';
                }
                
            }else{
                $old = FileActivityProperties::getSpecificValue($actid, $old);
                $new = FileActivityProperties::getSpecificValue($actid, $new);
                
                switch($cat){
                    case 1: return ' '.Util::getItemType($old).' <strong>'.Util::getNameOnly($old).'</strong> from <strong>'.Util::getFolderPathOnly($old).'</strong>.';
                    case 2: return ' '.Util::getItemType($old).' <strong>'.Util::getNameOnly($old).'</strong> to <strong>'.Util::getFolderPathOnly($new).'</strong>.';
                    case 3: return ' '.Util::getItemType($old).' <strong>'.Util::getNameOnly($old).'</strong> into '.Util::getItemType($new).' <strong>'.Util::getNameOnly($new).'</strong> in <strong>'.Util::getFolderPathOnly($old).'</strong>.';
                }
            }
        }
        
	public function setImage($img){
            return '<span class="icon" style="color:#494949;padding-right:5px;margin-right:10px;">'.$img.'</span>';
        }
        
        public function getAdmin($actid , $id){
            $id = FileActivityProperties::getSpecificValue($actid, $id);
            $admin = User::getByID((string)$id);
            return '<strong>'.$admin->getFirstName().' '.$admin->getLastName().'</strong>';
        }
}