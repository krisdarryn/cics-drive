<?php

require_once MODEL.'User.php';
require_once MODEL.'FileFilters.php';
require_once PROCESS.'AdminProcess.php';
require_once LIBRARY.'Input.php';
/**
 * @package process
 * @subpackage student
 */
 
class FileFilter extends AdminProcess {
	
	public function run() {
		parent::run();
		if(( Input::post('forStudent')!=null || Input::post('forFaculty')!=null ) && !Input::post('add')){
			$this->setUserLog(Input::post('forStudent'), Input::post('forFacaulty')!=null, Input::post('extension'));
                        echo json_encode(FileFilters::updateFileFilter(Input::post('forFaculty'),Input::post('forStudent'),Input::post('extension')));
			die();
		}
			
		if ( Input::post('add') != null ){
                    $this->addFileFilter();
		}
		
		if ( Input::post('delete') != null ){
			FileFilters::deleteFromFileFilter(Input::post('delete'));
                        $actid = $this->setUserActivityLog(UserActivityLogs::CATEGORY_FILE_FILTER_REMOVE, $this->getUser()->getUserID());
                        $this->setUserActivityProperties($actid, UserActivityProperties::FILE_FILTER, Input::post('delete'));
		}
		
		$this->page->addAsset('update.file.filter.js');
		$pageData['filters'] = FileFilters::fileList();
		$this->page->setTitle('File Filter - CICS Drive');
		$this->page->setContent('admin/file_filter.php',$pageData);
		echo $this->page;
	}
	
	private function addFileFilter(){
		$newUser = new FileFilters();
		$forFaculty = Input::post('faculty');
		$forStudent = Input::post('student');
		if(FileFilters::existingExtension(Input::post('extension')) != null ){
			$this->page->addAlert('Extension already exist');
		}else{ 
			if(Input::post('extension')!=null){
				if(ctype_alnum(Input::post('extension'))){
					if(ctype_graph(Input::post('extension'))){
                                          if(Input::post('add')){
							if($forFaculty != null && $forStudent != null){
                                                            //$this->setUserLog($forStudent, $forFaculty, Input::post('extension'));
                                                            $a = $newUser->addFileExtension(strtolower(Input::post('extension')),$forFaculty,$forStudent);
							}else if($forFaculty == null && $forStudent != null){
								$forFaculty = 0;
								$a = $newUser->addFileExtension(strtolower(Input::post('extension')),$forFaculty,$forStudent);
                                                                //$this->setUserLog($forStudent, $forFaculty=null, Input::post('extension'));
							}else if($forFaculty != null && $forStudent == null){
								$forStudent = 0;
								$a = $newUser->addFileExtension(strtolower(Input::post('extension')),$forFaculty,$forStudent);
                                                                //$this->setUserLog($forStudent=null, $forFaculty, Input::post('extension'));
							}else{
								$forFaculty = 0;
								$forStudent = 0;
								$a = $newUser->addFileExtension(strtolower(Input::post('extension')),$forFaculty,$forStudent);
                                                                //$this->setUserLog($forStudent=null, $forFaculty=null, Input::post('extension'));
							}
                                                        
							$message = 'File extension added';
						}
						if ($a){
							$this->page->addAlert($message, 'success');
                                                        $actid = UserActivityLogs::logAcivity(UserActivityLogs::CATEGORY_FILE_FILTER_ADD, $this->getUser()->getUserID());
                                                        UserActivityProperties::logProperties($actid, UserActivityProperties::FILE_FILTER,strtolower(Input::post('extension')));

						}else{
							$this->page->addAlert('Error during update.', 'error');
						}
					}else{
						$this->page->addAlert('Character spacing has been Detected.', 'error');
					}
				}else{
					$this->page->addAlert('Special character has been Detected.', 'error');
					}
			}
		}
	}
        
        /**
         *
         * @param type $student
         * @param type $faculty
         * @param type $extension 
         */
        public function setUserLog($student=null,$faculty=null,$extension=null){

                $actid = $this->setUserActivityLog(UserActivityLogs::CATEGORY_FILE_FILTER_UPDATE, $this->getUser()->getUserID());
                UserActivityProperties::logProperties($actid, UserActivityProperties::FILE_FILTER, $extension);
          
        }
}