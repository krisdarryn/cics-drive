<?php

require_once MODEL.'User.php';
require_once MODEL.'StudentsQuestions.php';
require_once PROCESS.'AdminProcess.php';
require_once LIBRARY.'Input.php';
/**
 * @package process
 * @subpackage student
 */
class ViewUserDetails extends AdminProcess{
	
	protected $user;
	protected $adminUser;
	public function run() {
		
		parent::run();
		$this->adminUser = $this->getUser();
                
		if(Input::post('updateAccount')){
			$this->user->updateUserAccount();
		}
		
		if(Input::post('user_id')!=null){
			Session::set('user_id',Input::post('user_id'));        
			$this->user = User::getByID(Input::post('user_id'));
		}
		
		if(Session::get('user_id')!=null){
			$this->user = User::getByID(Session::get('user_id'));
			$this->updateUserAccount();
		}
		else{
			$this->response->redirect('/404');
			die();
		}
		
		//debug_print_r($this->user);
		//$this->page->addAlert('Must fill-up all student fields');
		$pageData['user'] = $this->user;
		$this->page->setHeader('');
		$this->page->setFooter('');
		$this->page->setContent('admin/view_user_details.php',$pageData);
		echo $this->page;		
	}
	
	private function updateUserAccount(){ 
			if(Input::post('fname')!=null && Input::post('lname')!=null)
			{
				if(ctype_digit(Input::post('fname')) || ctype_digit(Input::post('mname')) || ctype_digit(Input::post('lname'))){
					$this->page->addAlert('Numeric character is not allowed.', 'error');
				}else{

					$this->user->setFirstName(Input::post('fname', false));
					$this->user->setLastName(Input::post('lname', false));
					$this->user->setMiddleInitial(Input::post('mname'));
                    $this->user->setStatus(Input::post('userStatus'));
                    $this->user->setIdNumber(Input::post('idno'));
                    $this->user->setEmail(Input::post('email'));
					$lname = Input::post('lname');
					
					$a = $this->user->updateRecord();
						
					if ($a){
						if(Input::post('updateAccount')==2){
							$this->page->addAlert('Account "'.$lname.'" was successfuly updated', 'success');
						}elseif(Input::post('updateAccount')==1){
							$this->page->addAlert('Account "'.$lname.'" was successfuly updated', 'success');
						}else{
							$this->page->addAlert('Account "'.$lname.'" was successfuly updated', 'success');
						}
                                                $logID = UserActivityLogs::logAcivity(UserActivityLogs::CATEGORY_ACCOUNT_UPDATE, $this->adminUser->getUserID());
                                                if ($logID) {
                                                  UserActivityProperties::logProperties($logID, UserActivityProperties::USER_ID, $this->getUser()->getUserID());
                                                }
					}else{
						$this->page->addAlert('Error during update.', 'error');
					}
				}
			}
		}
}
	