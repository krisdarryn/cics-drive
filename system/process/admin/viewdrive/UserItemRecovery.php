<?php
/**
 * @package process
 * @subpackage student
 */

require_once PROCESS.'AdminProcess.php';
require_once LIBRARY.'/drive/StudentDrive.php';
require_once LIBRARY.'drive/Folder.php';
require_once LIBRARY.'drive/StudentDrive.php';
require_once LIBRARY.'ItemRecovery.php';

class UserItemRecovery extends AdminProcess {

	/**
	 * @var Drive $drive An instance of a Drive class.
	 */
	private $itemrecovery;
	
	/**
	 * @var Drive $drive An instance of a Drive class.
	 */
	private $drive;
	

	/**
	*	@var use as a path of the item that was removed 
	*/
	private $remove_path;
	
	/**
	*	@var use as the user object when viewing a user drive by the administrator
	*/
	private $viewuser;
	
	public function run() {
		
		parent::run();
		
		if(Session::get('view_user_id') != null){
			$this->User = User::getByID(Session::get('view_user_id'));	
		}else{
			$this->response->redirect('/404');
			die();
		}
		
		$uri = $this->request->params();
		
		$this->itemrecovery = new ItemRecovery( $this->getUser(), array('viewdrive' => true, 'viewtype' => 0 ), $this->User); 
		$this->drive = new StudentDrive(null,1,$this->User);
	
		//delete item	
		if(Input::post('delete')){	
			$items = Input::post('delete');
			$count=0;
			foreach($items as $item){
				if($this->itemrecovery->delete( $item )){
					$count++;
				}
			}
			$this->page->addAlert($count.' Item(s) has been deleted permanently','success');
		}
	
		$pageData['contentString'] = $this->itemrecovery->getTrashedContents();
		$pageData['dir'] = urldecode($uri[0]);
		
		$this->page->addAsset('drive.css');
		$this->page->addAsset('dropzone.css');
		$this->page->addAsset('jquery.contextMenu.css');
		$this->page->addAsset('jquery.modal.css');
		$this->page->addAsset('drive.menu.css');
		$this->page->addAsset('our.modal.css');
		
		$this->page->addAsset('jquery.modal.min.js');
		$this->page->addAsset('jquery.modal.js');
		$this->page->addAsset('custom_dialog.js');
		$this->page->addAsset('drive.functions.js');
		$this->page->addAsset('jquery.contextMenu.js');
		$this->page->addAsset('drive.js');
		
		$pageData['user'] = $this->User;
		
		$this->page->setHeader('admin/viewdrive/header.php',$pageData);
		$this->page->setFooter('');
		$this->page->setContent('admin/viewdrive/item_recovery_user.php',$pageData);
		
		echo $this->page;
	
	}
	
}