<?php

require_once MODEL . 'User.php';
require_once MODEL . 'Util.php';
require_once PROCESS . 'AdminProcess.php';
require_once LIBRARY . 'Paginator.php';
require_once MODEL . 'UserActivityLogs.php';
require_once MODEL . 'StudentsQuestions.php';

/**
 * @package process
 * @subpackage student
 */
class UserActivity extends AdminProcess {

  private $report;

  public function run() {
    parent::run();
    $offset = 0;

    $this->report = array();


    $this->reports = array();
    $session = Session::get('userActivity');
    if ($session == null) {
      $session = array(
        "userType" => array(3, 2, 1),
        "category" => null,
        "start" => null,
        "end" => null
      );
    }

    $usertype = $session['userType'];
    $category = $session['category'];
    $start = $session['start'];
    $end = $session['end'];
    $date = array();

    if (Input::post('userType') != null && is_array(Input::post('userType'))) {
      $ut = array();
      $usertype = Input::post('userType');
      if (isset($userType[0]) && $userType[0] == "on") {
        $ut[] = UserType::STUDENT;
      }
      if (isset($userType[1]) && $userType[1] == "on") {
        $ut[] = UserType::FACULTY;
      }
      if (isset($userType[3]) && $userType[3] == "on") {
        $ut[] = UserType::ADMIN;
      }
      $session['userType'] = $usertype;
    }

    if (Input::post('category') != null) {
      $category = Input::post('category');
      if (Input::post('category') == 0)
        $category = null;

      $session['category'] = $category;
    }

    if (Input::post('startdate') != null && Input::post('enddate')) {
      $start = DateTime::createFromFormat("Y/m/d", Input::post('startdate'));
      $end = DateTime::createFromFormat("Y/m/d", Input::post('enddate'));
      ;
      $session['start'] = $start;
      $session['end'] = $end;
    }


    Session::set('userActivity', $session);


    $params = $this->request->params();

    $baseUrl = SITE_URL . ltrim($params[0], '/');

    $baseUrl = preg_replace('/\/page\/[0-9]*/', '/', $baseUrl);

    $maxRows = 15;

    $offset = ($this->request->page != null) ? ($this->request->page - 1) * $maxRows : 0 * $maxRows;

    //$logs = UserActivityLogs::getUserReports($maxRows,$offset,$usertype,$category);
    //$this->initializeReport($logs);
    $date = null;
    if ($session['start'] != null && $session['end'] != null) {
      $date[0] = $session['start']->format(DatabaseEntity::SQL_DATETIME_FORMAT);
      $date[1] = $session['end']->format(DatabaseEntity::SQL_DATETIME_FORMAT);
    }

    $results = UserActivityLogs::generateReport($offset, $maxRows, $usertype, $session['category'], $date);
    $rowcount = UserActivityLogs::countReports(UserActivityLogs::generateReport(0, 10000000, $usertype, $session['category'], $date));

    //$offset = ($pageData['pagination']->getCurrentPage()-1) * $maxRows;
    $pageData['pagination'] = new Paginator($baseUrl, $rowcount, $maxRows, $this->request->page);




    //css
    $this->page->addAsset('jquery.simple-dtpicker.css');
    $this->page->addAsset('jquery.modal.css');
    $this->page->addAsset('admin.css');

    //js
    $this->page->addAsset('jquery.simple-dtpicker.js');
    $this->page->addAsset('jquery.modal.js');
    $this->page->addAsset('admin.js');

    $pageData['results'] = $this->parseResults($results);
    $categoryList = UserActivityLogs::categoryList();
    ksort($categoryList);
    $pageData['catOptions'] = $categoryList;
    $this->page->setTitle('File Activity Logs - CICS Drive');

    $pageData['session'] = $session;

    $this->page->setContent('admin/user_list.php', $pageData);
    $this->page->setFooter('admin/footer.php');
    echo $this->page;
  }

  public function initializeReport($logs) {
    if (count($logs) < 1) {
      return;
    }
    foreach ($logs as $key => $log) {

      switch ($log['category']) {
//                    case UserActivityLogs::CATEGORY_CHANGE: $this->report[$key][] = $this->readyReport($log['activityID'], $id); break;
//                    case UserActivityLogs::CATEGORY_UPDATE:
//                    case UserActivityLogs::CATEGORY_CREATE_ACCOUNT: break;
//                    case UserActivityLogs::CATEGORY_RECOVER:    break;
//                    case UserActivityLogs::CATEGORY_ACTIVATE:   break;
      }
    }
  }

  public function readyReport($actid, $id) {
    $value = UserActivityProperties::getSpecificValue($actid, $id);

    debug_print_r($value);
  }

  public function parseResults($results) {
    $str = '';
    foreach ($results as $result) {
      $description = '';
      $actid = $result['activityID'];
      $property = UserActivityProperties::getProperties($actid);
      $hasProperties = false;
      if (count($property) > 0) {
        $hasProperties = true;
      }
      switch ($result['category']) {
        case UserActivityLogs::CATEGORY_ACCOUNT_UPDATE:

          if ($hasProperties) {
            $user = User::getByID($property[UserActivityProperties::USER_ID]->getValue());
            if ($user != null) {
              $subject = $user->getFirstName() . ' ' . $user->getLastName();
              $description = $result['user'] . ' updated ' . $subject . '\'s account';
            }
          } else {
            $description = $result['user'] . ' updated his/her account';
          }
          break;
        case UserActivityLogs::CATEGORY_ACCOUNT_ACTIVATE:
          $description = $description = $result['user'] . ' activated his/her account';
          break;
        case UserActivityLogs::CATEGORY_ACCOUNT_PASSWORD_RECOVER:
          $description = $description = $result['user'] . ' recovered his/her account';
          break;
        case UserActivityLogs::CATEGORY_MISSED_QUESTION:
          if ($hasProperties) {
            $questionID = $property[UserActivityProperties::SECURITY_QUESTION_ID]->getValue();
            $question = StudentsQuestions::getById($questionID, true);
            if ($question != null) {
              $description = $result['user'] . ' supplied the wrong answer to ' . $question->getQuestion();
            }
          }

          break;
        case UserActivityLogs::CATEGORY_ACCOUNT_CREATE:
          if ($hasProperties) {
            $user = User::getByID($property[UserActivityProperties::USER_ID]->getValue());
            if ($user != null) {
              $subject = $user->getFirstName() . ' ' . $user->getLastName();
              $description = $result['user'] . ' created ' . $subject . '\'s account';
            }
          }
          break;
        case UserActivityLogs::CATEGORY_ACCOUNT_UPDATE_STATUS:
          if ($hasProperties) {
            $userID = $property[UserActivityProperties::USER_ID]->getValue();
            $user = User::getByID($userID);
            $status = $property[UserActivityProperties::USER_STATUS]->getValue();
            switch ($status) {
              case User::STATUS_ACTIVE:
                $status = 'active';
                break;
              case User::STATUS_INACTIVE:
                $status = 'inactive';
                break;
              case User::STATUS_SUSPENDED:
                $status = 'suspended';
                break;
              case User::STATUS_SUSPICIOUS:
                $status = 'suspicious';
                break;
            }
            $subject = '(' . $user->getUserID() . ') ' . $user->getFirstName() . ' ' . $user->getLastName();

            $description = $result['user'] . ' changed ' . trim($subject) . '\'s status to ' . $status;
          }
          break;
        case UserActivityLogs::CATEGORY_SECURITY_QUESTION_ADD:
          if ($hasProperties) {
            $question = StudentsQuestions::getById($property[UserActivityProperties::SECURITY_QUESTION_ID]->getValue(), true);
            if ($question != null) {
              $description = $result['user'] . ' added the question: "' . $question->getQuestion() . '"';
            }
          }
          break;
        case UserActivityLogs::CATEGORY_SECURITY_QUESTION_ENABLED:
          if ($hasProperties) {
            $question = StudentsQuestions::getById($property[UserActivityProperties::SECURITY_QUESTION_ID]->getValue(), true);
            if ($question != null) {
              $description = $result['user'] . ' enabled the question: "' . $question->getQuestion() . '"';
            }
          }
          break;
        case UserActivityLogs::CATEGORY_SECURITY_QUESTION_DISABLED:
          if ($hasProperties) {
            $question = StudentsQuestions::getById($property[UserActivityProperties::SECURITY_QUESTION_ID]->getValue(), true);
            if ($question != null) {
              $description = $result['user'] . ' disabled the question: "' . $question->getQuestion() . '"';
            }
          }
          break;
        case UserActivityLogs::CATEGORY_FILE_FILTER_ADD:
          if ($hasProperties) {
            $description = $result['user'] . ' added file filter ' . $property[UserActivityProperties::FILE_FILTER]->getValue();
          }
          break;
        case UserActivityLogs::CATEGORY_FILE_FILTER_REMOVE:
          if ($hasProperties) {
            $description = $result['user'] . ' removed file filter ' . $property[UserActivityProperties::FILE_FILTER]->getValue();
          }
          break;
        case UserActivityLogs::CATEGORY_FILE_FILTER_UPDATE:
          if ($hasProperties) {
            $description = $result['user'] . ' updated file filter ' . $property[UserActivityProperties::FILE_FILTER]->getValue();
          }
          break;
        case UserActivityLogs::CATEGORY_ACCOUNT_IMPORT:
          $description = $result['user'] . ' imported student accounts from a csv';
          break;
      }
      $datetime = DateTime::createFromFormat(DatabaseEntity::SQL_DATETIME_FORMAT, $result['date']);
      $date = $datetime->format('F j,Y');
      $time = $datetime->format('g:i a');
      $str .= '
				<tr>
					<td>' . $description . '</td>
					<td>' . $date . '</td>
					<td>' . $time . '</td>
				</tr>   ';
    }
    return $str;
  }

  public function parseResults1($results) {

    $str = '';
    foreach ($results as $key => $result) {
      if ($result['category'] == null) {
        continue;
      }
      $description = '';

      switch ($result['category']) {
        case UserActivityLogs::CATEGORY_ACCOUNT_UPDATE:
          if (key_exists($key + 1, $results) && $results[$key + 1]['category'] == null && $results[$key + 1]['activityID'] == $results[$key]['activityID']) {
            if ($results[$key + 1]['id'] == UserActivityProperties::USER_ID) {
              $user = User::getByID($results[$key + 1]['value']);
              $subject = $user->getFirstName() . ' ' . $user->getLastName();
              $description = $result['user'] . ' updated ' . $subject . '\'s account';
            } else {
              continue;
            }
          } else {
            $description = $result['user'] . ' updated his/her account';
          }

          break;
        case UserActivityLogs::CATEGORY_ACCOUNT_ACTIVATE:
          $description = $result['user'] . ' activated his/her account';
          break;
        case UserActivityLogs::CATEGORY_ACCOUNT_PASSWORD_RECOVER:
          $description = $result['user'] . ' recover his/her password';
          break;
        case UserActivityLogs::CATEGORY_MISSED_QUESTION:
          if (key_exists($key + 1, $results) && $results[$key + 1]['category'] == null && $results[$key + 1]['activityID'] == $results[$key]['activityID']) {
            if ($results[$key + 1]['id'] == UserActivityProperties::SECURITY_QUESTION_ID) {
              $question = StudentsQuestions::getById($results[$key + 1]['value'], true);
              if ($question != null) {
                $description = $result['user'] . ' supplied the wrong answer to: "' . $question->getQuestion().'"';
              }
            }
          }

          break;
        case UserActivityLogs::CATEGORY_ACCOUNT_CREATE:
          if (key_exists($key + 1, $results) && $results[$key + 1]['category'] == null && $results[$key + 1]['activityID'] == $results[$key]['activityID']) {
            if ($results[$key + 1]['id'] == UserActivityProperties::USER_ID) {
              $user = User::getByID($results[$key + 1]['value']);
              $subject = '(' . $user->getUserID() . ') ' . $user->getFirstName() . ' ' . $user->getLastName();
              $description = $result['user'] . ' created ' . trim($subject) . '\'s account';
            }
          }
          break;
        case UserActivityLogs::CATEGORY_ACCOUNT_UPDATE_STATUS:
          if ((key_exists($key + 1, $results) && $results[$key + 1]['category'] == null && $results[$key + 1]['activityID'] == $results[$key]['activityID']) &&
            (key_exists($key + 2, $results) && $results[$key + 2]['category'] == null && $results[$key + 2]['activityID'] == $results[$key]['activityID'])) {
            if ($results[$key + 1]['id'] == UserActivityProperties::USER_ID && $results[$key + 2]['id'] == UserActivityProperties::USER_STATUS) {
              $user = User::getByID($results[$key + 1]['value']);
              $status = $results[$key + 2]['value'];
              switch ($results[$key + 2]['value']) {
                case User::STATUS_ACTIVE:
                  $status = 'active';
                  break;
                case User::STATUS_INACTIVE:
                  $status = 'inactive';
                  break;
                case User::STATUS_SUSPENDED:
                  $status = 'suspended';
                  break;
                case User::STATUS_SUSPICIOUS:
                  $status = 'suspicious';
                  break;
              }
              $subject = '(' . $user->getUserID() . ') ' . $user->getFirstName() . ' ' . $user->getLastName();

              $description = $result['user'] . ' changed ' . trim($subject) . '\'s status to ' . $status;
            }
          }
          break;
        case UserActivityLogs::CATEGORY_SECURITY_QUESTION_ADD:
          if (key_exists($key + 1, $results) && $results[$key + 1]['category'] == null && $results[$key + 1]['activityID'] == $results[$key]['activityID']) {
            if ($results[$key + 1]['id'] == UserActivityProperties::SECURITY_QUESTION_ID) {
              $question = StudentsQuestions::getById($results[$key + 1]['value'], true);
              if ($question != null) {
                $description = $result['user'] . ' added the question: "' . $question->getQuestion() . '"';
              }
            }
          }
          break;
        case UserActivityLogs::CATEGORY_SECURITY_QUESTION_ENABLED:
          if (key_exists($key + 1, $results) && $results[$key + 1]['category'] == null && $results[$key + 1]['activityID'] == $results[$key]['activityID']) {
            if ($results[$key + 1]['id'] == UserActivityProperties::SECURITY_QUESTION_ID) {
              $question = StudentsQuestions::getById($results[$key + 1]['value'], true);
              if ($question != null) {
                $description = $result['user'] . ' enabled the question: "' . $question->getQuestion() . '"';
              }
            }
          }
          break;
        case UserActivityLogs::CATEGORY_SECURITY_QUESTION_DISABLED:
          if (key_exists($key + 1, $results) && $results[$key + 1]['category'] == null && $results[$key + 1]['activityID'] == $results[$key]['activityID']) {
            if ($results[$key + 1]['id'] == UserActivityProperties::SECURITY_QUESTION_ID) {
              $question = StudentsQuestions::getById($results[$key + 1]['value'], true);
              if ($question != null) {
                $description = $result['user'] . ' disabled the question: "' . $question->getQuestion() . '"';
              }
            }
          }
          break;
        case UserActivityLogs::CATEGORY_FILE_FILTER_ADD:
          if (key_exists($key + 1, $results) && $results[$key + 1]['category'] == null && $results[$key + 1]['activityID'] == $results[$key]['activityID']) {
            if ($results[$key + 1]['id'] == UserActivityProperties::FILE_FILTER) {
              $description = $result['user'] . ' added file filter ' . $results[$key + 1]['value'];
            }
          }
          break;
        case UserActivityLogs::CATEGORY_FILE_FILTER_REMOVE:
          if (key_exists($key + 1, $results) && $results[$key + 1]['category'] == null && $results[$key + 1]['activityID'] == $results[$key]['activityID']) {
            if ($results[$key + 1]['id'] == UserActivityProperties::FILE_FILTER) {
              $description = $result['user'] . ' removed file filter ' . $results[$key + 1]['value'];
            }
          }
          break;
        case UserActivityLogs::CATEGORY_FILE_FILTER_UPDATE:
          if (key_exists($key + 1, $results) && $results[$key + 1]['category'] == null && $results[$key + 1]['activityID'] == $results[$key]['activityID']) {
            if ($results[$key + 1]['id'] == UserActivityProperties::FILE_FILTER) {
              $description = $result['user'] . ' updated file filter ' . $results[$key + 1]['value'];
            }
          }
          break;
      }

      $datetime = DateTime::createFromFormat(DatabaseEntity::SQL_DATETIME_FORMAT, $result['date']);
      $date = $datetime->format('F j,Y');
      $time = $datetime->format('g:i a');
      $str .= '
				<tr>
					<td>' . $description . '</td>
					<td>' . $date . '</td>
					<td>' . $time . '</td>
				</tr>                
';
    }
    return $str;
  }

}