<?php
require_once MODEL.'User.php';
require_once LIBRARY.'Session.php';
/**
 * @package process
 */
class DriveView extends Process {
	public function run(){
		
		$user = '';
		
		if(Input::post('killUserViewSession')){
			Session::remove('view_user_id');
			Session::remove('admin_id');
			echo json_encode(true);
			die();
		}
		
		if(Input::post('view')){
			if(Input::post('toView')){
				$user = User::getByID(Input::post('view'));
				$user->setDriveView( Input::post('data') =='3'?0:Input::post('data') );
				echo json_encode( $user->updateRecord() );
			}else{
				$user = User::getByID(Input::post('view'));
				echo json_encode( $user->getDriveView() );
			}
		}else{
                        $us = Session::getUser();
                        $us = $us['userId'];
			if(Input::post('data')){        
				$user = User::getByID($us);
				$user->setDriveView( Input::post('data') =='3'?0:Input::post('data') );
				echo json_encode( $user->updateRecord() );
			}else{
				$user = User::getByID($us);
				echo json_encode( $user->getDriveView() ); 
			}
		}
		die();
	}
}