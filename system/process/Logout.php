<?php
require_once LIBRARY.'Session.php';

/**
 * @package process
 */
class Logout extends Process {
	public function run() {
                if(!Session::get('addQuestion')){
                    Session::logout();
                    Session::remove('questionSet');
                    Session::remove('validationData');
                    $this->response->redirect(SITE_URL.'login');
                }else{
                    $this->response->redirect(SITE_URL.'student/drive');
                }
		
	}
}