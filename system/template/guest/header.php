<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<title><?php echo $this->getTitle();?></title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="robots" content="" />
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
	<link rel="favicon" type="image/x-icon" href="/favicon.ico">
	<?php echo $this->getCssAssets();?>
	<!--[if IE]><link rel="stylesheet" href="media/css/ie.css" media="all" /><![endif]-->
	<style>
	a {
		text-decoration:underline !important;
	}
	 .login section h1 {margin-bottom:5px;}
	 h2 {
		color: #fff;
		font-family: 'Open Sans', sans-serif;
		font-size: 28px;
		margin-bottom: 35px;
		letter-spacing: -1px;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.2);
	 }
	 h3 {
	 	font-size:20px;
		color: #fff;
		font-family: 'Open Sans', sans-serif;
		margin-top:35px;
		margin-bottom: 15px;
		letter-spacing: -1px;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.2);
	 }
	 
	  h4 {
	 	font-size:20px;
		color: #fff;
		font-family: 'Open Sans', sans-serif;
		margin-bottom: 5px;
		float:left;
		letter-spacing: -1px;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.2);
	 } 
		
	 h5 {
	 	font-size:18px;
		color: #fff;
		font-family: 'Open Sans', sans-serif;
		margin-bottom: 5px;
		letter-spacing: -1px;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.2);
	 }
	 h6 {
	 	font-size:25px;
		color: #fff;
		font-family: 'Open Sans', sans-serif;
		letter-spacing: -1px;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.2);
	 }
	 .alert {
	 	top:50px;
	 	background-color: transparent !important;
	 	border-bottom: 0 !important;
	 	width: 100%;
	 	left:1px;
		position:absolute;
	 }
	 section.alert div {
	 	margin:0 30%;
	 }
	 div.orange, div.red, div.green {
	 	font-size: 15px;
	 	font-weight: bold;
	 }
	 div.orange a {
	 	color:#FFF !important;
	 }
	 p {
	 	color: #fff;
	 }

	 
	 a.button {
	 	text-decoration: none !important;
	 	text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.4);
	 	padding:12.5px;
	 	font-weight: bold;
	 	margin-top:-1px !important;

	 }

	 span.question{
		float:right;
		text-decoration:underline;
		color: rgb(93, 172, 237);
	} 
	
	span.question:hover{
		cursor:pointer;
	}

	hr {
	    border: 0;
	    height: 0;
	    border-top: 1px solid rgba(0, 0, 0, 0.3);
	    border-bottom: 1px solid rgba(255, 255, 255, 0.3);
	    margin:15px 0;
	}
	p.timer{
		font-size:18px;
		text-align:right;
	}
	p > strong{
		color:#f33;
		font-size:25px;
		font-family:'times_new_roman';
	}
	label{
		font-size:18px;
		color: #fff;
		font-family: 'Open Sans', sans-serif;
		margin-bottom: 5px;
		letter-spacing: -1px;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.2);
		float:left;
	}
	</style>
</head>
<body class="login">
<?php
	echo $this->getAlertString();
?>

