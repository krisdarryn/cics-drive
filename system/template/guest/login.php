	<section>
		<h1><strong>CICS</strong> Drive</h1>
		<form method="post" action="">
			<input type="text" name="email" placeholder="Email" required  />
			<input placeholder="Password" name="password" type="password" required />
			<button class="blue">Login</button>
		</form>
		<p><a href="/recovery">Forgot your password?</a></p>
		<p><a href="<?php echo SITE_URL."register";?>"><button>Create Account</button></a></p>
	</section>
      <div class="comContent">
				<div class = "textModal">
					<h2>The version of your current browser is outdated.</h2>
					<p>*Some features may not be supported by your current browser, please choose the recommended versions of your browsers below. </p>
				</div>
			   <div class="firefox">
                    <a href="http://www.mozilla.org/en-US/firefox/new/"><span>Mozilla FireFox</span> <br>16.0 or higher</a>
                </div>
                <div class="ie">
                    <a href="http://www.microsoft.com/en-us/download/internet-explorer-10-details.aspx">
					<span>Internet Explorer</span> <br>10</a>
                </div>
                <div class="chrome">
                    <a href="https://www.google.com/intl/en/chrome/browser/?hl=en&brand=CHMA&utm_campaign=en&utm_source=en-ha-sea-ph-bk&utm_medium=ha">
					<span>Google Chrome</span> <br>23.0 or higher</a> 
                </div>
                <div class="safari">
                    <a href="http://www.mozilla.org/en-US/firefox/new/">
					<span>Safari</span> <br>5.1 or higher</a>
                </div>
        </div>
