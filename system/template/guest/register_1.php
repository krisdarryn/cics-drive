<section>
	<h1><strong>CICS</strong> Drive</h1>
	<h2>Registration</h2>
	<form method="post" action="/register"> 
	    <label for="idnumber" >ID Number</label>
		<input id="idnumber" pattern="\d{1,8}" maxlength="8" name="idNumber" value="" type="text" placeholder="ID Number" required />
		<label for="lastname">Last Name</label>
		<input id="lastname" type="text" placeholder="Last Name" required name="lastName"/>
		<a class="button "style="width:37%;display:inline-block;color:white;" href="<?php echo SITE_URL."login";?>">Back</a>
		<button style="width:49%;display:inline;" class="green" name="step1" value="step1" >Next</button>
	</form>
	<h3>Step 1 of 3</h3>
	<p><a href="<?php echo SITE_URL."login";?>">Already have an account? login here.</a></p>
</section>		