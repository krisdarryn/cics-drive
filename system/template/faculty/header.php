<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<title><?php echo $this->getTitle();?></title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="robots" content="" />
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
	<?php echo $this->getCssAssets();?>
	<!--[if IE]><link rel="stylesheet" href="css/ie.css" media="all" /><![endif]-->
</head>
<body>
<header class="main">
	<h1><strong>CICS</strong> Drive</h1>
</header>
<section class="user">
	<div class="profile-img">
		<p><a href="/faculty/account/edit"><img src="<?php echo $user->getAvatarUrl();?>" alt="" height="40" width="40" /></a	> Welcome back <?php echo $user->getFirstName();?></p>
	</div>
	<div class="buttons">
		<!--<button class="ico-font">&#59157;</button>-->
<!--		<span class="button dropdown">
			<a href="#">Notifications <span class="pip">3</span></a>
			<ul class="notice">
				<?php echo $notifications;?>
				<li>
					<hgroup>
						<h1><a href="#">View all notifications</a></h1>

					</hgroup>
				</li>
			</ul>
		</span> -->

		<a href="<?php echo SITE_URL.'logout';?>"><button class="blue" style="float:right">Logout</button></a>
		<!--<?php echo $storageindicator;?>-->
	</div>
</section>

<nav>
	<ul>
		<li><a href="<?php echo SITE_URL.'faculty/drive';?>"><span class="icon">&#59256;</span> My Drive</a></li>
		<li><a id="editLink" href="javascript:;"><span class="icon">&#59256;</span>Code Editor</a></li>
                <li><a href="<?php echo SITE_URL.'faculty/item_recovery';?>"><span class="icon">&#59249;</span> Item Recovery</a></li>
		<li><a href="<?php echo SITE_URL.'faculty/archive';?>"><span class="icon">&#128188;</span> Archive</a></li>
                <li>
			<a id="s1"><span class="icon">&#128100;</span> Account <span class="miniIcon expandMenu">&#59228;</span></a>
			<ul class="submenu">
				<li><span class="subicon">&#10002;</span><a href="<?php echo SITE_URL.'faculty/account/edit';?>">Edit Account</a></li>
				<li><span class="subicon">&#9998;</span><a href="<?php echo SITE_URL.'faculty/account/edit/login_details';?>">Change Login Details</a></li>
			</ul>
		</li>
		<li><a href="<?php echo SITE_URL.'faculty/publications';?>"><span class="icon">&#9733;</span> Publications</a></li>
		<li></li>
	</ul>
	
</nav>
<?php echo $this->getAlertHtml();?>
<section class="content">