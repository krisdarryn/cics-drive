﻿
	<section class="widget">
		<header>
			<span class="icon">&#10002;</span>
			<hgroup>
				<h1>Edit Account</h1>
				<!-- <h2>Drag &amp drop file upload</h2> -->
			</hgroup>
		</header>
		<div class="content">
			<form method="post" enctype="multipart/form-data">
				<div class="field-wrap">
					<label for="lastName">Id Number:</label>
					<input type="text" name="idno" maxlength="8" id="idno" placeholder="<?php echo $user->getidNumber();?>">
				</div>
				<div class="field-wrap">
					<label for="firstName">First Name:</label>
					<input type="text" name="firstName"  id="firstName" placeholder="<?php echo $user->getFirstName(); ?>" >
				</div>
				<div class="field-wrap">
					<label for="middleInitial">Middle Initial:</label>
					<input type="text" name="middleInitial" maxlength="1" id="middleInitial" placeholder="<?php echo $user->getMiddleInitial();?>">
				</div>
				<div class="field-wrap">
					<label for="lastName">Last Name:</label>
					<input type="text" name="lastName" id="lastName" placeholder="<?php echo $user->getLastName();?>">
				</div>
				<div class="field-wrap">
					<label for="avatar">Avatar: </label>
					<input type="file" id="avatar" accept="image/jpeg, image/png" name="avatar" style="background:url('<?php echo $user->getAvatarUrl();?>') no-repeat 5px 0px; background-size:contain; padding-left: 60px"/>
				</div>

				<button>Save Changes</button>
			</form>

		</div>
	</section>


