﻿	<section class="widget">
		<header>
			<span class="icon">&#10002;</span>
			<hgroup>
				<h1>Edit Account</h1>
				<!-- <h2>Drag &amp drop file upload</h2> -->
			</hgroup>
		</header>
		<div class="content">
			<form method="post" enctype="multipart/form-data">
				<div class="field-wrap">
					<label for="firstName">First Name:</label>
					<input type="text" disabled name="firstName" id="firstName" placeholder="<?php echo $user->getFirstName();?>">
				</div>
				<div class="field-wrap">
					<label for="middleInitial">Middle Initial:</label>
					<input type="text" name="middleInitial" maxlength="1" id="middleInitial" placeholder="<?php echo $user->getMiddleInitial();?>">
				</div>
				<div class="field-wrap">
					<label for="lastName">Last Name:</label>
					<input type="text" disabled name="lastName" id="lastName" placeholder="<?php echo $user->getLastName();?>">
				</div>
				<div class="field-wrap">
					<label for="">ID Number:</label>
					<input type="text" name="IdNumber" disabled id="IdNumber" placeholder="<?php echo $user->getIdNumber();?>">
				</div>
				<div class="field-wrap">
					<label for="avatar">Avatar: </label>
					<img style="position:relative;top:10px;" src="<?php echo $user->getAvatarUrl();?>" width="30px" />
					<input style="width:250px" type="file" id="avatar" accept="image/jpeg, image/png" name="avatar" />
				</div>
						
				<button>Save Changes</button>
			</form>

		</div>
	</section>

