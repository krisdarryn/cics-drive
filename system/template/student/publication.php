	<section class="widget">
		<header>
			<span class="icon">&#59153;</span>
			<hgroup>
				<h1><?php echo $document;?></h1>
			</hgroup>
		</header>
		<div class="content">
          <div id="publicationDescription" style="padding: 10px; margin-bottom: 50px; font-size: 1.3em;">
            <?php echo $description;?>
          </div>
          <div id="drive-editor">
                  <textarea id="codeEditor" name="codeEditor"><?php echo $fileContents;?></textarea>
                  <input type="hidden" name="mime" id="mime" value="<?php echo $mimeType;?>">
          </div>
          <div id="publicationComments" style="margin:20px 0;">
            <?php echo $comments;?>
            <?php if (!$muted): ?>
            <form method="post">
              <input type="text" maxlength="140" name="txtAddComment" autocomplete="off">
              <button>Add Comment</button>
            </form>
            <?php endif; ?>
          </div>
          <div style="clear:both;"></div>
		</div>
	</section>
