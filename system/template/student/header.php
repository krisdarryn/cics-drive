<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<title><?php echo $this->getTitle();?></title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="robots" content="" />
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
	<?php echo $this->getCssAssets();?>
	<!--[if IE]><link rel="stylesheet" href="css/ie.css" media="all" /><![endif]-->
</head>
<body>
<header class="main">
	<h1><strong>CICS</strong> Drive</h1>	
    <div class="pull-right" style="margin-top:-30px;">
      <select name="facultySearch" id="facultySearch" style="width:200px;"/>
        <option></option>
        <?php echo $facultyOptions;?>
      </select>
    </div>
</header>
<section class="user">
	<div class="profile-img">
		<p><a href="/student/account/edit"><img src="<?php echo $user->getAvatarUrl();?>" alt="" height="40" width="40" /></a> Welcome back <?php echo $user->getFirstName();?></p>
	</div>
	<div class="buttons">
<!--		<span class="button dropdown">
			<a href="#">Notifications <span class="pip">3</span></a>
			<ul class="notice">
				<?php echo $notifications;?>
				<li>
					<hgroup>
						<h1><a href="#">View all notifications</a></h1>
					</hgroup>
				</li>
			</ul>
		
		</span> -->
		
		 <a href="<?php echo SITE_URL.'logout';?>"><button class="blue" style="float:right">Logout</button></a>
		 <!--<?php echo $storageindicator;?>-->
	</div>
</section>

<nav>
	<ul>
		<li><a href="<?php echo SITE_URL.'student/drive';?>"><span class="icon">&#59256;</span> My Drive</a></li>
		<li><a id="editLink" href="javascript:;"><span class="icon">&#59256;</span>Code Editor</a></li>
                <li><a href="<?php echo SITE_URL.'student/item_recovery';?>"><span class="icon">&#59249;</span> Item Recovery</a></li>
		<li><a><span class="icon">&#128100;</span> Account <span class="miniIcon expandMenu">&#59228;</span></a>
			<ul class="submenu">
				<li><span class="subicon">&#10002;</span><a href="<?php echo SITE_URL.'student/account/edit';?>">Edit Account</a></li>
				<li><span class="subicon">&#9998;</span><a href="<?php echo SITE_URL.'student/account/edit/login_details';?>">Change Login Details</a></li>
				<li class="last"><span class="subicon">&#59249;</span><a href="<?php echo SITE_URL.'student/account/edit/security_questions';?>">Reset Security Answers</a></li>
			</ul>
		</li>
		<li></li>
	</ul>
	
</nav>
<?php echo $this->getAlertHtml();?>
<section class="content">