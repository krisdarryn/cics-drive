﻿	<section class="widget">
		<header>
			<span class="icon">&#10002;</span>
			<hgroup>
				<h1><?php echo $name;?>'s Shared folders</h1>
                <?php echo $breadcrumb;?>
			</hgroup>
		</header>
		<div class="content">
          <h1 style="font-size:2em; font-weight: bold; margin: 1em 0;"><span class="icon icon-dark">&#128274;</span>This folder is private</h1>
          <h2 style="font-size:1.5em; font-weight: bold; margin: .8em 0;">Enter the password to continue:</h2>
          <div>
            <form method="post">
              <input type="password" autofocus name="folderPassword">
              <button class="green">Continue</button>
            </form>
          </div>
		</div>
	</section>

