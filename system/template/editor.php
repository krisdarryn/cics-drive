	<section class="widget">
		<header>
			<span class="icon">&#59153;</span>
			<hgroup>
				<h1><?php echo $document;?></h1>
                <h2><?php echo $breadCrumb;?></h2>
			</hgroup>
		</header>
		<div class="content">
			<form method="post" action="">
				<div class="toolbar">
					<button name="btnOpen" rel="ajax:modal">Open File</button>
					<button name="btnDownload" value="dldl"> <span class="icon">&#59224;</span> Download File</button>

					<button name="btnSave" value="true">Save</button>
				</div>

				<div id="drive-editor">
						<textarea id="codeEditor" name="codeEditor"><?php echo $fileContents;?></textarea>
						<input type="hidden" name="directory" id="directory" value="<?php echo $directory;?>">
						
						<input type="hidden" name="mime" id="mime" value="<?php echo $mimeType;?>">
				</div>

				<div style="clear:both;"></div>
			</form>
		</div>
	</section>
