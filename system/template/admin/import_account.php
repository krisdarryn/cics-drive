﻿
	<section class="widget">
		<header>
			<span class="icon">&#57347;</span>
			<hgroup>
				<h1>Import User Account</h1>
				<!-- <h2>Drag &amp drop file upload</h2> -->
			</hgroup>
		</header>
		<div class="content">
			<form method="post" enctype="multipart/form-data">
				<div class="field-wrap">
					<label for="avatar">Import CSV file </label>
					<input type="file" id="csv" accept="application/csv" name="csv"/>
				</div>

				<button class="green" name="importCSV" require >Import</button>
			</form>

		</div>
	</section>


