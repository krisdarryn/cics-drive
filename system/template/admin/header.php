<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<title><?php echo $this->getTitle();?></title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="robots" content="" />
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
	<?php echo $this->getCssAssets();?>
	<!--[if IE]><link rel="stylesheet" href="css/ie.css" media="all" /><![endif]-->
</head>
<body>
<header class="main">
	<h1><strong>CICS</strong> Drive</h1>
</header>
<section class="user">
	<div class="profile-img">
		<p><a href="/admin/account/edit"><img src="<?php echo $user->getAvatarUrl();?>" alt="" height="40" width="40" /><a> Welcome back <?php echo $user->getFirstName();?></p>
	</div>
	<div class="buttons">
		<!--<button class="ico-font">&#59157;</button>-->
<!--		<span class="button dropdown">
			<a href="#">Notifications <span class="pip">3</span></a>
			<ul class="notice">
				<?php echo $notifications;?>
				<li>
					<hgroup>
						<h1><a href="#">View all notifications</a></h1>

					</hgroup>
				</li>
			</ul>
		</span> -->

		<a href="<?php echo SITE_URL.'logout';?>"><button class="blue" style="float:right">Logout</button></a>
	</div>
</section>

<nav>
	<ul>
		<li title="My Drive"><a href="<?php echo SITE_URL.'admin/drive';?>"><span class="icon">&#59256;</span> My Drive</a></li>
		<li><a id="editLink" href="javascript:;"><span class="icon">&#59256;</span>Code Editor</a></li>
                <li title="Item Recovery"><a href="<?php echo SITE_URL.'admin/item_recovery';?>"><span class="icon">&#59249;</span> Item Recovery</a></li>
		<li title="Account">
			<a><span class="icon">&#128100;</span> Account <span class="miniIcon expandMenu">&#59228;</span></a>
			<ul class="submenu">
				<li title="Edit Account"><span class="subicon">&#10002;</span><a href="<?php echo SITE_URL.'admin/account/edit';?>">Edit Account</a></li>
                                <li title="Change Login Details"><span class="subicon">&#9998;</span><a href="<?php echo SITE_URL.'admin/account/edit/login_details';?>">Change Login Details</a></li>
			</ul>
		</li>
		<li title="User Management">
			<a><span class="icon">&#128101;</span>Users Management <span class="miniIcon expandMenu">&#59228;</span></a>
			<ul class="submenu">
				<li title="Account List"><span class="subicon">&#57349;</span><a href="<?php echo SITE_URL.'admin/users/list';?>">Accounts List</a></li>
				<li title="Import Account"><span class="subicon">&#57347;</span><a href="<?php echo SITE_URL.'admin/users/import';?>">Import Accounts</a></li>
				<li title="Security Questions"><span class="subicon">&#59153;</span><a href="<?php echo SITE_URL.'admin/security_questions';?>">Security Questions</a></li>
			</ul>
		</li>
		<li title="Logs and Reports">
			<a><span class="icon">&#128318;</span> Logs and Reports <span class="miniIcon expandMenu">&#59228;</span></a>
			<ul class="submenu">
				<li title="File Activity"><span class="subicon">&#57349;</span><a href="<?php echo SITE_URL.'admin/logs/files';?>">File Activity</a></li>
				<li title="User Activity"><span class="subicon">&#128101;</span><a href="<?php echo SITE_URL.'admin/logs/users';?>">User Activity</a></li>
			</ul>
		</li>
		<li title="System Backup">
			<a href="<?php echo SITE_URL.'admin/backup';?>"><span class="icon">&#9881;</span> System Backup</a>
                </li>
		<li title="File Filter">
			<a href="<?php echo SITE_URL.'admin/file_filter';?>"><span class="icon">&#128196;</span> File Filter</a>
		</li>
		<li></li>
	</ul>
	
</nav>
<?php echo $this->getAlertHtml();?>
<section class="content">