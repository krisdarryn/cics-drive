﻿	<section class="widget">
		<header>
			<span class="icon">&#9729;</span>
			<hgroup>
				<h1><?php echo $user->getLastName().', '.$user->getFirstName().'\'s';?> Drive</h1>
				<h2><?php echo $breadcrumb;?></h2>
			</hgroup>
			
			<aside>
				<span>
					<a id="option" >&#9881;</a>
					<ul class="settings-dd">
						<li><label for="alldrive">Display all drive items</label> <input type="radio"  id="alldrive" name="drive_view" /></li>
						<li><label for="folderdrive" >Display folder only</label> <input type="radio"  id="folderdrive" name="drive_view" /></li>
						<li><label for="filedrive" >Display file only</label> <input type="radio"  id="filedrive" name="drive_view" /></li>
					</ul>
				</span>
			</aside>
		</header>
		<div class="content">
			<div class="toolbar">
				<input id="url" type="hidden" value="<?php echo $dir;?>" />
				<input id="dirPath" type="hidden" value="<?php echo rtrim(SITE_URL,'/').$dir?>" />
				<input id="viewUserID" type="hidden" value=<?php echo $user->getUserID() ?> />
	<!--remove-->
				<form action="<?php echo $dir; ?>" method="post" style="display:inline;">
					<button id="remove_button"style="display:none;" >Remove</button>
				</form>
					<button id="dummyRemove_button" class="orange"><span class="icon" style="color:#4969E5;" >&#10006;</span> Remove Item </button>
	<!---delete-->
				<form style="display:inline;" action="<?php echo $dir;?>" method="post">
					<button style="display:none;" id="delete_button"></button>
				</form>
					<button class="blue" id="dummyDelete_button"><span class="icon">&#59177;</span> Delete </button>
			</div>
			
			<div id="flash"></div>
			<div id="dropzone">
				<ul id="fileSystem">
					<?php echo $contentString;?>
				</ul>
			</div>
		</div>
	</section>

<?php echo $this->getJsAssets();?>
</body>
</html>