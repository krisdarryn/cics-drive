
	<section class="widget">
		<header>
			<span class="icon">&#57349;</span>
			<hgroup>
				<h1>System Backup</h1>
			</hgroup>
		</header>
            <div class="content">
                 <p class="content" style="border: 1px solid #424242;padding:10px;line-height:1.5em;font-size:13px;border-radius: 5px;">
                     <strong>Reminder:</strong> The CICS Drive Backup module only copies the current <strong>Database Tables with records</strong>,<strong>all User Avatars</strong> and  <strong>all User Drives</strong> (files of all users). 
                     After backing up YOU must download the recent back up and to be stored in to different devices for maximum security. <br/>
                 </p>
                 <div class="toolbar">
<!--create backup-->	
                        <button class="blue" id="create_backup"><span class="icon">&#128230;</span> Create Backup </button>
                 </div>
             <div style="height:400px;overflow: auto;">
                 <table id="myTable">
                     <thead>
                         <tr>
                            <th>Backup History</th>  
                            <th>Action</th>  
                            
                         </tr>
                     </thead>
                     <tbody>
                         <?php foreach($backupRecord as $key => $record) {?>
                            <tr> 
                                <td><strong>Back up Name : </strong><span class="backup_name"><?php echo $record->getName(); ?></span></td>
                                <td>
                                    <a class="button small download_backup green"><span class="icon">&#128229;</span> Download</a>
                                    <a class="button small restore_backup orange"><span class="icon">&#59249;</span> Restore</a>
                                    <a class="button small delete_backup"><span class="icon">&#59177;</span> Delete</a>
                                </td>
                            </tr>                          
                         <?php }?>                        
                     </tbody>
                 </table>
            </div>
        </section>
        <div class="modal" id="backupmodal">
            <h4>Backing up current System State</h4>
            <div class="tables">
                <span>Step 1:</span><span></span>
            </div>
            <div class="avatars">
                <span>Step 2:</span><span></span>
            </div>
            <div class="drives">
                <span>Step 3:</span><span></span>
            </div>
            <div class="pull-right">
                <button id="backupcancel" class="red">Cancel</button>
                <button id="backupok" class="blue">Start</button>
            </div>
        </div>
        <div class="modal" id="restoremodal">
            <h4>Restoring</h4>
            <div class="restore">
                 <span>This may take some time. Please wait...</span>
            </div>
            <div class="pull-right">
                <button id="restorecancel" class="red">Cancel</button>
                <button id="restoreok" class="blue">Start</button>
            </div>
        </div>
        <div style="display:none">
            <form action="<?php SITE_URL.'admin/backup'?>" method="post">
                <input type="hidden" name="download" value="">
                <button id="download"></button>
            </form>
        </div>
        <div style="display:none">
            <form action="<?php SITE_URL.'admin/backup'?>" method="post">
                <input type="hidden" name="delete" value="">
                <button id="delete"></button>
            </form>
        </div>
