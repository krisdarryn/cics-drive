
	<section class="widget">
		<header>
			<span class="icon">&#59136;</span>
			<hgroup>
				<h1>Create User Account</h1>
			</hgroup>
		</header>
		<div class="content">
			<form method="post" enctype="multipart/form-data">	
				<div>
					<p style="text-align:right;font-style:italic;">*Select <strong> the type</strong> of  <strong> user account</strong> you want to <strong> create.</strong>.</p>
				</div>
				<div class="field-wrap">
					<table border="1"> 
					<tr height="40px">
						<td><input type="radio" value="1" name="usertype" id="1"/></td>
						<td align="left"><label for="a1">Admin</label></td>
					</tr>
					<tr height="40px">
						<td><input type="radio" value="<?php echo $a = 2;?>" name="usertype" id="2"/></td>
						<td align="left"><label for="a1">Faculty</label></td>
					</tr>
					<tr height="40px">
						<td><input type="radio" value="3" name="usertype" id="3"/></td>
						<td align="left"><label for="a1">Student</label></td>
					</tr>
				</table>
				</div>
				<div class="field-wrap">
					<label for="idno">id Number:<h5 style="text-align:right;font-style:italic;">*For <strong>student</strong> only.</h5></label>
					<input type="text" name="idno" id="idno" placeholder="Type Id Number">
				</div>
				<div class="field-wrap">
					<label for="fname">First Name:<h5 style="text-align:right;font-style:italic;">*For <strong>student</strong> only.</h5></label>
					<input type="text" name="fname" id="fname" placeholder="Type First Name">
				</div>
				<div class="field-wrap">
					<label for="lname">Last Name:<h5 style="text-align:right;font-style:italic;">*For <strong>student</strong> only.</h5></label>
					<input type="text" name="lname" id="lname" placeholder="Type Last Name">
				</div>
				<div>
					<p style="text-align:center;font-style:italic;">*Note: the field below are for <strong> faculty / admin </strong> type of <strong> account </strong> only.</p>
				</div>
				<div class="field-wrap">
					<label for="email">E-mail address:</label>
					<input type="email" name="email" id="email" placeholder="Type Email Address">
				</div>
				<div class="field-wrap">
					<label for="Password">Password:</label>
					<input type="password" name="Password" id="password" placeholder="Type Password">
				</div>
				<button class="green">Create Account</button>
			</form>

		</div>
	</section>

