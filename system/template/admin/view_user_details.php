<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<title><?php echo $this->getTitle();?></title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="robots" content="" />
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
	<?php echo $this->getCssAssets();?>
	<!--[if IE]><link rel="stylesheet" href="css/ie.css" media="all" /><![endif]-->
</head>
<body style="padding:0px;margin:0xp">
	<section class="widget">
		<header>
			<hgroup>
				<h1 style="letter-spacing:2px;"> 	
                                  <img src="<?php echo $user->getAvatarUrl();?>" height="40" width="40"/> 
					<?php 
						$type='';
						switch($user->getUserType()){
							case 1:	$type = 'Administrator ';
									break;
							case 2:	$type = 'Faculty member ';
									break;
							case 3:	$type = 'Student ';
						}
						echo $type.$user->getLastName().'\'s Profile';
					?>
				</h1>
			</hgroup>
		</header>
		<?php echo $this->getAlertHtml();?>
		<section class="detailsmargin">	
			<div>
                                
				<form method="post" action="<?php echo SITE_URL.'admin/users/details'; ?>">
					<label>ID Number</label>
						<input type="text"  name="idno" value="<?php echo $user->getIdNumber(); ?>">
					<label>Last Name</label>
						<input type="text" name="lname" required value="<?php echo $user->getLastName(); ?>">
					<label>Middle Initial</label>
                    <input type="text" name="mname" maxlength="1" value="<?php echo $user->getMiddleInitial(); ?>">
					<label>First Name</label>
						<input type="text" name="fname" required value="<?php echo $user->getFirstName(); ?>">
					<label>Email Address</label>
						<input type="text" name="email" value="<?php echo $user->getEmail(); ?>">
					<label>Used Storage</label>
						<input type="text" disabled="disabled"	value="<?php echo $user->getUsedStorage(); ?>">
					
                          <?php if ($user->getStatus() == "I"): ?>
                           <input type="hidden" name="userStatus" value="<?php echo $user->getStatus(); ?>">
                          <?php else: ?>
                           <label>Status</label>
                        <select name="userStatus" style="padding: 10px;
font-size: 12px;
font-family: Arial;
color: #606060;
border: 1px solid #e3e3e3;
background: #fff;
width: 100%;
margin: 0 0 15px 0;
border-radius: 3px;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
box-sizing: border-box;
-webkit-box-sizing: border-box;
-moz-box-sizing: border-box;">
                          <option value="A" <?php echo $user->getStatus() == "A" ? "selected":"";?>>Active</option>
                          <option value="K" <?php echo $user->getStatus() == "K" ? "selected":"";?>>Suspicious</option>
                          <option value="S" <?php echo $user->getStatus() == "S" ? "selected":"";?>>Suspended</option>
                        
                        </select>
                        <?php  endif;?>
					<button class="green" name="accountUpdate" value="<?php echo $user->getUserType();?>"> Update Account</button>
				</form>
			</div>
		</section>
	</section>
	

<?php echo $this->getJsAssets();?>
</body>
</html>

