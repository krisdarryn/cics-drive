
	<section class="widget">
		<header>
			<span class="icon">&#57349;</span>
			<hgroup>
				<h1>File Activity Logs and Reports</h1>
			</hgroup>
		</header>
				
		<div class="toolbar">
                       <a class="button" href="#settingsModal" rel="modal:open"><span class="icon">&#9881;</span> Settings</a>
                       <a class="button" href="#daterange" rel="modal:open"><span class="icon">&#9881;</span> Date Range</a>
                       <form action="<?php echo SITE_URL.'admin/logs/files';?>" style="display:inline" method="post">
                           <!-- <input type="text" placeholder="Search by user" name="user" value="" style="width: 300px;">-->
                       </form>
                </div>
		<table id="myTable" class="clearfix">
			<thead>
				<tr>
					<th><a href="" class="sortable">Description</a></th>
					<th><a href="javascript:;" class="sortable">Date</a></th>
					<th><a href="javascript:;" class="sortable">Time</a></th>
					
				</tr>
			</thead>
			<tbody>
                                <?php if( count($report) == 0 ){ ?>
                                        <tr>
                                            <td><?php echo 'No record found'; ?></td>
                                            <td><?php echo '- - - - - -'; ?></td>
                                            <td><?php echo '- - - - - -'; ?></td>
                                        </tr>
                                <?php } ?>

                                <?php foreach($report as $key => $r){ ?>
                                    <tr>
                                        <td><?php echo $report[$key]['report']?></td>
                                        <td><?php echo $report[$key]['date']?></td>
                                        <td><?php echo $report[$key]['time']?></td>
                                    </tr>
                                <?php } ?>
				
			</tbody>
		</table>
		<?php echo $pagination; ?>
	</section>
	 <div class="modal" id="settingsModal">
                <h3 id="title">Settings</h3>
                <hr style="border: 1px solid black"/> 
                <div id="modalContents" style="margin-top: -10px;">
                    <div class="content">
                        <form method="post" action="<?php echo SITE_URL.'admin/logs/files';?>" >
                                <h4>User Types:</h4>
                                <div  class="checkboxGroup" id="usertypes">
                                    <label for="userTypeStudents">Students
                                        <input type="checkbox" id="userTypeStudents" name="userType[]" value="3" <?php echo in_array("3", $flag)?'checked':''; ?> >
                                    </label>

                                    <label for="userTypeFaculty">Faculty
                                        <input type="checkbox" id="userTypeFaculty" name="userType[]" value="2" <?php echo in_array("2", $flag)?'checked':''; ?> >
                                    </label>

                                    <label for="userTypeAdministrators">Administrators
                                        <input type="checkbox" id="userTypeAdministrators" name="userType[]" value="1" <?php echo in_array("1", $flag)?'checked':''; ?> >
                                    </label>
                                </div>
                            <hr/>
                            <h4>Category:</h4>
                                <div id="category">
                                    
                                     <select name="category">
                                            <option value="99999" <?php echo $cat==99999?'selected':'';?>>All</option>
                                           <?php foreach($categories as $key => $category){?>
                                            <option value="<?php echo $category; ?>" <?php  echo ($cat!=99999 && $category==$cat)?'selected':'';?> > <?php echo $key; ?></option >
                                           <?php }?>
                                     </select>
                                </div>
                            <hr/>
                            <button class="green pull-right" id="submitButton">Ok</button>
                            <input type="hidden" id="sortColumn" name="sortColumn" ">
                            <input type="hidden" id="sortOrder" name="sortOrder" >
                        </form>
                    </div>
                </div>
        </div>
        <div class="modal" id="daterange">
             <form method="post" action="<?php echo SITE_URL.'admin/logs/files';?>" >
                    <h4>Date range:</h4>
                    <div id="date">
                         <label for="startdate"><strong>Start Date</strong></label>
                         <input id="startdate" type=""text name="startdate" value="<?php echo count($date)>0?$date[0]:'';?>">
                         <label for="enddate"><strong>End Date</strong></label>
                         <input id="enddate" type=""text name="enddate" value="<?php echo count($date)>0?$date[1]:'';?>">
                    </div>
                    <button class="green pull-right" id="submitButton">Ok</button>
             </form>
        <div>
        