<?php
// SHA-1: 2d25cc2ff7ba7ca39d9582e9bf3e68855cb2408c
/**
 * @package system
 */
$timestart = time();
/**
 * access point sa system. Diri mag sugod ang tanan
 *
 */

/*******************************************************************************
	define the system constants
	kani sila kay dapat kay accessible bisag asa sa system
*******************************************************************************/
gc_enable(); // Enable Garbage Collector
date_default_timezone_set('Asia/Manila'); // setting the default time zone into asia/manila

// mga paths ni. dapa\xampp\htdocs\cicsdrivet kada directory naay trailing slash 
define('ROOT', '/var/www/'); // absolute path please | e.g. ROOT = D:/xampp/htdocs/
define('SYSTEM', ROOT.'system/'); // e.g. D:/xampp/htdocs/system/
define('LIBRARY', SYSTEM.'library/'); //e.g. D:/xampp/htdocs/system/library/
define('MODEL', SYSTEM.'model/'); //e.g. D:/xampp/htdocs/system/model/
define('TEMPLATE', SYSTEM.'template/'); //e.g. D:/xampp/htdocs/system/template/
define('PROCESS', SYSTEM.'process/'); //e.g. D:/xampp/htdocs/system/process/
define('IFACE', PROCESS.'interface/'); //e.g. D:/xampp/htdocs/system/process/interface/
define('DRIVE', SYSTEM.'drive_root/'); //e.g. D:/xampp/htdocs/system/drive_root/
define('BACKUP', DRIVE.';BACKUP;/'); //e.g. D:/xampp/htdocs/system/drive_root/;BACKUP;/
define('ADMINRECYCLE', DRIVE.';ADMINRECYCLE;/'); //e.g. D:/xampp/htdocs/system/drive_root/;ADMINRECYCLE;/
define('ARCHIVE', DRIVE.';ARCHIVE;/'); //e.g. D:/xampp/htdocs/system/drive_root/;ARCHIVE;/
define('AVATAR', ROOT.'media/images/avatars/'); //e.g. D:/xampp/htdocs/media/images/avatars/
define('TEMP',ROOT.'temp/'); //e.g. D:/xampp/htdocs/temp/

// mga constants para sa URL
//(lab)define('SITE_URL', 'http://172.19.130.83/'); //e.g SITE_URL = http://drive.cics.uc.ph/
//(adhoc)define('SITE_URL', 'http://169.254.162.18/'); //e.g SITE_URL = http://drive.cics.uc.ph/
define('SITE_URL', 'http://cicsdrive.gopagoda.com/'); //e.g SITE_URL = http://drive.cics.uc.ph/
define('ASSET_PATH', SITE_URL.'media/'); //e.g. http://drive.cics.uc.ph/media/
define('CSS', ASSET_PATH.'css/'); //e.g. http://drive.cics.uc.ph/media/css/
define('JS', ASSET_PATH.'js/'); //e.g. http://drive.cics.uc.ph/media/js/
define('FONT', ASSET_PATH.'@font-face/'); //e.g. http://drive.cics.uc.ph/media/@font-face/
define('IMAGES', ASSET_PATH.'images/'); //e.g. http://drive.cics.uc.ph/media/images/

define('AV_STATUS', false);
define('MYSQLDUMP', 'mysqldump'); // e.g. D:/xampp/mysql/bin/mysqldump.exe
define('MYSQL', 'mysql'); // e.g. D:/xampp/mysql/bin/mysqldump.exe

require_once SYSTEM.'drive.php';

require_once LIBRARY.'Klein.php';
require_once LIBRARY.'Page.php';

require_once PROCESS.'Process.php';

respond('/notification',function($request){
    require_once PROCESS.'Notification.php';
    echo new Notification(25000);
});

respond('/', function($request, $response){
    require_once PROCESS.'Login.php';
    new Login($request, $response);
});

respond('/logout', function($request, $response){
    require_once PROCESS.'Logout.php';
    new Logout($request, $response);
});

respond('/login', function ($request, $response) { 
    require_once PROCESS.'Login.php';
    new Login($request, $response);
});

respond('/driveview', function ($request, $response) { 
    require_once PROCESS.'DriveView.php';
    new DriveView($request, $response);
});

respond('/security_validation', function ($request, $response) { 
    require_once PROCESS.'LoginValidation.php';
    new LoginValidation($request, $response);
});

respond('/question', function ($request, $response) { 
    require_once PROCESS.'LoginValidation.php';
    new LoginValidation($request, $response);
});

respond('/register', function($request, $response){
    require_once PROCESS.'RegisterProcess.php';
    new RegisterProcess($request, $response);
});

respond('/register/changeQuestion*', function($request, $response){
    require_once PROCESS.'RegisterProcess.php';
    new RegisterProcess($request, $response);
});

respond('StepOne', function ($request, $response) { 
    require_once PROCESS.'registration/StepOne.php';
    new StepOne($request, $response);
});

respond('StepOne', function ($request, $response) { 
    require_once PROCESS.'registration/StepOne.php';
    new StepOne($request, $response);
});

respond('/recovery', function($request, $response){
    require_once PROCESS.'RecoveryProcess.php';
    new RecoveryProcess($request, $response);
});

respond('/known_file_types', function($request, $response){
	require_once PROCESS.'ValidFileTypes.php';
    new ValidFileTypes();	
});

with('/view_user_drive',function(){
	
	respond('/*', function($request, $response){
		require_once PROCESS.'admin/viewdrive/ViewUserDrive.php';
		new ViewUserDrive($request, $response);	
	});
	
	respond('/[*]', function($request, $response){
		require_once PROCESS.'admin/viewdrive/ViewUserDrive.php';
		new ViewUserDrive($request, $response);	
	});
}); 


with('/student', function () {
    require_once 'path_student.php';
});

with('/faculty', function () {
    require_once 'path_faculty.php';
});

with('/admin', function () {
    require_once 'path_admin.php';
});



// Page not found errors
respond('404', function ($request, $response) { 
    $response->code(404);
	$response->render(TEMPLATE.'404.html');
});


dispatch();



// performance info
/* $timeEnd = time();
$difference = $timeEnd - $timestart;
$kb = memory_get_usage(true) / 1024;
debug_echo("TimeStart: $timestart\nTime End: $timeEnd\nDifference: $difference\nmemory:$kb KB");
 */
