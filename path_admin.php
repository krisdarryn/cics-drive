<?php

respond('/*', function ($request, $response) { 
	$response->redirect('/admin/drive');
});

respond('/item_recovery', function($request, $response){
    require_once PROCESS.'admin/ItemRecoveryProcess.php';
    new ItemRecoveryProcess($request, $response);
});

respond('/backup', function($request, $response){
    require_once PROCESS.'admin/BackupProcess.php';
    new BackupProcess($request, $response);
});

with('/account',function(){
        respond('/edit', function ($request, $response) { 
		require_once PROCESS.'admin/EditAccount.php';
		new EditAccount($request, $response);
	});
        
        respond('/edit/login_details', function ($request, $response) { 
		require_once PROCESS.'admin/EditLoginDetails.php';
		new EditLoginDetails($request, $response);
	});
	
});

with('/users',function(){

	respond('/create_account', function ($request, $response) { 
		require_once PROCESS.'admin/CreateAccount.php';
		new CreateAccount($request, $response);
	});
	
	respond('/reset_user_password', function ($request, $response) { 
		require_once PROCESS.'admin/ResetUserPassword.php';
		new ResetUserPassword($request, $response);
	});
	
	respond('/details', function ($request, $response) { 
		require_once PROCESS.'admin/ViewUserDetails.php';
		new ViewUserDetails($request, $response);
	});
	
	respond('/import', function ($request, $response) { 
		require_once PROCESS.'admin/ImportAccount.php';
		new ImportAccount($request, $response);
	});
	
        with ('/list', function() {
                 
            respond('/[page]/[i:page]', function ($request, $response) { 
                    require_once PROCESS.'admin/AccountList.php';
                    new AcountList($request, $response);
            });
            
            respond('/*', function ($request, $response) { 
                    require_once PROCESS.'admin/AccountList.php';
                    new AcountList($request, $response);
            });
        });
		
	with('/drive', function () {
	
		respond('/*', function ($request, $response) { 
			require_once PROCESS.'admin/viewdrive/ViewUserDrive.php';
			new ViewUserDrive($request, $response);	
		});
	
		respond('/[*]', function ($request, $response) { 
			require_once PROCESS.'admin/viewdrive/ViewUserDrive.php';
			new ViewUserDrive($request, $response);	
		});
	});
	
	with('/item_recovery', function () {
	
		respond('/user', function ($request, $response) { 
			require_once PROCESS.'admin/viewdrive/UserItemRecovery.php';
			new UserItemRecovery($request, $response);	
		});
	
		respond('/admin', function ($request, $response) { 
			require_once PROCESS.'admin/viewdrive/AdminItemRecovery.php';
			new AdminItemRecovery($request, $response);	
		});
	});
	
	
});

with('/logs',function(){
	
	with('/files',function(){
		respond('/[page]/[i:page]',function($request, $response){
			require_once PROCESS.'admin/FileActivity.php';
			new FileActivity($request, $response);
		});
		
		respond('/*',function($request, $response){
			require_once PROCESS.'admin/FileActivity.php';
			new FileActivity($request, $response);
		});
	});
	
        with('/users',function(){
		respond('/[page]/[i:page]',function($request, $response){
			require_once PROCESS.'admin/UserActivity.php';
			new UserActivity($request, $response);
		});
		
		respond('/*',function($request, $response){
			require_once PROCESS.'admin/UserActivity.php';
			new UserActivity($request, $response);
		});
	});
	
});
		
with('/user/[i:userId]', function(){
    respond('/drive/*', function ($request, $response) { 
        $request->path = '/';
        require_once PROCESS.'admin/AccountDrive.php';
        new AccountDrive($request, $response);
    }); 
    respond('/drive/[*:path]', function ($request, $response) { 
        require_once PROCESS.'admin/AccountDrive.php';
        new AccountDrive($request, $response);
    }); 
});

respond('/security_questions', function ($request, $response) {
	require_once PROCESS.'admin/SecurityQuestion.php';
	new SecurityQuestion($request, $response);
});


respond('/file_filter', function ($request, $response) { 
	require_once PROCESS.'admin/FileFilter.php';
	new FileFilter($request, $response);
});



with('/drive', function () {

	respond('/*', function ($request, $response) { 
	    require_once PROCESS.'admin/DriveProcess.php';
	    new DriveProcess($request, $response);
		});

	respond('/[*]', function ($request, $response) { 
	    require_once PROCESS.'admin/DriveProcess.php';
	    new DriveProcess($request, $response);
	});
});

with('/editor', function () {
	respond('/[*]', function ($request, $response) { 
	    require_once PROCESS.'admin/EditorProcess.php';
	    new EditorProcess($request, $response);
	});

	respond('/*', function ($request, $response) { 
	    require_once PROCESS.'admin/EditorProcess.php';
	    new EditorProcess($request, $response);
	});
});